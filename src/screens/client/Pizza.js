import React, { Component, memo ,useState } from 'react'
import { Dimensions, View, ImageBackground, ActivityIndicator, Image, StatusBar, ToastAndroid, StyleSheet , FlatList} from 'react-native'
import { pizzaByPrestataire, allPrestataire, bestDeals } from '../../API/client/list'
import { TouchableOpacity, ScrollView } from 'react-native-gesture-handler'
import { addToPanier } from '../../API/client/commande'
import Background from '../../components/Background'
import { MENU_URI, API_URL } from '../../core/config'
import Swiper from 'react-native-swiper';
import {
    widthPercentageToDP as wp,
    heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
var {height , width} = Dimensions.get('window');
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import { faUser, faCartPlus, faCommentsDollar, faDatabase, faPizzaSlice, faList } from '@fortawesome/free-solid-svg-icons';
import Ionicons from 'react-native-vector-icons/MaterialCommunityIcons';
import { currentClient, isClientLogged, getData, logged } from "../../core/session";
import TopMenu from "../../components/TopMenu";
import { noterPrestataire } from '../../API/client/authClient';
import { Rating, AirbnbRating } from 'react-native-ratings';
// import ViewOverflow from 'react-native-view-overflow';
import { Container, Header, Content, Card, CardItem, Thumbnail, Text, Button, Icon, Left, Body, Right } from 'native-base';
import CustomHeader from './CustomHeader'
import HomeScreen from '../HomeScreen'
import { createBottomTabNavigator } from 'react-navigation-tabs';
import { createAppContainer } from 'react-navigation'
import Notifications from './Notifications'
import { createStackNavigator } from 'react-navigation-stack'
import { AppLoading } from 'expo'
import TopMenuP from "../../components/TopMenuP"
import { theme } from '../../core/theme'
import { createMaterialBottomTabNavigator } from '@react-navigation/material-bottom-tabs';
import Commande from './Commande'
import Prestataire from './Prestataire'
import { render } from 'react-dom'

const Tab = createMaterialBottomTabNavigator();

class Pizza extends Component {
    constructor() {
        super()
        this.state = {
            // cat:[],
            bestDeals:[],
            prestataires:[],
            // selectedCat :null,
            // banner:[],
            client: {},
            isLoading: false,
            note: 1,
            selectedId: 0
            
        }
    }
    componentDidMount = async () => {
        this.setState({ isLoading: true });
        await this._getbestDeals()
        await this._getAllPresta()

        // await this.fetchCli();
        // await this.fetchPizzaOwn();

        // this.setState({ isLoading: false })
    }

    componentWillUnmount= async()=>{
      this.setState({
        prestataires:[],
        client: {},
      })
    }

    _getAllPresta = async () =>{
      let prestataires = await allPrestataire()
      this.setState({prestataires},()=> this.setState({isLoading : false}) )
      
    }

    _getbestDeals = async () =>{
      let b = await bestDeals()
      this.setState({bestDeals : b},)
      
    }


    fetchCli = async () => {
        let a = await currentClient()
        alert(a)
        this.setState({ client: a })
        if(a !== null){
          alert(a)
          this.props.navigation.setParams({showTabBar : show});
        }
    }

    fetchPizzaOwn = async () => {
        let piz = await pizzaByPrestataire(this.props.navigation.state.params.Num_prestataire)

        this.setState({ pizzas: piz })
    }

    addPanier = async (Code_produit) => {
        this.setState({ isLoading: true })
        let id_client = this.state.client["id_client"]
        let Num_prestataire = this.props.navigation.state.params.Num_prestataire

        let res = await addToPanier({ Code_produit, id_client, Num_prestataire })

        console.log(res)

        res === "yet" ?
            ToastAndroid.show("Ajouté au panier", ToastAndroid.SHORT)
            : ToastAndroid.show("Déjà dans le panier", ToastAndroid.SHORT)
        this.setState({ isLoading: false })
    }

    _renderBestItem = (item) => (  
      <TouchableOpacity style={{flex:1}}>      
          <Image resizeMode="contain" source={{uri:`${MENU_URI}${item.file}`}}  style={{width:'100%' , height:'100%'}} />

            <Text> {item.Code_produit} </Text>



      </TouchableOpacity> 
    )

    evaluate = async (presta) => {
        let idCli = this.state.client["id_client"]
        let num_prestataire = presta['Num_prestataire']
        let note = this.state.note
        let response = await noterPrestataire(idCli, num_prestataire, note)
    }

    _renderItemFood(item){
      return(
          <TouchableOpacity style={styles.divFood}
            	onPress ={()=> {
                // console.log(this.props.navigation)
                this.props.navigation.navigate("MenuDetails",{express :false, isNewCli : false , currentPrestataire : item})
              }
            }
          >
            {/* <ViewOverflow> */}
              <Image
                  style={styles.imageFood}
                  resizeMode="cover"
                  // source={require('../../../assets/assietePizza.png')} />
                  source={{ uri: `${MENU_URI}${item['nom_file']}`}} />
            
            {/* </ViewOverflow> */}
              
            
              <View style={{height:((width/2)-20)-150, backgroundColor:'transparent', width:((width/2)-20)-10}} />
              <Text style={{fontWeight:'bold',fontSize:20,textAlign:'center'}}>
                {item['Nom_etablissement']}
              </Text>
              <Text style={{textAlign: 'center',paddingHorizontal: 5 , fontSize : 12}}>Tél: {item.Telephone} </Text>
              <Text style={{textAlign: 'center' , paddingHorizontal: 5 , fontSize : 12}}>Email: {item.Email} </Text>


              {/* <View style={{ backgroundColor: "white" }}>
                <AirbnbRating
                    count={5}
                    showRating={true}
                    reviews={["Terrible", "Mauvais", "Bien", "Très bien", "Excellent",]}
                    defaultRating={3}
                    size={10}
                    fraction={1}
                    ratingBackgroundColor='white'
                    onFinishRating={(value) => {
                        this.setState({ note: value })
                    }}
                />
                </View> */}
              {/* <Text style={{fontSize:20,color:"green"}}>${item.price}</Text> */}
          </TouchableOpacity>
       
       
        )
  }

    
    render() {
      if(this.state.isLoading){
        return  <View style={styles.loading_container}>
        <ActivityIndicator size='large' />
      </View>
      }

        return (

          <View style={{ flex: 1 ,backgroundColor:theme.colors.primary}}>
           
            {/* <View style={{width: width, alignItems:'center'}} >
                <Image style={{height:60,width:width/2 }} resizeMode="contain" source={require("../../../assets/assietePizza.png")}  /> */}
                {/* <Swiper style={{height:width/2}}  showsButtons={false} autoplay={true} autoplayTimeout={2}> */}
                  {/* {
                    this.state.dataBanner.map((itembann)=>{
                      return(
                        <Image style={styles.imageBanner} resizeMode="contain" source={{uri:itembann}}/>
                      )
                    })
                  } */}
                        {/* <Image style={styles.imageBanner} resizeMode="contain" source={require('../../../assets/223-2231517_the-aeroflyfs2-from-google-play-store-isnt-running.png')}/> */}

                {/* </Swiper> */}
                {/* <View style={{height:20}} /> */}
            {/* </View> */}

            <TopMenuP navigation={this.props.navigation} title="Nos prestataires" />
  
            <View style={styles.container}>           

            { this.state.prestataires.length !== 0 ?        
               <View style={{flex:1}} >
                 <Text style={{ color: theme.colors.primary}} >Produits recommandés</Text>
                  <View style={{ height:10}} />
                  <FlatList
                    style={{flex:1}}
                    horizontal={true}
                    data={this.state.bestDeals}
                    renderItem={({ item }) => this._renderBestItem(item)}
                    keyExtractor = { (item,index) => index.toString() }
                  />

               </View>
               
              
                : null}

              { this.state.prestataires.length !== 0 ?        
                <FlatList
                  style={{flex:1}}
                  data={this.state.prestataires}
                  // numColumns={this.state.bestDeals.length !== 0 ? false : 2}
                  // horizontal ={ this.state.bestDeals.length !== 0 ? true : false}
                  horizontal={true}
                  renderItem={({ item }) => this._renderItemFood(item)}
                  keyExtractor = { (item,index) => index.toString() }
                />
              
              :            
              <View style={{ marginVertical: hp("30%") , alignItems: 'center'}}>
                <Image source={require('../../../assets/empty.png')}  style={{ width: width /4 , height: width/4}}/>
                <Text style={{ alignItems: 'center'}}>
                    Aucun prestataire n'est disponible
                </Text>
              </View>              
               }
           
          	</View>

          </View>

        )
    }
}


const styles = StyleSheet.create({
  container: {
    // flex: 1,
    // justifyContent: "center",
    // alignItems: "center"
    flex: 1,
    paddingTop:15,
    borderTopStartRadius:40,
    borderTopEndRadius:40,

    marginTop:20,
    // marginTop: StatusBar.currentHeight || 0,
    // paddingTop:10,
    backgroundColor: 'white'
  },
    imageBanner: {
    height:width/2,
    width:width-40,
    borderRadius:10,
    marginHorizontal:20
  },
  divCategorie:{
    backgroundColor:'red',
    margin:5, alignItems:'center',
    borderRadius:10,
    padding:10
  },
  titleCatg:{
    fontSize:20,
    fontWeight:'bold',
    textAlign:'center',
    // marginBottom:10
  },
  imageFood:{
    flex:1,
    borderRadius: 20,
    flexDirection: 'row',
    alignItems: 'stretch',
    width:((width/2)-20)-10,
    height:((width/2)-20)-30,
    backgroundColor:'transparent',
    // position:'absolute',
    // top:-45,

  },
  divFood:{
    flex:1,
    width:(width/2)-20,
    // paddingHorizontal: 10,
    paddingBottom: 10,
    borderRadius:10,
    marginTop:20,
    marginBottom:5,
    marginLeft:10,
    alignItems:'center',
    elevation:8,
    shadowOpacity:0.3,
    shadowRadius:50,
    backgroundColor:'white',
  },
  loading_container: {
    position: 'absolute',
    // zIndex: 10,
    left: 0,
    right: 0,
    top: 100,
    bottom: 0,
    alignItems: 'center',
    justifyContent: 'center'
  },
  divDeals: {
    backgroundColor:'white',
    width:(width/2)-20,
    padding:10,
    borderRadius:10,
    marginTop:55,
    marginBottom:5,
    marginLeft:10,
    alignItems:'center',
    elevation:8,
    shadowOpacity:0.3,
    shadowRadius:50,
    position: 'relative',
    overflow: 'visible', // doesn't do anything
  },
  divImage: {
    borderRadius: 40,
    width:((width/2)-20)-10,
    height:((width/2)-20)-30,
    backgroundColor:'transparent',
  },
  divImage1: {
    position: 'absolute',
    top: -45,
  },
});


export default  function Main() {

  const [isLogged, setisLogged] = useState(false)
  
  const test = async()=>{
    let res = await isClientLogged()
    setisLogged(res)
  console.log(isLogged)
  }  


  if(isLogged)  
    return (
       <Tab.Navigator          
          activeColor={theme.colors.primary}
          inactiveColor='grey'
          barStyle={{backgroundColor:'white'}}
          shifting={true}
          screenOptions={({ route }) => ({
              tabBarIcon: ({ focused, color, size }) => {
                  let iconName;
      
                  if (route.name === 'Prestataires') {
                  iconName = focused
                      ? 'tablet-dashboard'
                      : 'view-dashboard-outline';
                  } else if (route.name === 'Menu') {
                  iconName = focused ? 'format-list-bulleted' : 'format-list-bulleted';
                  } else if (route.name === 'Commande') {
                  iconName = focused ? 'cart-arrow-right' : 'cart';
                  }
      
                  // You can return any component that you like here!
                  return <Ionicons  name={iconName} size={20} color={color} />;
              },
              })}
              tabBarOptions={{
              activeTintColor: 'red',
              inactiveTintColor: 'black',
              }}
      > 
        <Tab.Screen name="Prestataires" component={Pizza} />
        <Tab.Screen name="Commande" component={Commande} />
        <Tab.Screen name="Test" component={Prestataire} />

      </Tab.Navigator>
    );

    return(
      <Tab.Navigator          
      activeColor={theme.colors.primary}
      inactiveColor='grey'
      barStyle={{backgroundColor:'white'}}
      shifting={true}
      screenOptions={({ route }) => ({
          tabBarIcon: ({ focused, color, size }) => {
              let iconName;
  
              if (route.name === 'Prestataires') {
              iconName = focused
                  ? 'tablet-dashboard'
                  : 'view-dashboard-outline';
              } else if (route.name === 'Menu') {
              iconName = focused ? 'format-list-bulleted' : 'format-list-bulleted';
              } else if (route.name === 'Commande') {
              iconName = focused ? 'cart-arrow-right' : 'cart';
              }
  
              // You can return any component that you like here!
              return <Ionicons  name={iconName} size={20} color={color} />;
          },
          })}
          tabBarOptions={{
          activeTintColor: 'red',
          inactiveTintColor: 'black',
          }}
  > 
    <Tab.Screen name="Prestataires" component={Pizza} />
    <Tab.Screen name="Commande" component={Commande} />
    {/* <Tab.Screen name="Test" component={Prestataire} /> */}

  </Tab.Navigator>
    );
}



