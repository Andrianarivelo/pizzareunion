import React, { memo,useState , useEffect } from 'react'
import {TouchableOpacity,View,Text,StyleSheet, Image,AsyncStorage ,StatusBar , Dimensions , Keyboard,ScrollView, KeyboardAvoidingView} from "react-native"
import axios from "axios"
 import Background from "../../components/Background"
import TextInput from "../../components/TextInput"
import SignButton from "../../components/SignButton"; 
import {Container , Content} from 'native-base'
import {ActivityIndicator} from 'react-native-paper'


var {width} = Dimensions.get('window');
var {widthS} = Dimensions.get('screen');
import {Toast} from 'native-base'

import { emailValidator,passwordValidator,credValidator } from "../../core/utils";
import { theme } from "../../core/theme";
import { loginClient, connectedClient } from "../../API/client/authClient";
import { API_URL } from '../../core/config'
import { storeDate, getData, storeData } from '../../core/session'
import {Checkbox} from 'react-native-paper'
import ModalLoading from '../../components/ModalLoading'



const LoginScreen = ({navigation}) =>{
    const [email, setEmail] = useState({ value: "", error: "" });
    const [password, setPassword] = useState({ value: "", error: "" });
    const [keyboardShown, setkeyboardShown] = useState(false)
    const [keyboardeHidden, setkeyboardeHidden] = useState(true)
    const [checked, setChecked] = React.useState(false);
   
    const [loading, setLoading] = useState(false);
    const [error, setError] = useState("");

    useEffect(() => {
      Keyboard.addListener("keyboardDidShow", _keyboardDidShow);
      Keyboard.addListener("keyboardDidHide", _keyboardDidHide);
  
      // cleanup function
      return () => {
        Keyboard.removeListener("keyboardDidShow", _keyboardDidShow);
        Keyboard.removeListener("keyboardDidHide", _keyboardDidHide);
      };
    }, []);
  
    const _keyboardDidShow = () => {
      // alert("Keyboard Shown");
      setkeyboardShown(true)
    };

    const _keyboardDidHide = () => {
      setkeyboardShown(false)
      // alert("Keyboard Hidden");
    };
  
    
    const storeClient = async (value) => {
      try {
        const jsonValue = JSON.stringify(value)
        await AsyncStorage.setItem('client', jsonValue)
      } catch (e) {
        // saving error
      }
    } 

const _handleSign = async() =>{
  if(loading) return ;
  setLoading(true);
    const emailError = emailValidator(email.value)
    const passwordError = passwordValidator(password.value)

      if (emailError || passwordError ) {
        setEmail({ ...email, error: emailError });
        setPassword({ ...password, error: passwordError });
        setLoading(false);
        return; 
    }else{
        let response = await loginClient({
            email : email.value,
            password : password.value
        })
      
      
        if(response == false) {
           
           setEmail({ ...email, error: "Indentifiant incorrect" });
           setPassword({...password,error : "Mot de passe incorrect"})
  
       }else{
         try{
            let cli = await connectedClient(email.value)
           storeClient(cli)
          //  navigation.state.params.express === true ? navigation.navigate("Prestataire") :
           navigation.navigate("MainTabs",{screen:"Prestataire" ,initial:false}) 
         }catch(err){
          console.log(err)
         }
       

       }

      setLoading(false) 
    } 
  
      
}


    return(
      <Background style={styles.container}>
        <ScrollView contentContainerStyle={styles.content}>
            
            <View style={{ backgroundColor:'white' , padding:20,borderRadius:50, alignItems:'center',justifyContent: 'center'}}>
              <Image  
              style={ keyboardShown ? styles.imageTiny :  styles.image}
              source={require("../../../assets/logo.png")}
              resizeMode="contain"
              />
            </View>
            
            <View style={{ flexDirection:'column', alignItems:'center',justifyContent: 'center',marginTop:10 }}>
                <Text style={{ color: 'white',fontSize:18}}>CONNEXION</Text>  
            </View>
    
            <View style={{ flexDirection:'column', marginTop:5,padding:1,width:50 ,alignItems:'center',justifyContent: 'center',backgroundColor:'white'}}/>

            <View style={{height: 20}}/>
            {/* <Text style={{color :"yellow"}}>PIZZA REUNION</Text>
            {navigation.state.params.isNewCli === true && <Text style={{color : "green"}}>Votre compte client a été créer avec succèes</Text>}
            {navigation.state.params.isNewCli === true && <Text style={{color : "green"}}>Vous pouvez maintenant vous connecter</Text>} */}

          <TextInput 
                label ="Email"
                returnKeyType = "next"
                autoCapitalize="none"
                value={email.value}              
                onChangeText={text => {
                  if(emailValidator(text)){
                  return setEmail({value : text, error : emailValidator(text) })
                  }
                  setEmail({value : text, error : ""})
                
                }}
                error = {!!email.error}
                errorText = {email.error}
            />
            <TextInput 
                label ="Mot de passe"
                returnKeyType="done"
                autoCapitalize="none"
                value={password.value}
                onChangeText={text => {
                   if(passwordValidator(text)){
                    return setPassword({value:text,error:passwordValidator(text)})
                   }                  
                  setPassword({value : text, error :""})
                  
                
                }}
                error={!!password.error}
                errorText={password.error} 
                secureTextEntry
            />
            <View style={{ flexDirection: 'row'  }}>
              <View style={{flex:1,flexDirection: 'row' , alignItems:'center'}}>
              <Checkbox
                status={checked ? 'checked' : 'unchecked'}
                color='white'
                uncheckedColor='white'
                onPress={ async() => {
                  setChecked(!checked);
                  await storeDate('stayLogged',!checked)
                }}
              />
              <Text style={{ color: 'white'}}>
                Rester connecté
              </Text>
              </View >
              <View style={styles.forgotPassword}>
                  <TouchableOpacity
                      onPress={() => navigation.navigate("ForgotClient")}
                  >
                      <Text style={{color : "white",alignSelf:'center'}}>Mot de passe oublié</Text>
                  </TouchableOpacity>
              </View>
            </View>

            <SignButton  loading={loading} mode="contained" onPress={_handleSign} disabled={loading}>
                Se connecter
            </SignButton>
              {/* <ModalLoading isVisible={loading}/> */}
            <View style={styles.row}>
                <Text style={{color :"white"}}>Nouveau client ? </Text>
                <TouchableOpacity onPress={() => navigation.navigate("RegisterScreen")}>
                    <Text style={styles.link}>Créer un compte</Text>
                </TouchableOpacity>
            </View>
            <View style={styles.row}>
                <Text style={{color :"white"}}>Plutôt prestataire ? </Text>
                <TouchableOpacity onPress={() => navigation.navigate("LoginPrestataire")}>
                    <Text style={styles.link}>ici</Text>
                </TouchableOpacity>
            </View>
            {/* </ScrollView> */}

        </ScrollView>
            
      </Background>
    )
}

const styles = StyleSheet.create({
    container:{
      paddingTop:StatusBar.currentHeight || 0,
      flex:1,
    },  
    content:{
        flexGrow: 1, 
        justifyContent: 'center',
        alignItems:'center',
        paddingHorizontal:20

    } , 
    forgotPassword: {
      width: "100%",
      alignItems: "flex-end",
      justifyContent: 'center',
      flex:1

      // marginBottom: 24
    },
    row: {
      flexDirection: "row",
      marginTop: 4
    },
    label: {
      color: theme.colors.secondary
    },
    link: {
      fontWeight: "bold",
      // color: 'white',
      color: theme.colors.primary
    },
    image:{
      // maxWidth : 200,
      // maxHeight : 200 ,
      // position: 'absolute',
      // top: StatusBar.currentHeight || 0,
      // marginTop: StatusBar.currentHeight || 0,
      width : width /4 -30,
      height: width /4 -30,
      
      //  marginTop: 10
      },
      imageTiny:{
        width : width /8 -10,
        height: width /8 -10,
      }
  })
  

export default memo(LoginScreen)
