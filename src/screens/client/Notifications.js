import React, { Component, memo } from 'react'
import { Text, View, ImageBackground, Image, ActivityIndicator, StyleSheet ,StatusBar , Dimensions,Animated} from 'react-native'
import { commandeInfo, amountPaiement } from '../../API/client/list'
import { TouchableRipple, Button } from 'react-native-paper'
import { currentClient } from "../../core/session";
import TopMenu from "../../components/TopMenu"
import TopMenuP from "../../components/TopMenuP"
var {width} = Dimensions.get('window')
import {Icon} from 'native-base'

const dynamicHeight = new Animated.Value(0)
const AnimtedImage = Animated.createAnimatedComponent(Image)



class Notifications extends Component {

    constructor(props) {
        super(props)
        this.navigationWillFocusListener  =  props.navigation.addListener('willFocus',async ()=>{
            await this.fetchCli()
            await this.fetchCommandState()
            await this.fetchPaiementInfo()
        })
        this.ref_commande = props.route.params.ref_commande
        
        this.state = {
            notif: {},
            paypal: {},
            client: {},
            isLoading: false
        }
    }



    componentDidMount = async () => {
        this.setState({ isLoading: true })
        // this.ref_commande = this.props.navigation.state.params.ref_commande
        await this.fetchCli()
        await this.fetchCommandState()
        await this.fetchPaiementInfo()
        Animated.timing(dynamicHeight,{
            toValue: width / 3 ,
            easing :Easing.ease,
            duration: 200
          }).start()
        this.setState({ isLoading: false })

    }

    componentWillUnmount = async ()=>{
        this.setState({
            notif: {},
            paypal: {},
            client: {},
            isLoading: false
        })
    }
    fetchCli = async () => {
        let a = await currentClient()
        this.setState({ client: a })
    }

    fetchCommandState = async () => {
        let response = await commandeInfo(this.ref_commande)
        console.log('state' ,response)

        this.setState({ notif: response })
    }

    fetchPaiementInfo = async () => {
        this.setState({ isLoading: true })
        let paiementInfo = await amountPaiement(this.ref_commande)
        this.setState({ paypal: paiementInfo, isLoading: false })

    }
    goToPay = () => {

        this.props.navigation.navigate("Pay", {
            amount: this.state.paypal["total"],
            emailCli: this.state.client["Email"]
        })
    }

    renderNotification = () => {

        if (this.state.notif === false) {
            return (
                <View>
                    <Text style={{ color: "yellow" }}> Un de nos prestataire va recevoir votre commande</Text>
                </View>
            )
        } else if (this.state.notif === "annuler") {
            return (
                <View>
                    <Text style={{ color: "yellow" }}>Désolé, votre commande a été annuler</Text>
                </View>
            )
        } else {
            return (
                <View>
                    <Button labelStyle={{ color: "#000" }} style={{ backgroundColor: "white", marginTop: 10 }} mode="contained" onPress={() => { this.goToPay() }}>
                        Prodécer au paiement
                </Button>
                </View>
            )
        }
    }




    render() {
        return (

            // <ImageBackground
            //     source={require("../../../assets/loginBg.jpg")}
            //     style={{ flex: 1, width: "100%" }}
            // >
            <View style={{ flex: 1 }}>
                {this.state.isLoading &&
                <View style={styles.loading_container}>
                    <ActivityIndicator size="large" />
                </View>}

                <TopMenuP navigation={this.props.navigation} title="Mes commandes" />

                <View style={{ flex: 1, padding: 20, justifyContent: "center" }}>
                    <View style={{ flex: 1 ,flexDirection: "column", alignItems: "center" }}>
                       <AnimatedImage source={require('../../../assets/successActive.png')} style={{width:dynamicHeight,width:dynamicHeight}}/>

                    </View>
                <View style={{ height: width/4-60}}/>


                    <View elevation={5} style={{                         
                         shadowColor: "#000000",
                         shadowOpacity: 0.8,
                         shadowRadius: 2,
                         shadowOffset: {
                           height: 1,
                           width: 1,  },                       
                        flex: 1 ,flexDirection: 'row' , justifyContent: 'center', alignItems:'center' ,borderRadius: 10 }}>
                        

                        {this.state.notif === "annuler" &&
                            <View>
                                <Text style={{ color: "yellow" }}>Désolé, votre commande a été annuler</Text>
                            </View>
                        }
                        {this.state.notif === false &&
                            <View>
                                <Text style={{ color: "black" }}> Un de nos prestataire va recevoir votre commande</Text>
                            </View>
                        }

                        { this.state.notif  !== "annuler"  && this.state.notif  !== false &&

                            <View style={{ flex: 1 , flexDirection: 'column' ,alignItems: 'center'}}>                                 
                                <Text>
                                    Votre commande
                                    ref : {this.ref_commande}
                                </Text>
                                <Text style={{ }}>
                                    Total : {this.state.paypal.total} €

                                </Text>
                                <Button labelStyle={{ color: "#000" }} style={{ backgroundColor: "white", marginTop: 10 }} mode="contained" onPress={() => { this.goToPay() }}>
                                    Prodécer au paiement
                                </Button>
                            </View>
                            
                        }
                    </View>
                    <View style={{ flex: 1}}>
                        <Button onPress={async () => await this.fetchCommandState()} style={{ backgroundColor: "rgba(253, 165, 0,0.8)", marginTop: 10 }} mode="contained">Actualiser</Button>

                    </View>


                </View>

            </View>                
        //  </ImageBackground>

        )
    }
}

const styles = StyleSheet.create({
    loading_container: {
        position: 'absolute',
        zIndex: 10,
        left: 0,
        right: 0,
        top: 100,
        bottom: 0,
        alignItems: 'center',
        justifyContent: 'center'
    }
})
export default memo(Notifications)
