import React, { Component, memo } from 'react'
import { Text, View, ImageBackground , StyleSheet, Dimensions , StatusBar , Image , Modal,TouchableOpacity} from 'react-native'
import { commandeAPayer, commandeEnAttente, achatReussi } from '../../API/client/list'
import {  ScrollView } from 'react-native-gesture-handler'
import TabBar from "@mindinventory/react-native-tab-bar-interaction";
import { List, Avatar, Button } from 'react-native-paper';
import DateTimePicker from '@react-native-community/datetimepicker';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome'
import {Icon} from 'native-base'
import TextInput from '../../components/TextInput'
import { faCalendar, faClock, faPlus, faRemoveFormat } from '@fortawesome/free-solid-svg-icons'
var {width} = Dimensions.get('window')
import {
    widthPercentageToDP as wp,
    heightPercentageToDP as hp,
} from 'react-native-responsive-screen'
import { currentClient } from "../../core/session";
import TopMenu from "../../components/TopMenu"
import { repasserCommande } from '../../API/client/commande';
import { MENU_URI } from '../../core/config';
import { isNotNull, numberValidator } from '../../core/utils';
class Commande extends Component {

    constructor() {
        super()
        this.state = {
            apayer: [],
            enAttente: [],
            reussi: [],
            client: "",            
            codePostale:{ value:'',error: ''},
            ville:{ value:'',error: ''},
            modalLoc: false,

            isOpenDate: false,
            isOpenTime: false,

            dateRecep: new Date(),
            heureRecep: new Date(),
            isSurPlace: true,
            paniers: [],
            repassPanier: [],
        }

    }

    componentDidMount = async () => {
        await this.fetchCli()
        await this.fetchCommandeAPayer()
        await this.fetchCommandeEnAttente()
        await this.fetchAchatReussi()
        await this.initPanier()

        console.log(this.state.paniers)

    }

    async initPanier() {
        let tempPanier = []
        this.state.reussi.map((p, i) => {
            tempPanier.push(p.Code_facture)
        })
        this.setState({ paniers: tempPanier })
    }
    _formatTime(date) {
        let hour = date.getHours()
        let minutes = date.getMinutes()

        hour = hour < 10 ? "0" + hour : hour
        minutes = minutes < 10 ? "0" + minutes : minutes

        return hour + ":" + minutes
    }

    _formatDate(date) {
        if(date !== undefined && date !== undefined){
        let year = date.getFullYear();
        let month = date.getMonth() + 1;
        let dt = date.getDate();

        if (dt < 10) {
            dt = '0' + dt;
        }
        if (month < 10) {
            month = '0' + month;
        }

        return year + '-' + month + '-' + dt
        }
    }

    fetchCli = async () => {
        let a = await currentClient()
        console.log('client',a)
        this.setState({ client: a })
    }

    fetchCommandeAPayer = async () => {
        if(this.state.client !== null){
            let id_client = this.state.client["id_client"]
            let response = await commandeAPayer(id_client)
            if(response !== null && response.length > 0){
                console.log('payer',response)
                response.forEach(element => {
                    let imageNamePath = element["file"].split("/")
                    let a = imageNamePath[0] + "/menus/" + imageNamePath[1]
                    element['image'] = a
        
                    // console.log('commande',this.state.commande)                
                });
        
                response === "vide" ? this.setState({ apayer: undefined }) : this.setState({ apayer: response })

            }
        }        
    }

    fetchAchatReussi = async () => {

        if(this.state.client !== null){
            let id_client = this.state.client["id_client"]

            let response = await achatReussi(id_client)
            // const cmd = response
            if(response !== undefined && response.length >0){
                response.forEach(element => {
                    let imageNamePath = element["file"].split("/")
                    let a = imageNamePath[0] + "/menus/" + imageNamePath[1]
                    element['image'] = a
    
                
                });
                console.log('commande',response)    
    
                response !== "vide" ? this.setState({ reussi: response }) : this.setState({ reussi: undefined })
              
                
            }       
        }
    }

    fetchCommandeEnAttente = async () => {
        if(this.state.client !== null){
            let id_client = this.state.client["id_client"]
            let response = await commandeEnAttente(id_client)
            if(response !== undefined && response.length > 0){
                const cmd = response
                cmd.forEach(element => {
                    let imageNamePath = element["file"].split("/")
                    let a = imageNamePath[0] + "/menus/" + imageNamePath[1]
                    element['image'] = a
        
                    // console.log('commande',this.state.commande)
                
                });
        
                cmd !== "vide" ? this.setState({ enAttente: cmd }) : this.setState({ enAttente: undefined })
        

            }
           


        }
       
    }



    renderEnAttente = (commande) => (
        <List.Item
            style={{ backgroundColor: "rgba(211, 211, 211,0.5)", marginHorizontal: 10 , marginVertical: 5 , borderRadius: 10}}
            title={'ref :'+ commande.ref + ' : '+ commande['Nom_menu_commnder']}
            description={commande['Date_du_commande']}
            titleStyle={{ fontWeight: "bold" }}
            //  descriptionStyle={}
            left={props => <Avatar.Image  source={{uri: `${MENU_URI}${commande.image}`}} size={70} />}

        />


    )

    renderAPayer = (commande) => {
        return <List.Item

            style={{ backgroundColor: "rgba(211, 211, 211,0.5)", marginHorizontal: 10 , marginVertical: 5 , borderRadius: 10}}
            title = {commande['ref_commande'] + " : " + commande['Nom_menu_commnder']}
            description={commande['Date_du_commande'] }
            titleStyle={{ fontWeight: "bold" }}
            //  descriptionStyle={}
            left={props => <Avatar.Image source={{uri:`${MENU_URI}${commande.image}`}} size={70} />}

        />
    }

    isInRepass = (commande)=>{
        // alert(' repass')
        let find = this.state.repassPanier.findIndex( o => o.ref_commande  === commande.ref_commande )
        // alert(find)
        if(find !== -1){
            return true
        }else{
            return false
        }


    }

    renderReussi = (commande) => {
        return <List.Item

            style={{ backgroundColor: this.isInRepass(commande) ? 'rgb(253, 165, 0)' : "rgba(211, 211, 211,0.5)", borderRadius: 10 , marginHorizontal: 10 , marginVertical: 5 }}
            title={commande['Nom_menu_commnder']} 
            // description= { 'Qt :'+ commande['Quantiter_menu_commnder'] + ' = ' +commande['Prix_du_commande'] + ' €'}
            description ={ ()=> 
                <View style={{ flexDirection: 'column'}}>
                    <Text style={{color: 'grey'}}>Quantité : {commande['Quantiter_menu_commnder']} </Text>
                    <Text style={{color: 'grey'}}>Prix: {commande['Prix_du_commande']} €</Text>

                </View>}
            titleStyle={{ fontWeight: "bold" }}
            //  descriptionStyle={}
            left={props => <Avatar.Image source={{uri:`${MENU_URI}${commande.image}`}} size={70} />}
            right={props => <TouchableOpacity onPress={() => this._toogleRepass(commande.ref_commande)}>
                {/* <Text>choisir</Text> */}
                {/* <FontAwesomeIcon color="#1D2935" size={24} icon={ this.isInRepass(commande) ? faDele : faPlus} /> */}
                <Icon name={this.isInRepass(commande) ? "remove" : "add"} type="MaterialIcons"  size={35} color={"#33c37d"}/>


            </TouchableOpacity>}

        />
    }

    _toogleRepass = ref_commande => {
        // console.log(' commande',commande)


        let tempPanier = []

        // // console.log(' find',this.state.repassPanier)

        let  find = this.state.repassPanier.findIndex( (o) => o.ref_commande === ref_commande)
            console.log(' find',find)
        // console.log('repass',tempPanier)
        if(  find !== -1){
            this.setState({ repassPanier: [] })
        }else{
            this.state.reussi.forEach(element => {
                if(element.ref_commande === ref_commande){
                     tempPanier.push(element)
                }
                
            });
            this.setState({ repassPanier: tempPanier })
        }

        console.log(' repass' ,this.state.repassPanier)


        // if (tempPanier.includes(codeFacture)) {
        //     const indexOfCodeFactureInsidePaniers = tempPanier.findIndex((e) => e == codeFacture)
        //     tempPanier.splice(indexOfCodeFactureInsidePaniers, 1)
        //     this.setState({ repassPanier: tempPanier })
        //     console.log(' repass',tempPanier)

        // } else {
        //     tempPanier.push(codeFacture)
        //     this.setState({ repassPanier: tempPanier })
        // }
    }


    sendCommande = async () => {
            // this.state.repassPanier[0].ref_commande,
            // alert(this.state.repassPanier[0].ref_commande)

        let qt =[]
        let paniers =[]
        this.state.repassPanier.forEach(element => {
            qt.push(element['Quantiter_menu_commnder'])
            paniers.push(element.Code_facture)
        }); 
        alert(this._formatDate(this.state.dateRecep))

        
        console.log('quantité',qt)
        let response = await repasserCommande(
            paniers,
            this.state.repassPanier[0].ref_commande,            
            this._formatDate(this.state.dateRecep),
            this._formatTime(this.state.heureRecep),
            this.state.isSurPlace,
            qt,
            ville,
            codePostale
        )

            alert(response)
        console.log(response)
    }


    render() {

        if(this.state.client === null  ){
            return(

                <View style={{ flex: 1}}>
                    <View style={{ flexDirection :'row' , backgroundColor:'white', alignItems: 'center', justifyContent: 'center' , paddingTop: StatusBar.currentHeight || 0}}>
                        <Text style={{fontSize:32,fontWeight:"bold",color:"rgb(253, 165, 0)" }}>Commande</Text>
                    </View>

                <View style={{ flex: 1 , flexDirection: 'column',textAlign:'center', backgroundColor: 'white' , alignItems: 'center' , justifyContent: 'center',padding: 20}}>

                    <Text style={{ textAlign: 'center' , fontSize:  16}} >
                        Veuillez vous conncecter pour pouvoir accéder a ce contenue
                    </Text>
                   
                <Icon type='MaterialIcons' name='lock' style={{ fontSize: 70, color:'black'}} />

                </View>
            </View>

            )
        }
        return (

            <TabBar bgNavBar="white" bgNavBarSelector="white" stroke="skyblue">
                <TabBar.Item
                    icon={require('../../../assets/attente.png')}
                    selectedIcon={require('../../../assets/attenteActive.png')}
                    title="Tab1"
                    screenBackgroundColor={{ backgroundColor: 'transparent' }}
                >
                    <View>
                        <View>
                            {/* <TopMenu title="En Attente" navigation={this.props.navigation} /> */}
                            <View style={{ flexDirection :'row' , alignItems: 'center', justifyContent: 'center' , paddingTop: StatusBar.currentHeight || 0}}>
                                <Text style={{fontSize:32,fontWeight:"bold",color:"rgb(253, 165, 0)" }}>En Attente</Text>
                            </View>
                        </View>
                        <View style={{ maxHeight: hp('75%')  }}>
                            {/* <View style={{  }}> */}
                            {this.state.enAttente.length > 0 ?
                                <ScrollView>
                                    {this.state.enAttente !== undefined && this.state.enAttente.map((commande, i) => {
                                        return <TouchableOpacity style={{}} key={i} onPress={() => this.props.navigation.navigate("Notifications", { ref_commande: commande.ref })}>
                                            {this.renderEnAttente(commande)}
                                        </TouchableOpacity>
                                    })}
                                </ScrollView>
                                :
                            <View style={{ marginVertical: hp("30%") , alignItems: 'center'}}>
                                <Image source={require('../../../assets/empty.png')}  style={{ width: width /4 , height: width/4}}/>
                                <Text style={{ alignItems: 'center'}}>
                                    Commande vide
                                </Text>
                            </View>
                            }
                                {/* <View style={{ flex: 1, alignItems: "center" }}>
                                    <Text style={{ color: "white", fontWeight: "bold", fontSize: 18, textAlign: "center" }}>Commandes en attente</Text>
                                </View> */}

                            {/* </View> */}
                        </View>
                    </View>

                </TabBar.Item>
                <TabBar.Item
                    icon={require('../../../assets/success.png')}
                    selectedIcon={require('../../../assets/successActive.png')}
                    title="Tab2"
                    // screenBackgroundColor={{ backgroundColor: '#201b1b' }}
                    screenBackgroundColor={{ backgroundColor: 'transparent' }}

                >
                    <View style={{ }}>
                        {/* <View>
                            <TopMenu title="Achats réussis" navigation={this.props.navigation} />
                        </View> */}
                        <View style={{ flexDirection :'row' , alignItems: 'center', justifyContent: 'center' , paddingTop: StatusBar.currentHeight || 0}}>
                                <Text style={{fontSize:32,fontWeight:"bold",color:"rgb(253, 165, 0)" }}>Réussi</Text>
                        </View>
                        <View style={{ maxHeight: hp('75%') }}>
                        {/* <View style={{ height: 20}}/> */}
                            {this.state.reussi.length > 0 ?
                            <ScrollView>
                                {/* MODAL LOCALISATION */}
                <Modal
                    animationType="fade"
                    transparent={true}
                    visible={this.state.modalLoc}

                    onRequestClose={() => {
                        this.setState({
                            ville:{value:'',error: '' },
                            codePostale:{value:'',error: ''}                            
                        })
                    }}
                >
                    <View style={styles.centeredView}>
                    <View style={styles.modalView}>

                    <TextInput 
                        style={{ minWidth:150}}
                        label ="Ville"
                        returnKeyType = "done"
                        autoCapitalize="none"
                        value={this.state.ville.value}              
                        onChangeText={text => this.setState({ville: {value:text,error:''}}) }
                           
                        error = {!!this.state.ville.error}
                        errorText = {this.state.ville.error}
                    />
                    <TextInput 
                        style={{ minWidth:150}}
                        label ="Code postale"
                        returnKeyType="done"
                        autoCapitalize="none"
                        value={this.state.codePostale.value}
                        onChangeText={text => this.setState({codePostale:{value: text, error: ''}})}                        
                        error={!!this.state.codePostale.error}
                        errorText={this.state.codePostale.error} 
                    />

                        {/* <Text style={styles.modalText}>Vous devez vous connecter pour pouvoir effectuer une commande</Text> */}

                        <View style={{  flexDirection: 'row' , paddingTop:  20}}>
                        <TouchableOpacity
                        style={{ ...styles.openButton, backgroundColor: "#FDA500" }}
                        onPress={() => {
                           this.setState({modalLoc : !this.state.modalLoc})
                        }}
                        >
                        <Text style={styles.textStyle}>Annuler</Text>
                        </TouchableOpacity>
                        <View style={{width: 20}}/>
                        <TouchableOpacity
                        style={{ ...styles.openButton, backgroundColor: "#FDA500" }}
                        onPress={() => {
                            let villeError =  isNotNull(this.state.ville.value)
                            let codePostaleError  = numberValidator(this.state.codePostale.value)
                            if( villeError || codePostaleError ){
                                this.setState({codePostale: {value: this.state.codePostale.value,error: codePostaleError }})
                                this.setState({ville: {value: this.state.ville.value,error: villeError}})
                            
                            }else{
                                this.setState({modalVisible : !this.state.modalLoc})
                                this.sendCommande()
                            }                           

                        }}
                        >
                        <Text style={styles.textStyle}>VALIDER</Text>
                        </TouchableOpacity>

                        </View>

                       
                    </View>
                    </View>
                </Modal>
                
                {/* MODAL LOCALISATION */}
                            <View style={{  }}>

                            <View style={{  marginHorizontal : 10}}>
                                <View style={{ backgroundColor: ' white' ,flexDirection: "row" , alignItems: 'center',justifyContent: 'space-between'  }}>
                                    <TouchableOpacity onPress={()=> this.setState({isOpenDate : true})} style={{ flexDirection: "row",alignItems : "center" }}>
                                        <FontAwesomeIcon color="#1D2935" size={24} icon={faCalendar} />
                                        <Text style={{color : "#1D2935" ,paddingLeft: 10}} >Date de réception</Text>
                                    </TouchableOpacity>
                                    <Text style={{color : "#1D2935" , fontWeight:'bold'}}>{this._formatDate(this.state.dateRecep)}</Text>
                                </View>
                                <View style={{height:5, backgroundColor : 'transparent'}}/>
                                <View>
                                <View style={{ flexDirection: "row", justifyContent: "space-between" }}>
                                    <TouchableOpacity onPress={()=> this.setState({isOpenTime : true})} style={{ flexDirection: "row",alignItems : "center" }}>
                                        <FontAwesomeIcon color="#1D2935" size={24} icon={faClock} />
                                        <Text style={{color : "#1D2935",paddingLeft: 10}} >Heure de réception</Text>
                                    </TouchableOpacity>
                                    <Text style={{color : "#1D2935" , fontWeight:'bold'}}>{this._formatTime(this.state.heureRecep)}</Text>
                                </View>
                                </View>
                                <View style={{height: 5 , backgroundColor : 'transparent'}}/>


                                <View style={{flexDirection : "row",justifyContent : "space-evenly"}}>
                                    <TouchableOpacity onPress={()=> this.setState({isSurPlace : true})} style={styles.button,this.state.isSurPlace === true ? styles.buttonFocused : styles.button}>
                                        <Text  style={styles.button,this.state.isSurPlace === true ? {color: 'white'}: {color:'black'}}>Sur place</Text>
                                    </TouchableOpacity>
                                    <TouchableOpacity onPress={()=> this.setState({isSurPlace : false})} style={this.state.isSurPlace === false ? styles.buttonFocused : styles.button}>
                                        <Text style={styles.button,this.state.isSurPlace === true ? {color: 'black'}: {color:'white'}}>A emporter</Text>
                                    </TouchableOpacity>
                                </View>
                                <View style={{height: 5 , backgroundColor : 'transparent'}}/>



                                {this.state.isOpenDate === true &&
                                    <DateTimePicker
                                        testID="dateTimePicker"
                                        timeZoneOffsetInMinutes={0}
                                        value={this.state.dateRecep}
                                        mode="date"
                                    
                                        display="default"
                                        onChange={(event, selectedDate) => {
                                            this.setState({ isOpenDate: false })
                                            // alert(selectedDate)
                                            if(selectedDate !== undefined)  this.setState({dateRecep:selectedDate})
                                        }}
                                    />
                                }

                            {this.state.isOpenTime === true &&
                                <DateTimePicker
                                    testID="dateTimePicker"
                                    timeZoneOffsetInMinutes={0}
                                    value={this.state.heureRecep}
                                    mode="time"
                                    is24Hour={true}
                                    display="default"
                                    onChange={(event, selectedDate) => {
                                        this.setState({ isOpenTime: false })
                                        if(selectedDate !== undefined) this.setState({heureRecep : selectedDate})
                                    }}
                                />
                            }

                        </View>

                            </View>
                                {this.state.reussi !== undefined && this.state.reussi.map((commande, i) => {

                                    return <View key={i} onPress={() => this.props.navigation.navigate("Notifications", { ref_commande: commande.ref_commande })}>
                                        {this.renderReussi(commande)}
                                    </View>
                                })}
                            </ScrollView>
                            
                                :
                                
                                <View style={{ marginVertical: hp("30%") , alignItems: 'center'}}>
                                    <Image source={require('../../../assets/empty.png')}  style={{ width: width /4 , height: width/4}}/>
                                    <Text style={{ alignItems: 'center'}}>
                                        Commande vide
                                    </Text>
                                </View>
                                }
                        {this.state.repassPanier.length > 0 ?
                            <TouchableOpacity  onPress={() => this.setState({modalLoc:true}) } style={{
                        // backgroundColor:"#33c37d",
                                backgroundColor:"rgb(253, 165, 0)",                    
                                width:width-40,
                                alignItems:'center',
                                padding:10,
                                borderRadius:5,
                                margin:10
                            }}>
                            <Text style={{
                                fontSize:24,
                                fontWeight:"bold",
                                color:'white'
                                }}>
                                Repasser les commandes
                            </Text>
                            </TouchableOpacity>
                        :null}

                        


                        </View>
                        {/* <Button labelStyle={{ color: "#000" }}
                            style={{
                                backgroundColor: "white",
                                marginTop: 10
                            }} mode="contained"
                            onPress={() => this.sendCommande()}>
                            Repasser les commandes
                        </Button> */}
                        



                    </View>

                </TabBar.Item>
                <TabBar.Item

                    icon={require('../../../assets/toPay.png')}
                    selectedIcon={require('../../../assets/toPayActive.png')}
                    title="Tab3"
                    screenBackgroundColor={{ backgroundColor: 'transparent' }}
                >
                    <View style={{ backgroundColor : 'transparent'}}>
                        {/* <View>
                            <TopMenu title="A payer" navigation={this.props.navigation} />
                            
                        </View> */}
                        <View style={{ flexDirection :'row' , alignItems: 'center', justifyContent: 'center' , paddingTop: StatusBar.currentHeight || 0}}>
                                <Text style={{fontSize:32,fontWeight:"bold",color:"rgb(253, 165, 0)" }}>A payer</Text>
                        </View>
                        {/* <View style={{}}> */}
                            {/*Page Content*/}
                            <View style={{ maxHeight: hp("75%") }}>
                                {this.state.apayer.length > 0 ?
                                <ScrollView>
                                    {this.state.apayer !== undefined && this.state.apayer.map((commande, i) => {
                                        return <TouchableOpacity key={i} onPress={() => this.props.navigation.navigate("Notifications", { ref_commande: commande['ref_commande'] }) }>
                                            {this.renderAPayer(commande)}
                                        </TouchableOpacity>
                                    })}
                                </ScrollView>
                                

                                :

                                <View style={{ marginVertical: hp("30%") , alignItems: 'center'}}>
                                    <Image source={require('../../../assets/empty.png')}  style={{ width: width /4 , height: width/4}}/>
                                    <Text style={{ alignItems: 'center'}}>
                                        Commande vide
                                    </Text>
                                </View>
                                 } 
                            </View>


                        </View>
                    {/* </View> */}

                </TabBar.Item>
            </TabBar>

        )
    }
}

const styles = StyleSheet.create({

    buttonFocused:{
        backgroundColor : "#1D2935",
        padding :10,
        alignItems: 'center',
        justifyContent: 'center',
        shadowOffset: {
            width: 0,
            height: 10
          },
        height : ((width/2)-20)-120,
        borderRadius: 5,
        

      }, 
      button:{
        backgroundColor : "#d3d3d3",
        padding :10,
        alignItems: 'center',
        justifyContent: 'center',
        shadowOffset: {
            width: 0,
            height: 10
          },
          height : ((width/2)-20)-120,
          borderRadius: 5
      },
      openButton: {
        backgroundColor: "#F194FF",
        borderRadius: 20,
        padding: 10,
        elevation: 2
      },
      centeredView: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        // marginTop: 22
      },
      modalView: {
        margin: 20,
        backgroundColor: "rgba(255,255,255,0.8)",
        borderRadius: 20,
        padding: 35,
        alignItems: "center",
        shadowColor: "#000",
        shadowOffset: {
          width: 0,
          height: 2
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5
      }
})


export default memo(Commande)
{/* {this.state.apayer !== undefined && this.state.apayer.map((commande ,i) =>{
                  return   <TouchableOpacity key ={i} onPress={()=> this.props.navigation.navigate("Notifications",{ref_commande : commande.ref_commande})}>
                        <Text>{commande.ref_commande}</Text>
                    </TouchableOpacity>
                })} */}