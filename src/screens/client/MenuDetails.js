import React, { Component, memo } from 'react'
import { Dimensions, View, ImageBackground, ActivityIndicator, Image, StatusBar, ToastAndroid, StyleSheet , FlatList ,Text } from 'react-native'
import { pizzaByPrestataire, allPrestataire } from '../../API/client/list'
import { TouchableOpacity, ScrollView } from 'react-native-gesture-handler'
import { addToPanier } from '../../API/client/commande'
import Background from '../../components/Background'
import { MENU_URI, API_URL } from '../../core/config'
import {
    widthPercentageToDP as wp,
    heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
var {height , width} = Dimensions.get('window');
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import { faUser, faCartPlus, faCommentsDollar, faList, faListAlt, faPizzaSlice } from '@fortawesome/free-solid-svg-icons';

import { currentClient  , getData} from "../../core/session";
import TopMenu from "../../components/TopMenu";
import { noterPrestataire } from '../../API/client/authClient';
import { Rating, AirbnbRating } from 'react-native-ratings';
import { Container, Header, Content, Card, CardItem,Thumbnail, Icon ,Button, Left, Body, Right } from 'native-base';
// import {Icon as IconIonics} from 'react-native-vector-icons/Ionicons'
import * as Font from 'expo-font'
import { AppLoading } from 'expo';
import { Ionicons } from '@expo/vector-icons';
import AsyncStorage from '@react-native-community/async-storage';
import { createBottomTabNavigator } from 'react-navigation-tabs';
import { createAppContainer } from 'react-navigation'
import Panier from './Panier'
import Pizza from './Pizza'





class MenuDetails extends Component {
    constructor(props) {
        super(props)

        this.navigationWillFocusListener  =  props.navigation.addListener('willFocus',async ()=>{
          await this._getCart()
      })
        this.state = {
            pizzas: [ ],
            paniers:[],
            types:[],
            cat:[],
            prestataire:{},
            selectedCatg :null,
            banner:[],
            client: "",
            isLoading: false,
            note: 1,
            selectedId: 0,
            isReady: false
            
        }
    }
   

    async componentWillUnmount(){
      // this.navigationWillFocusListener.remove()
      this.setState({
        pizzas: [ ],
        paniers:[],
        types:[],
        cat:[],
        prestataire:{},
        selectedCatg :null,
        banner:[],
        client: "",
        isLoading: false,
        note: 1,
        selectedId: 0,
        isReady: false
      })
    }

    async componentDidMount() {
        await Font.loadAsync({
          Roboto: require('native-base/Fonts/Roboto.ttf'),
          Roboto_medium: require('native-base/Fonts/Roboto_medium.ttf'),
          ...Ionicons.font,
        });
        this.setState({ isReady: true });
        this.setState({ isLoading: true });
        await this._getCurrentPresta()
        await this._getCart();
        await this.fetchPizzaOwn()

        // await this._getAllTypes()
      }
  

    _getCurrentPresta = async () =>{
        // alert('ok')
        var prestataire = this.props.route.params.currentPrestataire
        this.setState({prestataire :prestataire},()=>console.log('okaaa',this.state.prestataire))
    }

    _getAllTypes = async ()=>{
        var types =[]
            this.state.pizzas.forEach(pizza => {
                types.forEach(type => {
                    if(pizza.Type === type){
                        return
                    }else{
                        types.push(pizza.Type)
                    }
                });            
            });
        
        this.setState({types})
        console.log('types',types)
    }


    fetchCli = async () => {
        let a = await currentClient()
        this.setState({ client: a })
    }


    fetchPizzaOwn = async () => {
        let piz = await pizzaByPrestataire(this.state.prestataire.Num_prestataire)
        console.log(piz)
        this.setState({ pizzas: piz } , ()=> this.setState({isLoading : false}))
    }

    addPanier = async (Code_produit) => {
        this.setState({ isLoading: true })
        let id_client = this.state.client["id_client"]
        let Num_prestataire = this.props.navigation.state.params.Num_prestataire

        let res = await addToPanier({ Code_produit, id_client, Num_prestataire })

        console.log(res)

        res === "yet" ?
            ToastAndroid.show("Ajouté au panier", ToastAndroid.SHORT)
            : ToastAndroid.show("Déjà dans le panier", ToastAndroid.SHORT)
        this.setState({ isLoading: false })
    }

    _addCart = async (item) =>{
        const itemCart ={
            pizza : item,
            quantity : 1,
            price : item.Prix
        }

        AsyncStorage.getItem('cart').then( (dataCart) =>{
          console.log(JSON.parse(dataCart))
            if(dataCart !== null){
                const cart = JSON.parse(dataCart)
                cart.push(itemCart)
                AsyncStorage.setItem('cart',JSON.stringify(cart))
                this._getCart()

            }else{
                const cart =[]
                cart.push(itemCart)
                AsyncStorage.setItem('cart',JSON.stringify(cart))
                this._getCart()

            }
        // alert('add successful')

        })
        .catch((error) =>{
            console.log(error)
        })
    }

    _getCart = async ()=>{
      let a = await getData('cart')
      this.setState({paniers: a})
    }
    

    renderPizza = (id, nom_menu, description, image, prix) => (
        <List.Item
            key={id}
            style={{ backgroundColor: "#d3d3d3", margin: 10 }}
            title={nom_menu}
            description={description}
            titleStyle={{ fontWeight: "bold" }}
            //  descriptionStyle={}
            left={props => <Avatar.Image source={{ uri: `${MENU_URI}/${image}` }} size={70} />}
            right={props => <TouchableOpacity style={{ ...props }, { alignItems: "center" }} onPress={() => this.addPanier(id)}>
                <Text style={{ fontWeight: "bold" }}>{prix} Euro</Text>
                <FontAwesomeIcon size={20} icon={faCartPlus} />
            </TouchableOpacity>}
        />
    )

    evaluate = async () => {
        let idCli = this.state.client["id_client"]
        let num_prestataire = this.props.navigation.state.params.Num_prestataire
        let note = this.state.note
        let response = await noterPrestataire(idCli, num_prestataire, note)
    }

    isAlreadyInCart =  (item) =>{
      if(this.state.paniers !== null){
        let find = this.state.paniers.find(o => o.pizza['Code_produit'] === item['Code_produit']);
        if(find !== undefined) return true
        return false
      }
      return false    

    }

    _renderItemFood(item){
      return(    
        <TouchableOpacity style={styles.mainContainer}>      
            <View style={styles.divFood}>
                <Image style={[styles.divImage, styles.divImage1]}        
                    resizeMode="contain"
                    source={{ uri: `${MENU_URI}${item['file']}`}}/> 
                <View style={{height:((width/2)-20)-80, backgroundColor:'transparent', width:((width/2)-20)-10}} />
                <Text style={{fontWeight:'bold',fontSize:18, textAlign: 'center'}}>
                 {item['Nom_menu']}
                </Text>
                <Text style={{ textAlign: 'center'}}>{item.Description}</Text>

                <View style={{ flex: 1 , flexDirection:'row' , margin: 10}}>
                    <View style={{ flex : 0.5 , flexDirection: 'column' }}>
                        <View style={{ flex:1 , flexDirection: 'row' , alignItems: 'center', justifyContent:'flex-start'}}>
                            <Text style={{fontSize:20,color:"rgb(253, 165, 0)"}}>{item.Prix}€</Text>
                        </View>
                    </View>
                    <View style={{ flex:1 , flexDirection:'column'}}>
                      { this.isAlreadyInCart(item)  === true ? 
                       <Button iconRight light style={{ width : ((width/2)-20)-70 , height: ((width/2)-20)-130 , backgroundColor : '#CBD0D4' }} onPress={ () => this.props.navigation.navigate('Cart')}>
                        <Text style={{ color :'white' , paddingLeft: 10}}> Panier </Text>
                        <Icon name="ios-cart" style={{fontSize: 20, color: 'white'}}/>
                       </Button>
                         : 
                         <Button iconRight light style={{ width : ((width/2)-20)-70 , height: ((width/2)-20)-130 , backgroundColor : 'rgba(253, 165, 0,0.8)' , padding: 10}} onPress={ () => this._addCart(item)} >
                         <Text style={{ color :'white'}}>Ajouter</Text>
                         <FontAwesomeIcon icon={faCartPlus}  color={'white'}  size={20} />
                        </Button>
                      }                         
                    </View>
                </View>
            </View>        
        </TouchableOpacity>         
        )
    }

    _renderItem(item){
        return(
          <TouchableOpacity style={[styles.divCategorie,{ backgroundColor : this.state.selectedCatg === item &&  this.state.selectedCatg !== null  ?  'rgb(253, 165, 0)' :   'white'    }]}
          onPress={()=>{
                if( this.state.selectedCatg === item) return this.setState({selectedCatg: null})
                this.setState({selectedCatg : item})            
              }}>
            {/* <Image
              style={{width:100,height:80}}
              resizeMode="contain"
              source={{uri : item.image}} /> */}
              <Text style={{fontWeight:'bold',fontSize:22}}>{item}</Text>

          </TouchableOpacity>
        )
      }


    render() {

        if (this.state.isReady && this.state.isLoading) {
            return <AppLoading />;
          }
        
          return (
            <View style={{ flex: 1,
              // backgroundColor:"#f2f2f2" 
              backgroundColor:"white" ,
              marginTop : StatusBar.currentHeight || 0
            
            }}>
              <View style={{  alignItems:'center' , backgroundColor: 'transparent' }} >
                <Image style={{height: 40,width:width/2}} resizeMode="contain" source={require("../../../assets/assietePizza.png")}  />
              </View> 
          
          <View style={styles.container}>
                      

          {/* above */}
          <View  style={{ height:100 , flexDirection: 'row' }} >
              <View style={{ flex:0.7 , flexDirection :'column' }}>
                  <Image style={styles.imagePresta} resizeMode="cover" source={{uri:`${MENU_URI}${this.state.prestataire['nom_file']}`}} />

              </View>

              <View style={{ flex:1 , flexDirection :'column',paddingLeft:10 }}>
                  <View style={{ flex:1 }}> 
                  <Text style={{ fontSize: 22 ,}}>{this.state.prestataire['Nom_etablissement']}</Text> 
                  <Text style={{color: 'black' }}  >{this.state.prestataire['Ville_prestataire']} {this.state.prestataire['Rue']} {this.state.prestataire['Numero_du_rue']} {this.state.prestataire['Postale']}  </Text> 
                  <View style={{height:((width/4)-20)-80, backgroundColor:'transparent', width:((width/2)-20)-10}} />
                  <Text style={{color: 'grey'}}>Ouvert du  {this.state.prestataire['Ouverture']}-{this.state.prestataire['Fermeture']} 
                    { this.state.prestataire['Heure_overture'] && this.state.prestataire['Heure_fermeture']  ? 
                    ' de ' + this.state.prestataire['Heure_overture'] + '-' + this.state.prestataire['Heure_fermeture'] : 
                    ''} 
                  </Text> 

                  </View>
              </View>
          </View>


          <View style={{flex:4, borderRadius:20, paddingVertical:20, backgroundColor:'white'}}>
            {/* <Text style={styles.titleCatg}>Types</Text> */}
            {/* <FlatList
              horizontal={true}
              data={this.state.types}
              renderItem={({ item }) => this._renderItem(item)}
              keyExtractor = { (item,index) => index.toString() }
            /> */}
          {this.state.pizzas.length === 0 ?
            
            <View style={{ flex:1 , heigth : '100'  ,  alignItems: 'center'}}>
              <Text>Aucun menu disponible pour le moment</Text>
              <View style={{height: 20}}/>                
              <TouchableOpacity
              style={{ justifyContent:'center',alignItems:'center', borderRadius: 10 ,width:(width/2)-50 ,height:((width/2)-20)-130 , backgroundColor: '#CBD0D4'}}
              onPress={ () => this.props.navigation.navigate('Prestataire')}
              >
                <Text >
                Voir les prestataires
                </Text>              
              </TouchableOpacity>
            </View>
                   
            :          
              <FlatList
                //horizontal={true}
                // CellRendererComponent={ViewOverflow}
                data={this.state.pizzas}
                numColumns={2}
                renderItem={({ item }) => this._renderItemFood(item)}
                keyExtractor = { (item,index) => index.toString() }
              />
            }
            
            <View style={{height:20}} />
        </View>

      </View>
    </View>


        )
    }
}

// const styles = StyleSheet.create({
//     container: {

//     },
//     loading_container: {
//         position: 'absolute',
//         zIndex: 10,
//         left: 0,
//         right: 0,
//         top: 100,
//         bottom: 0,
//         alignItems: 'center',
//         justifyContent: 'center'
//     }
// })

const styles = StyleSheet.create({
  container: {
    // flex: 1,
    // justifyContent: "center",
    // alignItems: "center"
    flex: 1,
    paddingTop : 10
    // marginTop: StatusBar.currentHeight || 0,
  },
    imageBanner: {
    height:width/2,
    width:width-40,
    borderRadius:10,
    marginHorizontal:20
  },
  divCategorie:{
    margin:5, alignItems:'center',
    borderRadius:10,
    padding:10,
    shadowOpacity:0.3,
    shadowRadius:50,
    borderWidth: 0.2,
    borderColor: 'rgb(253, 165, 0)'

  },
  titleCatg:{
    fontSize:30,
    fontWeight:'bold',
    textAlign:'center',
    marginBottom:10
  },
  divImage: {
    borderRadius: 40,
    width:((width/2)-20)-10,
    height:((width/2)-20)-30,
    backgroundColor:'transparent',
  },
  divImage1: {
    position: 'absolute',
    top: -45,
  },
  mainContainer: {
    flex: 1,
    // alignItems: 'center',
    // justifyContent: 'space-around',
  },
  divFood: {
    backgroundColor:'white',
    width:(width/2)-20,
    padding:10,
    borderRadius:10,
    marginTop:55,
    marginBottom:5,
    marginLeft:10,
    alignItems:'center',
    elevation:8,
    shadowOpacity:0.3,
    shadowRadius:50,
    position: 'relative',
    overflow: 'visible', // doesn't do anything
  },
  extraComponentContainer: {
    // fakes overflow but requires more markup
    backgroundColor: '#ffff00',
    paddingTop: 20,
    paddingLeft: 20,
    paddingBottom: 20,
    paddingRight: 20,
    position: 'relative',
  },
  
  element2: {
    position: 'absolute',
    top: 0,
    left: 0,
    zIndex: 100,
    elevation: 100,
  },
  imagePresta:{
    flex:1,
    borderRadius: 20,
    // flexDirection: 'row',
    // alignItems: 'stretch',
    width:((width/2)-20)-10,
    height:((width/2)-20)-30,
    backgroundColor:'transparent',
  }

});


export default memo(MenuDetails)