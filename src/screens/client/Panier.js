import React, { Component, memo } from 'react'
import { Text, View, ImageBackground, ActivityIndicator, StyleSheet ,StatusBar , Dimensions ,Modal, FlatList , Image , TouchableOpacity} from 'react-native'
import { panierClient, simplePanier } from '../../API/client/list'
import {ScrollView, TouchableHighlight } from 'react-native-gesture-handler'
import { TouchableRipple, Avatar, Button, List, Badge } from 'react-native-paper'
import { commander } from '../../API/client/commande'
import { addToPanier } from '../../API/client/commande'
import {
    widthPercentageToDP as wp,
    heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import { MENU_URI } from "../../core/config";
import TopMenu from "../../components/TopMenu"
import TopMenuP from "../../components/TopMenuP"
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome'
import DateTimePicker from '@react-native-community/datetimepicker';
import { faCalendar, faClock } from '@fortawesome/free-solid-svg-icons'
var {height , width} = Dimensions.get('window');
import AsyncStorage from '@react-native-community/async-storage';
import {Icon} from 'native-base'
import { getData, removeCart } from '../../core/session'
import TextInput from '../../components/TextInput'
import { numberValidator, isNotNull } from '../../core/utils'



class Panier extends Component {
    constructor(props) {
        super(props) 
        this.navigationWillFocusListener  =  props.navigation.addListener('willFocus',async ()=>{
            await this._getCart()
        })
        this.state = {
            modalVisible : false,
            modalLoc : false,
            paniers: [],
            panierSimple: [],
            quantite: [1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
            client: {},
            isLoading: false,
            codePostale:{ value:'',error: ''},
            ville:{ value:'',error: ''},

            isOpenDate: false,
            isOpenTime: false,

            dateRecep : new Date(),
            heureRecep : new Date(),
            isSurPlace : true
        }
            

    }
    async componentDidUpdate(prevProps,prevState){
        // if(this.state.paniers !== prevState.paniers) await this._getCart();
      }

    // shouldComponentUpdate = (nextProps , nextState)=>{
    //     if(this.state.paniers.length !== nextState.paniers.length) {
    //      this.forceUpdate()
    //         return true
    //     }
    //     return false
    // }  
  

    componentDidMount = async () => {
        this.setState({ isLoading: true })
        await this._getCart()
        await this.fetchCli()
        // alert(this.state.paniers.length)
        // await this.fetchMaPanier()
        // await this.fetchPanierSimple()
        this.setState({ isLoading: false })
    }

    async componentWillUnmount(){
        // this.navigationWillFocusListener.remove()
        this.setState({
            modalVisible : false,
            modalLoc: false,
            paniers: [],
            panierSimple: [],           
            client: {},
            isLoading: false,

            isOpenDate: false,
            isOpenTime: false,


        })
    }

    _getCart = async () =>{
        let a = await getData('cart')
        this.setState({paniers: a})        
    }
    

    fetchCli = async () => {
        let c = await getData('client')
        this.setState({client:c})
    }

    fetchMaPanier = async () => {
        let id_client = this.state.client["id_client"]
        let Num_prestataire = this.props.navigation.state.params.Num_prestataire2

        let cart = await panierClient(id_client, Num_prestataire)

        cart !== "vide" ? this.setState({ paniers: cart }) : this.setState({ paniers: undefined })

    }

    fetchPanierSimple = async () => {
        let id_client = this.state.client["id_client"]
        let Num_prestataire = this.props.navigation.state.params.Num_prestataire2
        let cart = await simplePanier(id_client, Num_prestataire)
        this.setState({ panierSimple: cart })
    }

    

    updateQuantite = (i, isMinus) => {
        let initialQt = this.state.quantite
        if (isMinus == false) {
            let newQtAtIndex = ++initialQt[i]

            initialQt.splice(i, 1, newQtAtIndex)
            this.setState({
                quantite: initialQt
            })
        } else {
            let newQtAtIndex = --initialQt[i]
            initialQt.splice(i, 1, newQtAtIndex)
            this.setState({ quantite: initialQt })
        }

    }

    sendCommande = async () => {
        this.setState({ isLoading: true })
        // alert(this.state.ville.value)

        if (this.state.client !== undefined){
            let panier=[]
            this.state.paniers.forEach(el => {
                this.addPanier(el.pizza['Code_produit'],el.pizza['Num_prestataire']) 
                let cart = {...el.pizza}
                cart['id_client'] = this.state.client['id_client']
                panier.push(cart)
            });            
            this.setState({panierSimple : panier} , async()=>{
                let qt = []
                this.state.paniers.forEach(el => {
                    qt.push(el.quantity)
                });
                const now = new Date()
                let ref = Math.round(now.getTime() / 100000)
                let md = this.state.isSurPlace === true ? "sur place" : "A emporter"
                console.log('panier',this.state.panierSimple);
                let response = await commander(
                    this.state.panierSimple,
                    ref,
                    this._formatDate(this.state.dateRecep),
                    this._formatTime(this.state.heureRecep) ,
                    md,
                    qt,
                    this.state.ville.value,
                    this.state.codePostale.value
                )
                console.log(' panier',this.state.panierSimple)
    
                this.setState({ isLoading: false })
                if ( response === 'ok'){
                    alert('commande successfull')
                    await removeCart()
                    await this._getCart()
                    this.props.navigation.navigate("Notifications", { ref_commande : ref })

                }else{
                    alert( 'not successfull')
                }


            })
           
        }else{
            this.setState({modalVisible: true})
            
        }

        

    }
    addPanier = async (Code_produit,Num_prestataire) => {
        this.setState({ isLoading: true })
        let id_client = this.state.client["id_client"]

        let res = await addToPanier({ Code_produit, id_client, Num_prestataire })

        console.log(res)

        // res === "yet" ?
        //     ToastAndroid.show("Ajouté au panier", ToastAndroid.SHORT)
        //     : ToastAndroid.show("Déjà dans le panier", ToastAndroid.SHORT)
        this.setState({ isLoading: false })
    }

    onChangeQual(i,type)
        {
            const dataCar = this.state.paniers
            let cantd = dataCar[i].quantity;

            if (type) {
            cantd = cantd + 1
            dataCar[i].quantity = cantd
            this.setState({paniers:dataCar})
            }
            else if (type==false&&cantd>=2){
            cantd = cantd - 1
            dataCar[i].quantity = cantd
            this.setState({paniers:dataCar})
            }
            else if (type==false&&cantd==1){
            dataCar.splice(i,1)
            this.setState({paniers:dataCar})
            } 
            AsyncStorage.setItem("cart",JSON.stringify(this.state.paniers))
        }

    _renderPanierItem = (item) => (
        // <List.Item
        //     key={index}
        //     style={{ backgroundColor: "#d3d3d3", margin: 10 }}
        //     title={nom_menu}
        //     titleStyle={{ fontWeight: "bold" }}
        //     //  descriptionStyle={}
        //     left={props => <Avatar.Image source={{ uri: `${MENU_URI}/${image}` }} size={70} />}
        //     right={props => <View style={{ ...props }, { alignItems: "center" }} >
        //         <Button onPress={() => this.updateQuantite(index, true)} mode="contained">-</Button>
        //         <Text >{this.state.quantite[index]}</Text>
        //         <Button onPress={() => this.updateQuantite(index, false)} mode="contained">+</Button>
        //     </View>}
        // />

        <View  style={{ flex:1 , flexDirection: 'row' }} >
                <View style={{ flex:1 , flexDirection :'column' , backgroundColor:'blue'}}>
                    <Image style={styles.imageMenu} resizeMode="stretch" source={{uri:`${MENU_URI}${item.pizza['file']}`}} />

                </View>

                <View style={{ flex:1 , flexDirection :'column',paddingLeft:10}}>
                    <View style={{ flex:1 }}> 
                     <Text style={{ fontSize: 22 ,}}>{item.pizza['Nom_menu']}</Text> 
                    </View>
                </View>
                <View style={{ height: 100 , backgroundColor:'grey'}}/>
        </View>
    )
    _formatTime(date) {
        if(date !== undefined){
        let hour = date.getHours()
        let minutes = date.getMinutes()

        hour = hour < 10 ? "0" + hour : hour
        minutes = minutes < 10 ? "0" + minutes : minutes

        return hour + ":" + minutes
        }
    } 
    
    _formatDate(date) {
        if(date !== undefined){
            let year = date.getFullYear();
            let month = date.getMonth() + 1;
            let dt = date.getDate();

            if (dt < 10) {
                dt = '0' + dt;
            }
            if (month < 10) {
                month = '0' + month;
            }

            return year + '-' + month + '-' + dt
        }
    }
    render() {
        // await _getCart()
        return (

            <View style={{flex:1 ,backgroundColor:'white'}}>
                <TopMenuP navigation={this.props.navigation} title="Mon panier"/>
                {/* MODAL */}
              <ScrollView style={{ flexGrow:1,paddingTop:10,}}>
                <Modal
                    animationType="fade"
                    transparent={true}
                    visible={this.state.modalVisible}

                    onRequestClose={() => {
                    Alert.alert("Modal has been closed.");
                    }}
                >
                    <View style={styles.centeredView}>
                    <View style={styles.modalView}>
                        <Text style={styles.modalText}>Vous devez vous connecter pour pouvoir effectuer une commande</Text>

                        <View style={{  flexDirection: 'row'}}>
                        <TouchableOpacity
                        style={{ ...styles.openButton, backgroundColor: "#FDA500" }}
                        onPress={() => {
                           this.setState({modalVisible : !this.state.modalVisible})
                        }}
                        >
                        <Text style={styles.textStyle}>Annuler</Text>
                        </TouchableOpacity>
                        <View style={{width: 20}}/>
                        <TouchableOpacity
                        style={{ ...styles.openButton, backgroundColor: "#FDA500" }}
                        onPress={() => {

                           this.setState({modalVisible : !this.state.modalVisible})
                           this.props.navigation.navigate('LoginScreen',{ fromCart: true})
                        }}
                        >
                        <Text style={styles.textStyle}>Se connecter</Text>
                        </TouchableOpacity>

                        </View>

                       
                    </View>
                    </View>
                </Modal>
                {/* MODAL */}


                {/* MODAL LOCALISATION */}
                <Modal
                    animationType="fade"
                    transparent={true}
                    visible={this.state.modalLoc}

                    onRequestClose={() => {
                        this.setState({
                            ville:{value:'',error: '' },
                            codePostale:{value:'',error: '' }
                            
                        })
                    }}
                >
                    <View style={styles.centeredView}>
                    <View style={styles.modalView}>
                        <Text>Veuillez remplir ces informations</Text>

                    <TextInput 
                    style={{ minWidth: 150 ,backgroundColor:'#CDCDCD',padding:10,borderRadius:10}}
                        placeholderTextColor={"white"}
                        label ="Ville"
                        returnKeyType = "done"
                        autoCapitalize="words"
                        value={this.state.ville.value}              
                        onChangeText={text => 
                            {
                                this.setState({ville: {value:text,error:''}})
                                
                             }}
                        selectionColor='white'
                           
                        error = {!!this.state.ville.error}
                        errorText = {this.state.ville.error}
                    />
                    <TextInput 
                        style={{ minWidth: 150 ,backgroundColor:'#CDCDCD',padding:10,borderRadius:10}}
                        placeholderTextColor={"white"}
                        label ="Code postale"
                        returnKeyType="done"
                        autoCapitalize="none"
                        selectionColor='white'

                        value={this.state.codePostale.value}
                        onChangeText={text => this.setState({codePostale:{value: text, error: ''}})}                        
                        error={!!this.state.codePostale.error}
                        errorText={this.state.codePostale.error} 
                    />

                        {/* <Text style={styles.modalText}>Vous devez vous connecter pour pouvoir effectuer une commande</Text> */}

                        <View style={{  flexDirection: 'row',paddingTop: 20}}>
                        <TouchableOpacity
                        style={{ ...styles.openButton, backgroundColor: "#FDA500" }}
                        onPress={() => {
                           this.setState({modalLoc : !this.state.modalLoc})
                        }}
                        >
                        <Text style={styles.textStyle}>Annuler</Text>
                        </TouchableOpacity>
                        <View style={{width: 20}}/>
                        <TouchableOpacity
                        style={{ ...styles.openButton, backgroundColor: "#FDA500" }}
                        onPress={() => {
                            let villeError =  isNotNull(this.state.ville.value)
                            let codePostaleError  = numberValidator(this.state.codePostale.value)
                            if( villeError || codePostaleError ){
                                this.setState({codePostale: {value: this.state.codePostale.value,error: codePostaleError }})
                                this.setState({ville: {value: this.state.ville.value,error: villeError}})
                            
                            }else{
                                this.setState({modalLoc : !this.state.modalLoc})
                                this.sendCommande()
                            }                           

                        }}
                        >
                        <Text style={styles.textStyle}>Valider</Text>
                        </TouchableOpacity>

                        </View>

                       
                    </View>
                    </View>
                </Modal>
                
                {/* MODAL LOCALISATION */}

            {/* <View style={{height:20}} />
            <View style={{ flexDirection :'row' , alignItems: 'center', justifyContent: 'center'}}>
                <Text style={{fontSize:32,fontWeight:"bold",color:"rgb(253, 165, 0)" }}>Mon panier</Text>
            </View>
            <View style={{height:10}} /> */}
   
            <View style={{flex:1}}>

            { this.state.paniers.length !== 0 ?
            <View>

                <View style={{marginHorizontal : 10}}>
                        <View style={{ flexDirection: "row" , alignItems: 'center',justifyContent: 'space-between'  }}>
                            <TouchableOpacity onPress={()=> this.setState({isOpenDate : true})} style={{ flexDirection: "row",alignItems : "center" }}>
                                <FontAwesomeIcon color="#1D2935" size={24} icon={faCalendar} />
                                <Text style={{color : "#1D2935" ,paddingLeft: 10}} >Date de réception</Text>
                            </TouchableOpacity>
                            <Text style={{color : "#1D2935" , fontWeight:'bold'}}>{this._formatDate(this.state.dateRecep)}</Text>
                        </View>
                        <View style={{height:5, backgroundColor : 'transparent'}}/>
                        <View>
                        <View style={{ flexDirection: "row", justifyContent: "space-between" }}>
                            <TouchableOpacity onPress={()=> this.setState({isOpenTime : true})} style={{ flexDirection: "row",alignItems : "center" }}>
                                <FontAwesomeIcon color="#1D2935" size={24} icon={faClock} />
                                <Text style={{color : "#1D2935",paddingLeft: 10}} >Heure de réception</Text>
                            </TouchableOpacity>
                            <Text style={{color : "#1D2935" , fontWeight:'bold'}}>{this._formatTime(this.state.heureRecep)}</Text>
                        </View>
                        </View>
                        <View style={{height: 5 , backgroundColor : 'transparent'}}/>


                        <View style={{flexDirection : "row",justifyContent : "space-evenly"}}>
                            <TouchableOpacity onPress={()=> this.setState({isSurPlace : true})} style={styles.button,this.state.isSurPlace === true ? styles.buttonFocused : styles.button}>
                                <Text  style={styles.button,this.state.isSurPlace === true ? {color: 'white'}: {color:'black'}}>Sur place</Text>
                            </TouchableOpacity>
                            <TouchableOpacity onPress={()=> this.setState({isSurPlace : false})} style={this.state.isSurPlace === false ? styles.buttonFocused : styles.button}>
                                <Text style={styles.button,this.state.isSurPlace === true ? {color: 'black'}: {color:'white'}}>A emporter</Text>
                            </TouchableOpacity>
                        </View>
                        <View style={{height: 5 , backgroundColor : 'transparent'}}/>



                        {this.state.isOpenDate === true &&
                            <DateTimePicker
                                testID="dateTimePicker"
                                timeZoneOffsetInMinutes={0}
                                value={this.state.dateRecep}
                                mode="date"
                            
                                display="default"
                                onChange={(event, selectedDate) => {
                                    this.setState({ isOpenDate: false })
                                    if(selectedDate !== undefined)  this.setState({dateRecep:selectedDate})
                                }}
                            />
                        }

                    {this.state.isOpenTime === true &&
                        <DateTimePicker
                            testID="dateTimePicker"
                            timeZoneOffsetInMinutes={0}
                            value={this.state.heureRecep}
                            mode="time"
                            is24Hour={true}
                            display="default"
                            onChange={(event, selectedDate) => {
                                this.setState({ isOpenTime: false })
                                if(selectedDate !== undefined) this.setState({heureRecep : selectedDate})
                            }}
                        />
                    }

                </View>

              {/* <ScrollView style={{flexGrow:1 }}> */}
   
                {
                  this.state.paniers.map((item,i)=>{
                    return(
                      <View key={i} style={{width:width-20,margin:10,backgroundColor:'transparent', flexDirection:'row', borderBottomWidth:2, borderColor:"#cccccc", paddingBottom:10}}>
                        <Image resizeMode="contain" style={{width:width/3,height:width/3}} source={{uri:`${MENU_URI}${item.pizza['file']}`}} />
                        <View style={{flex:1, backgroundColor:'trangraysparent', padding:10, justifyContent:"space-between"}}>
                          <View>
                            <Text style={{fontWeight:"bold", fontSize:20}}>{item.pizza['Nom_menu']}</Text>
                            <Text>{item.pizza.Description}</Text>
                          </View>
                          <View style={{flexDirection:'row',justifyContent:'space-between'}}>
                            <Text style={{fontWeight:'bold',color:"rgb(253, 165, 0)",fontSize:20}}>${item.price*item.quantity}</Text>
                            <View style={{flexDirection:'row', alignItems:'center'}}>
                              <TouchableOpacity onPress={()=>this.onChangeQual(i,false)}>
                                <Icon name="ios-remove-circle" size={35} color={"#33c37d"} />
                              </TouchableOpacity>
                              <Text style={{paddingHorizontal:8, fontWeight:'bold', fontSize:18}}>{item.quantity}</Text>
                              <TouchableOpacity onPress={()=>this.onChangeQual(i,true)}>
                              <Icon name="ios-add-circle" size={35} color={"#33c37d"}/>
                              </TouchableOpacity>
                            </View>
                          </View>
                        </View>
                      </View>
                    )
                  })
                }
   
                <View style={{height:20}} />

   
                <TouchableOpacity  onPress={() => this.setState({modalLoc: true}) } style={{
                    // backgroundColor:"#33c37d",
                    backgroundColor:"rgb(253, 165, 0)",                    
                    width:width-40,
                    alignItems:'center',
                    padding:10,
                    borderRadius:5,
                    margin:20
                  }}>
                  <Text style={{
                      fontSize:24,
                      fontWeight:"bold",
                      color:'white'
                    }}>
                    VALIDER
                  </Text>
                </TouchableOpacity>
   
                <View style={{height:20}} />
              {/* </ScrollView> */}
            </View>
             
              :
              <View style={{ flex:1 , alignItems: 'center', justifyContent:'center'}}>
                {/* <View style={{ flex: 1 , flexDirection: 'row' ,alignItems: 'center', justifyContent: 'center'}}> */}
                <Text>Votre Panier est vide</Text>
                {/* </View> */}
                <View style={{height: 20}}/>
                
                <TouchableOpacity
                style={{ justifyContent:'center',alignItems:'center', borderRadius: 10 ,width:(width/2)-50 ,height:((width/2)-20)-130 , backgroundColor: '#CBD0D4'}}
                onPress={ () => this.props.navigation.navigate('Pizza')}
                >
                    <Text >
                    Voir les prestataires
                    </Text>              
                </TouchableOpacity>
              </View>
              
              }
   
            </View>
            </ScrollView>
         </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        // flex: 1,
        // justifyContent: "center",
        // alignItems: "center",
        flex: 1,
        
        // marginTop: StatusBar.currentHeight || 0,
        marginHorizontal: 10
      },
    loading_container: {
        position: 'absolute',
        zIndex: 10,
        left: 0,
        right: 0,
        top: 100,
        bottom: 0,
        alignItems: 'center',
        justifyContent: 'center'
    },
    imageMenu:{
        flex:1,
        borderRadius: 20,
        // flexDirection: 'row',
        // alignItems: 'stretch',
        width:width/3,
        height:width/3,
        backgroundColor:'transparent',
      },
      centeredView: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        // marginTop: 22
      },
      modalView: {
        margin: 20,
        backgroundColor: "rgba(255,255,255,0.8)",
        borderRadius: 20,
        padding: 35,
        alignItems: "center",
        shadowColor: "#000",
        shadowOffset: {
          width: 0,
          height: 2
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5
      },
      
      openButton: {
        backgroundColor: "#F194FF",
        borderRadius: 20,
        padding: 10,
        elevation: 2
      },
      textStyle: {
        color: "white",
        fontWeight: "bold",
        textAlign: "center"
      },
      modalText: {
        marginBottom: 15,
        textAlign: "center"
      },
      buttonFocused:{
        backgroundColor : "#1D2935",
        padding :10,
        alignItems: 'center',
        justifyContent: 'center',
        shadowOffset: {
            width: 0,
            height: 10
          },
        height : ((width/2)-20)-120,
        borderRadius: 5,
        

      }, 
      button:{
        backgroundColor : "#d3d3d3",
        padding :10,
        alignItems: 'center',
        justifyContent: 'center',
        shadowOffset: {
            width: 0,
            height: 10
          },
          height : ((width/2)-20)-120,
          borderRadius: 5
      }

})

export default memo(Panier)
