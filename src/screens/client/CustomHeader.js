import React, { Component } from 'react';
import { Container, Header, Left, Body, Right, Button, Icon, Title,Text } from 'native-base';
import { Ionicons } from '@expo/vector-icons';
import * as Font from 'expo-font'
import { AppLoading } from 'expo';

export default class CustomHeader extends Component {
  constructor() {
    super()
    this.state = {
        isReady: false
    }
  }
 
  async componentDidMount() {
    await Font.loadAsync({
      Roboto: require('native-base/Fonts/Roboto.ttf'),
      Roboto_medium: require('native-base/Fonts/Roboto_medium.ttf'),
      ...Ionicons.font,
    });
    this.setState({ isReady: true });
  }
  render() {
    if (!this.state.isReady) {
      return <AppLoading />;
    }
    
    return (
      <Container>
        <Text>njaka</Text>
      </Container>
      // <Container>
      //   <Header>
      //     <Left>
      //       <Button transparent>
      //         <Icon name='arrow-back' />
      //       </Button>
      //     </Left>
      //     <Body>
      //       <Title>Header</Title>
      //     </Body>
      //     <Right>
      //       <Button transparent>
      //         <Icon name='menu' />
      //       </Button>
      //     </Right>
      //   </Header>
      // </Container>
    );
  }
}