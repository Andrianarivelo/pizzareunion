import React, { memo, useState } from 'react'
import { View, Text, StyleSheet, TouchableOpacity,Image,ImageBackground, Keyboard,KeyboardAvoidingView ,Dimensions, StatusBar } from 'react-native'
import TextInput from "../../components/TextInput"
import SignButton from "../../components/SignButton"
import Background from "../../components/Background"
import { theme } from "../../core/theme"
import { emailValidator, passwordValidator, userExist, nameValidator, phoneValidator, noSpecCharValidator } from "../../core/utils";
var {width} = Dimensions.get('window')
import { ScrollView } from 'react-native-gesture-handler'
import { signUpClient } from '../../API/client/authClient'
import { storeDate } from '../../core/session'
const RegisterScreen = ({ navigation }) => {
  const [nom, setNom] = useState({ value: "", error: "" });
  const [email, setEmail] = useState({ value: "", error: "" });
  const [password, setPassword] = useState({ value: "", error: "" });
  const [confirmPassword, setConfirmPassword] = useState({value : "", error :""})
  const [loading, setLoading] = useState(false);
  const [pseudo, setPseudo] = useState({ value: "", error: "" })
  const [phone, setPhone] = useState({value : "", error :""})
  const [error, setError] = useState("");


  const _handleSignUp = async () => {

    if (loading) return;
   
    const nameError = nameValidator(nom.value);
    const emailError = emailValidator(email.value);
    const passwordError = passwordValidator(password.value);
    const phoneError = phoneValidator(phone.value)
    const pseudoError = noSpecCharValidator(pseudo.value)
    

    if (emailError || passwordError || nameError || phoneError || pseudoError) {
      setNom({ ...nom, error: nameError });
      setEmail({ ...email, error: emailError });
      setPassword({ ...password, error: passwordError });
      setPhone({...phone,error: phoneError})
      setPseudo({...pseudo,error:pseudoError})
      return;
    } 

    setLoading(true);

    const response = await signUpClient({
      nom: nom.value,
      phone : phone.value,
      pseudo: pseudo.value,
      email: email.value,
      password: password.value
    })

    

    if (response == "emailuser") {
      const emailError = userExist(email.value);

      setEmail({ ...email, error: emailError })
    } else if(response == 'ok'){     
      navigation.navigate('LoginScreen',{isNewCli: true})
    }

    setLoading(false);

  }
  return (

      <Background style={styles.container}>
        <ScrollView contentContainerStyle={styles.content}>
      
      {/* <View style={{ flex: 1, alignItems: "center",justifyContent: 'center',paddingHorizontal: 20 , paddingBottom: 20 }}> */}
        <View style={{ backgroundColor:'white' , padding:20,borderRadius:50, alignItems:'center',justifyContent: 'center'}}>
          <Image  
          style={styles.image}
          source={require("../../../assets/logo.png")}
          resizeMode="contain"
          />
        </View>

        <View style={{ flexDirection:'column', alignItems:'center',justifyContent: 'center',marginTop:10 }}>
          <Text style={{ color: 'white',fontSize:18}}>INSCRIPTION</Text>  
        </View>
    
        <View style={{ flexDirection:'column', marginTop:5,padding:1,width:50 ,alignItems:'center',justifyContent: 'center',backgroundColor:'white'}}/>


     
       <TextInput
         label="Nom"
         returnKeyType="next"
         autoCapitalize="none"
         value={nom.value}
         onChangeText={text => setNom({ value: text, error: "" })}
         error={!!nom.error}
         errorText={nom.error}
       />
       <TextInput
         label="Pseudonyme"
         returnKeyType="next"
         autoCapitalize="none"
         value={pseudo.value}
         onChangeText={text => {
            if(text.length > 255){
              setPseudo( {value: text , error:'Trop long'})
            }
          setPseudo({ value: text, error: "" })
         }}
         error={!!pseudo.error}
         errorText={pseudo.error}
       />

       <TextInput
         label="Téléphone"
         returnKeyType="next"
         autoCapitalize="none"
         value={phone.value}
         onChangeText={text => setPhone({ value: text, error: "" })}
         error={!!phone.error}
         errorText={phone.error}
       />
      
       <TextInput
         label="Email"
         returnKeyType="next"
         value={email.value}
         onChangeText={text => setEmail({ value: text, error: "" })}
         error={!!email.error}
         errorText={email.error}

       />
       

       <TextInput
         label="Mot de passe"
         returnKeyType="done"
         autoCapitalize="none"
         value={password.value}
         onChangeText={text => setPassword({ value: text, error: "" })}
         error={!!password.error}
         errorText={password.error}
         secureTextEntry


       />
       <TextInput
         label="Confirmation mot de passe"
         returnKeyType="done"
         autoCapitalize="none"
         value={confirmPassword.value}
         onBlur={(text)=>{
          if (password.value !== confirmPassword.value){
            setConfirmPassword({value: '', error :"Non identique"})
          }
         }}
         onChangeText={text =>{
            setConfirmPassword({value : text, error :""})            
          }}
         error={!!confirmPassword.error}
         errorText={confirmPassword.error}
         secureTextEntry


       />    
    
       <SignButton
         mode="contained"
         loading={loading}
         style={styles.button}
         onPress={_handleSignUp}>
         M'inscrire
           </SignButton>


       <View style={styles.row}>
         <Text style={{color : "white"}}>J'ai déjà un compte </Text>
         <TouchableOpacity onPress={() => navigation.navigate("LoginScreen",{message : true})}>
           <Text style={styles.link}>Se connecter</Text>
         </TouchableOpacity>
       </View>

        {/* </View> */}
      </ScrollView>
    </Background>

   
  )
}

const styles = StyleSheet.create({
  container:{
    paddingTop:StatusBar.currentHeight || 0,
    flex:1,
  },  
  content:{
    flexGrow: 1, 
    justifyContent: 'center',
    alignItems:'center',
    paddingHorizontal:20
  } , 
  image:{
    width : width /4 -30,
    height: width /4 -30,
    },
  formContainer : {
    
  },
  label: {
    color: theme.colors.secondary
  },
  button: {
    marginTop: 24
  },
  row: {
    flexDirection: "row",
    marginTop: 4
  },
  link: {
    fontWeight: "bold",
    color: theme.colors.primary
  }
});

export default memo(RegisterScreen)

