import React, { memo, useState, useEffect} from 'react'
import { TouchableOpacity, View, Text, StyleSheet, Image ,Keyboard ,StatusBar , Dimensions} from "react-native"
import Background from "../../components/Background"
import TextInput from "../../components/TextInput"
import SignButton from "../../components/SignButton";
var {width} = Dimensions.get('window');
import {Checkbox} from 'react-native-paper'

import { emailValidator, passwordValidator, credValidator } from "../../core/utils";
import { theme } from "../../core/theme";
import { loginPrestataire, connectedPrestataire } from '../../API/prestataire/authPrestataire'
import { storeDate } from '../../core/session'
import AsyncStorage from '@react-native-community/async-storage';
import { ScrollView } from 'react-native-gesture-handler';


const LoginPrestataire = ({ navigation }) => {
    const [email, setEmail] = useState({ value: "", error: "" });
    const [password, setPassword] = useState({ value: "", error: "" });
    const [loading, setLoading] = useState(false);
    const [error, setError] = useState("");
    const [keyboardShown, setkeyboardShown] = useState(false)
    const [checked, setChecked] = useState(false)



    const storePresta = async (value) => {
        try {
          const jsonValue = JSON.stringify(value)
          await AsyncStorage.setItem('prestataire', jsonValue)

        } catch (e) {
          // saving error
        }
      } 

      useEffect(() => {
        Keyboard.addListener("keyboardDidShow", _keyboardDidShow);
        Keyboard.addListener("keyboardDidHide", _keyboardDidHide);
    
        // cleanup function
        return () => {
          Keyboard.removeListener("keyboardDidShow", _keyboardDidShow);
          Keyboard.removeListener("keyboardDidHide", _keyboardDidHide);
        };
      }, []);
    
      const _keyboardDidShow = () => {
        // alert("Keyboard Shown");
        setkeyboardShown(true)
      };
  
      const _keyboardDidHide = () => {
        setkeyboardShown(false)
        // alert("Keyboard Hidden");
      };

    const fetchConnectedPresta = async () =>{
        try {
            await AsyncStorage.getItem('prestataire').then((res)=>{
                if(res !== undefined){
                    navigation.navigate('MainPrestataire')            
                }
        })            
        } catch(e) {
        }
    } 
    const _handleSign = async () => {
       
        if (loading) return;
        const emailError = emailValidator(email.value)
        const passwordError = passwordValidator(password.value)

        if (emailError || passwordError ) {
            setEmail({ ...email, error: emailError });
            setPassword({ ...password, error: passwordError });
            // setEmail({ value:'',error:emailError})
            // setEmail({ value:'',error:passwordError})

            return setLoading(false);
        } else {
            setLoading(true)

            let response = await loginPrestataire({
                email: email.value,
                password: password.value
            })
            // console.log(response)

            if (response == "ok") {
                // alert('ok')
                let presta = await connectedPrestataire(email.value)
                console.log('presta',presta)
                storePresta(presta)

                // await storeDate("prestataire",email.value)
                navigation.navigate('MainTabs', { screen: 'MainPrestataire',initial: false });
            } else if (response == "bloque") {
                setEmail({ ...email, error: "Vous êtes actuellement bloqué" });
            } else {
                setEmail({ ...email, error: "Indentifiant incorrect" });
                setPassword({...password,error : "Identifiant incorrect"})
            }

        }




        setLoading(false)
    }
    return (
        <Background style={styles.container}>
            <ScrollView contentContainerStyle={styles.content}>
                {/* <View style={styles.content}> */}
                <View style={{ backgroundColor:'white' , padding:20,borderRadius:50, alignItems:'center',justifyContent: 'center'}}>
                    <Image  
                    style={ keyboardShown ? styles.imageTiny :  styles.image}
                    source={require("../../../assets/logo.png")}
                    resizeMode="contain"
                    />
                </View>

                <View style={{ flexDirection:'column', alignItems:'center',justifyContent: 'center',marginTop:10 }}>
                    <Text style={{ color: 'white',fontSize:18}}>CONNEXION</Text>  
                </View>
        
                <View style={{ flexDirection:'column', marginTop:5,padding:1,width:50 ,alignItems:'center',justifyContent: 'center',backgroundColor:'white'}}/>

                
                <View style={{height:20}}/>
                <TextInput
                    label="Email"
                    returnKeyType="next"
                    autoCapitalize="none"
                    value={email.value}
                    onChangeText={text => setEmail({ value: text, error: "" })}
                    error={!!email.error}
                    errorText={email.error}
                    disabled={loading}
                />
                <TextInput
                    label="Mot de passe"
                    returnKeyType="done"
                    autoCapitalize="none"
                    value={password.value}
                    onChangeText={text =>{
                        setPassword({ value: text, error: "" })
                        // alert(text)
                        }
                    } 
                    error={!!password.error}
                    errorText={password.error}
                    secureTextEntry
                    disabled={loading}
                />

                {/* <View style={styles.forgotPassword}>
                    <TouchableOpacity
                        onPress={() => navigation.navigate("ForgotPrestataire")}
                    >
                        <Text style={{ color: "white" }}>Mot de passe oublié?</Text>
                    </TouchableOpacity>
                </View> */}
                

                <View style={{ flexDirection: 'row'  }}>
                    <View style={{flex:1,flexDirection: 'row' , alignItems:'center'}}>
                        <Checkbox
                            status={checked ? 'checked' : 'unchecked'}
                            color='white'
                            uncheckedColor='white'
                            onPress={ async() => {
                            setChecked(!checked);
                            await storeDate('stayLogged',!checked)
                            }}
                        />
                        <Text style={{ color: 'white'}}>
                            Rester connecté
                        </Text>
                    </View >
                        <View style={styles.forgotPassword}>
                            <TouchableOpacity
                                onPress={() => navigation.navigate("ForgotPrestataire")}
                            >
                                <Text style={{color : "white",alignSelf:'center'}}>Mot de passe oublié</Text>
                            </TouchableOpacity>
                        </View>
                </View>

                <SignButton loading={loading} mode="contained" onPress={_handleSign}  disabled={loading}>  
                    Me connecter
                </SignButton>

                <View style={styles.row}>
                    <Text style={{ color: "white" }}>Nouveau prestataire? </Text>
                    <TouchableOpacity onPress={() => navigation.navigate("RegisterPrestataire")}>
                        <Text style={styles.link}>S'inscrire</Text>
                    </TouchableOpacity>
                </View>
                <View style={styles.row}>
                    <Text style={{ color: "white" }}>Plutôt client ? </Text>
                    <TouchableOpacity onPress={() => navigation.navigate("LoginScreen",{express : false,isNewCli : false})}>
                        <Text style={styles.link}>ici</Text>
                    </TouchableOpacity>
                </View>
                {/* </View> */}
            </ScrollView>

        </Background>
    )
}

const styles = StyleSheet.create({
    container:{
        paddingTop:StatusBar.currentHeight || 0,
        flex:1,
    },  
    content:{
        flexGrow: 1, 
        justifyContent: 'center',
        alignItems:'center',
        paddingHorizontal:20

    } ,
    forgotPassword: {
        width: "100%",
        alignItems: "flex-end",
        justifyContent: 'center',
        flex:1
  
        // marginBottom: 24
      },
    row: {
        flexDirection: "row",
        marginTop: 4
    },
    label: {
        color: theme.colors.secondary
    },
    link: {
        fontWeight: "bold",
        color: theme.colors.primary
    },
    image:{
        width : width /4 - 30,
        height: width /4 -30,
    },
    imageTiny:{
        width : width /8-10,
        height: width /8-10,
    }
    
})


export default memo(LoginPrestataire)
