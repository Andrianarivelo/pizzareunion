import React, { Component, memo } from 'react'
import { Text, View, ImageBackground, StyleSheet, Image, ActivityIndicator, Dimensions,TextInput ,TouchableOpacity, KeyboardAvoidingView} from 'react-native'
import { allMenuByPrest } from "../../API/prestataire/list";
import { ScrollView } from 'react-native-gesture-handler';
import {
    widthPercentageToDP as wp,
    heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import { Avatar, Button, Card, Title, Paragraph, FAB } from 'react-native-paper';
import { MENU_URI } from '../../core/config';
import { deleteProduit } from '../../API/prestataire/menu';
import { currentPrestataire, getData } from '../../core/session'
import TopMenuP from "../../components/TopMenuP"
import {theme} from '../../core/theme'
import {Icon , Input,Item} from 'native-base'
var {width} = Dimensions.get('window')

const LeftContent = props => <Avatar.Icon {...props} icon="pizza"/>

class Menu extends Component {

    constructor(props) {
        super(props)
        this.navigationWillFocusListener  =  props.navigation.addListener('willFocus',async ()=>{
            await this.fetchConnectedPresta()
            await this.fetchMenu()
        })
        this.state = {
            menus: [],
            prestataire: "",
            isLoading: false,
            search:''
        }
    }

    componentDidMount = async () => {
        this.setState({ isLoading: true })
        await this.fetchConnectedPresta()
        await this.fetchMenu()
        this.setState({ isLoading: false })
    }
    fetchConnectedPresta = async () => {
        let presta = await getData('prestataire')
        // console.log('presta',presta)
        this.setState({
            prestataire: presta
        })

    }

    fetchMenu = async () => {
        let Num_prestataire = this.state.prestataire["Num_prestataire"]
        let response = await allMenuByPrest(Num_prestataire)
        console.log('menus',response)

        response === "vide" ? this.setState({ menus: undefined }) : this.setState({ menus: response })

    }

    renderMenu = (id, code_produit, image, nom_menu) => (
        // <View style={{backgroundColor:'red',marginHorizontal:10}}>
        <Card style={styles.cardContainer}>
            <Card.Title title={nom_menu} subtitle="Card Subtitle" left={LeftContent} style={{paddingRight:10}}  />
            <Card.Content>
            {/* <Title>{nom_menu}</Title> */}
            <Paragraph>Card content</Paragraph>
            </Card.Content>
            <Card.Cover   source={{ uri: `${MENU_URI}${image}` }}  />
            <Card.Actions style={{}}>
            <Button style={{ backgroundColor:'green'}} color="white" >Modifier</Button>
            <Button>Supprimer</Button>
            </Card.Actions>
        </Card>
        // </View>

        // <View key={id} onPress={() =>
        //     this.props.navigation.navigate("Pizza", { Num_prestataire: 15 })
        // } style={styles.cardContainer}>
        //     <View style={styles.imageContainer}>
        //         <Avatar.Image size={100} source={{ uri: `${MENU_URI}${image}` }} />
        //     </View>
        //     <View >
        //         <Text style={{ fontWeight: "bold",textAlign: 'center', fontSize: 22 }}>{nom_menu}</Text>
        //         <Button mode="contained" onPress={() => this.deletePizza(code_produit)} style={{ backgroundColor: "red" }}>Supprimer</Button>
        //         <View style={{ height: 10}}/>
                
        //         <Button  onPress={() => { this.props.navigation.navigate("MenuEdit", { CodePdt: code_produit }) }}>Modifier</Button>
        //     </View>

        // </View>
    )

    search = async ()=>{
        this.state.menus.forEach(el => {

            
        });
    }

    deletePizza = async (code_produit) => {

        let response = await deleteProduit(code_produit)

        response === "ok" ? await this.fetchMenu() : ""
    }
    render() {
        return (
            <KeyboardAvoidingView style={{flex:1}}>
            
            
            <View style={{ flex:1 , backgroundColor:theme.colors.primary}}>
                {/* <View> */}
                <TopMenuP title="Menus" navigation={this.props.navigation} />
                {/* </View> */}

                {this.state.isLoading && <View style={styles.loading_container}>
                    <ActivityIndicator size="large" />
                </View>}

                
                
                <ScrollView contentContainerStyle={styles.container}>

                <FAB
                    style={styles.fab}
                    large
                    icon="plus"
                    onPress={() => alert('ok')}
                />

                        <View  style={{flex:0.2 , flexDirection: 'row' ,alignItems:'center'}} >
                            {/* <Icon type="MaterialIcons" name="search" style={{ }}/> */}
                            {/* <View style={{width : 20}}/> */}
                            {/* <TextInput
                             inlineImageLeft='search_icon'
                             style={{ backgroundColor:'yellow' ,borderRadius:10 ,width:'80%'}}
                             /> */}
                             <Item style={{ }}>
                                <Icon active type="MaterialIcons" name='search' />
                                <Input placeholder='Icon Textbox' returnKeyType='search' />
                                {/* <Icon active  type="MaterialIcons" name='cancel' /> */}
                                <TouchableOpacity onPress={ async ()=> await this.fetchAllPrestataireCmd()}>
                                    <Icon type="MaterialIcons" name="cancel" style={{ }}/>                   
                                </TouchableOpacity>

                            </Item>
                                
                                                    </View>
                        <View style={{height:20}}/>

                    {/* <ScrollView horizontal={true} style={{flex:6}}>
                        {this.state.menus !== undefined && this.state.menus.map((menu, i) => {
                            return this.renderMenu(i, menu.Code_produit, menu.file, menu.Nom_menu)
                        })

                        }
                    </ScrollView> */}

                    {/* {this.state.isLoading === false &&

                    <Button labelStyle={{ color: "#000" }} style={{ backgroundColor: "white", marginTop: 10 }} mode="contained" onPress={() => this.props.navigation.navigate("MenuAdd")}>
                        Ajouter un menu
                </Button>
                } */}

                </ScrollView>
            </View>
            </KeyboardAvoidingView>

        )
    }
}

const styles = StyleSheet.create({
    cardContainer: {
        backgroundColor: "rgba(205, 205, 205,0.1)",
        marginRight: 20
    },
    loading_container: {
        position: 'absolute',
        zIndex: 10,
        left: 0,
        right: 0,
        top: 100,
        bottom: 0,
        alignItems: 'center',
        justifyContent: 'center'
    },
    container: {
        flexGrow: 1,
        marginTop:20,
        padding: 10,
        backgroundColor:'white',
        borderTopRightRadius:40,
        borderTopLeftRadius:40
    },fab: {
        position: 'absolute',
        margin: 16,
        right: 0,
        // bottom: 0,
        bottom: 0
      },
})
export default memo(Menu)