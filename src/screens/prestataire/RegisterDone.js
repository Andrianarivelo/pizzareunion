import React, { memo } from 'react'
import { View, Text,Image, StyleSheet ,StatusBar} from 'react-native'
import Background from '../../components/Background'
import { TouchableRipple } from 'react-native-paper'
import { Button } from 'react-native-paper'

const RegisterDone = ({ navigation }) => {
    return (
        <View style={{flex: 1 ,alignItems: 'center',justifyContent:'center' , paddingHorizontal: 20 }}>
            
            <Image
                style={{ maxWidth: 200, maxHeight: 200 }}
                source={require("../../../assets/logo.png")}
                resizeMode="contain"
            />
            <Text style={{ color: "rgb(253, 165, 0)" }}>PIZZA REUNION</Text>
            <View style={{height: 20}}/>
            <View>
                <Text style={styles.message}>Pizza Reunion vous remercie de votre inscription.</Text>
                <Text style={styles.message}>Un email de confirmation vous serez  envoyé par un administateur</Text>
                <View style={{height: 20}}/>
                
                <Button uppercase={false} style={{ backgroundColor: 'rgb(253, 165, 0)'}} mode="contained" onPress={() => navigation.navigate("LoginPrestataire")}>
                    Terminer
                </Button>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container : {

    },
    message : {
        textAlign : "center",
        color : "black",
    }
})
export default memo(RegisterDone)
