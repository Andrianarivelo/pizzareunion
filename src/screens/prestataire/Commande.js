import React, { Component, memo } from 'react'
import { Text, View, ImageBackground, ScrollView, StyleSheet,ActivityIndicator , Dimensions , Image } from 'react-native'
import { commandeAvalider } from '../../API/prestataire/list'
import { TouchableOpacity } from 'react-native-gesture-handler'
import {
    widthPercentageToDP as wp,
    heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
var {width} = Dimensions.get('window')
import { currentPrestataire } from '../../core/session'
import { TouchableRipple, Avatar, Button, List, Badge } from 'react-native-paper'
import { API_URL, MENU_URI } from '../../core/config';
import TopMenuP from "../../components/TopMenuP"
import {Icon} from 'native-base'
import AsyncStorage from '@react-native-community/async-storage';


class Commande extends Component {

    constructor(props) {
        super(props)
        this.navigationWillFocusListener  =  props.navigation.addListener('willFocus',async ()=>{
            await this.fetchCommande()
        })
        this.state = {
            commandes: [],
            commandeByref: [],
            prestataire: [],
            isLoading : false
        }
    }

    componentDidMount = async () => {

        this.setState({isLoading : true})
        await this.fetchConnectedPresta()
        await this.fetchCommande()
        // await this._setByRef()
        this.setState({isLoading : false})

    }

    componentWillUnmount = async()=>{
        this.state = {
            commandes: [],
            commandeByref: [],
            prestataire: "",
        }
    }

    fetchConnectedPresta = async () =>{
        try {
            await AsyncStorage.getItem('prestataire').then((res)=>{
                this.setState({prestataire : JSON.parse(res)})
            })
                
            } catch(e) {
            console.log(e)
            }
    }

    fetchCommande = async () => {
        let Num_prestataire = this.state.prestataire["Num_prestataire"]
        let response = await commandeAvalider(Num_prestataire)
        const cmd = response
        if (response != null && response != undefined && response != 'vide'){
            cmd.forEach(element => {
                let imageNamePath = element["file"].split("/")
                let a = imageNamePath[0] + "/menus/" + imageNamePath[1]
                element['image'] = a

                console.log('commande',this.state.commande)
                response == "vide" ? this.setState({ commandes: undefined }) : this.setState({ commandes: response })
            
            });
        }
       
            
        
            
        
        
       
        
    }



 
   
    goToDetail = ref => {
        this.props.navigation.navigate("CommandeDetail", { ref_commande: ref })
    }

    renderCommandes = async (index, ref, image) => (
        <List.Item
            key={index}
            style={{ backgroundColor: "#d3d3d3", margin: 10 }}
            title={ref}
            titleStyle={{ fontWeight: "bold" }}
            left={props => <Avatar.Image source={{ uri: `${MENU_URI}/${image}` }} size={70} />}

        />
    )


    render() {
        return (
            <ImageBackground
                source={require("../../../assets/home.jpg")}
                style={{ flex: 1, width: "100%" }}
            >   
                {this.state.isLoading && <View style={styles.loading_container}>
                    <ActivityIndicator size="large" />
                </View>}

                <View>
                    <TopMenuP title="Commandes" navigation={this.props.navigation} />
                </View>

                <View style={{  flex: 1}} >

                        {this.state.commandes.length > 0 ?  this.state.commandes.map((commande, i) => {

                            return(
                            <ScrollView>

                                <TouchableOpacity onPress={()=>this.goToDetail(commande.ref)}  key={i} style={{width:width-20,margin:10,backgroundColor:'transparent', flexDirection:'row', borderWidth:0.5, borderRadius: 10 ,  borderColor:"rgb(253, 165, 0)",paddingBottom:5}}>
                                <Image resizeMode="contain" style={{width:width/3,height:width/3}} source={{uri:`${MENU_URI}${commande['image']}`}} />
                                <View style={{flex:1, backgroundColor:'trangraysparent', padding:10, justifyContent:"space-between"}}>
                                <View>
                                    <Text style={{ color: 'white',fontWeight:"bold", fontSize:20}}>{commande['Nom_client'] + ' ' +commande['Prenom_client']}</Text>  
                                    <Text style={{ color: 'white',fontWeight:"bold", fontSize:20}}>{ 'ref : ' +commande.ref}</Text>  
                                    <Text style={{ color: 'white', fontSize:16}}>{commande['Nom_menu_commnder']}</Text>    
                                    <Text style={{ color: 'white', fontSize:16}}>{commande['Categories_menu_commnder'] + 'cm' }</Text>      
                                </View>
                                    {/* <View style={{flexDirection:'row',justifyContent:'space-between'}}>
                                    <Text style={{fontWeight:'bold',color:"rgb(253, 165, 0)",fontSize:20}}>${item.price*item.quantity}</Text>
                                    <View style={{flexDirection:'row', alignItems:'center'}}>
                                        <TouchableOpacity onPress={()=>this.onChangeQual(i,false)}>
                                        <Icon name="ios-remove-circle" size={35} color={"#33c37d"} />
                                        </TouchableOpacity>
                                        <Text style={{paddingHorizontal:8, fontWeight:'bold', fontSize:18}}>{item.quantity}</Text>
                                        <TouchableOpacity onPress={()=>this.onChangeQual(i,true)}>
                                        <Icon name="ios-add-circle" size={35} color={"#33c37d"}/>
                                        </TouchableOpacity>
                                    </View>
                                    </View> */}
                                </View>
                                </TouchableOpacity>
                            </ScrollView>

                            )
                            
                            
                        }) :
                        <View style={{ flex: 1 , flexDirection:'row' , alignItems:'center' ,justifyContent: 'center'}}>
                            
                            {/* <Icon type="MaterialIcons" name='list' style={{ color: 'white' , fontSize:70}} /> */}
                            
                            <Text style={{ color : 'white' , fontSize: 22}}>
                                Commande vide
                            </Text>
                        </View>
                        
                        
                        }

                </View>

            </ImageBackground>
        )
    }
}

const styles = StyleSheet.create({
    loading_container: {
        position: 'absolute',
        zIndex: 10,
        left: 0,
        right: 0,
        top: 100,
        bottom: 0,
        alignItems: 'center',
        justifyContent: 'center'
    }
})

export default memo(Commande)
/* {this.state.commandes !== undefined && this.state.commandes.map((commande, i)=>{
    return <View key={i}>
        <TouchableOpacity onPress={() => this.goToDetail(commande.ref)}>
             <Text>{commande.ref}</Text>
        </TouchableOpacity>
    </View>
})} */