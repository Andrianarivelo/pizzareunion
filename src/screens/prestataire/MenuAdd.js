import React, { Component, memo } from 'react'
import { Text, View, ScrollView, StyleSheet, TouchableOpacity, Image, ImageBackground ,Dimensions , KeyboardAvoidingView} from 'react-native'
import TextInput from "../../components/TextInput"
import { theme } from "../../core/theme"
import SignButton from "../../components/SignButton"
import { ajouterMenu } from "../../API/prestataire/menu";
import { PRESTA_URI } from '../../core/config'
import * as Permissions from 'expo-permissions'
import * as ImagePicker from 'expo-image-picker'
import { API_URL } from "../../core/config"
import axios from "axios"
import { Button, Avatar } from 'react-native-paper';
import { currentPrestataire, getData } from '../../core/session'
import TopMenuP from "../../components/TopMenuP"
import Background from '../../components/Background'
import { Icon } from 'native-base'
import { isNotNull, numberValidator } from '../../core/utils'
var {width} =Dimensions.get('window')

class MenuAdd extends Component {
  constructor() {
    super()
         this.state = {
            displayImg: null,
            prestataire: "",
            isLoading: false,
            type : {
              value: '',
              error:''
            },
            nom : {
              value: '',
              error:''
            },
            description : {
              value: '',
              error:''
            },
            taille : {
              value: '',
              error:''
            },
            prix : {
              value: '',
              error:''
            },
            image :""
      }

  }

  componentDidMount = async () => {
    await this.getCameraPermissions()
    await this.fetchConnectedPresta()
  }
  fetchConnectedPresta = async () => {
    let presta = await getData('prestataire')
    this.setState({
      prestataire: presta
    })

  }
  getCameraPermissions = async () => {

    let { status } = await Permissions.getAsync(Permissions.CAMERA_ROLL);
    if (status !== 'granted') {
      alert("permission not granteed")
      

    } else {

    }
  }

  _addMenu = async () => {
    this.setState({ isLoading: true })
    let Num_prestataire = this.state.prestataire["Num_prestataire"]

    const typeError = isNotNull(this.state.type.value)
    const nomError = isNotNull(this.state.nom.value)
    const tailleError = numberValidator(this.state.taille.value)
    const prixError = numberValidator(this.state.prix.value)
    const descriptionError = isNotNull(this.state.description.value)

    if(typeError || nomError || tailleError || prixError || descriptionError){
        this.setState({
          type:{ ...this.state.type.value, error: typeError},
          nom:{...this.state.nom.value, error: nomError},
          taille:{ ...this.state.taille.value, error: tailleError},
          prix:{ ...this.state.prix.value, error: prixError},
          description:{ ...this.state.description.value,error: descriptionError},
        })
        this.setState({isLoading: false});
        return ;
    }else{
      let menu = {
        Type: this.state.type.value,
        Nom_menu: this.state.nom.value,
        Description: this.state.description.value,
        Taille: this.state.taille.value,
        Prix: this.state.prix.value,
        image: this.state.image,
        nom_image: Math.random(),
        Num_prestataire: Num_prestataire
      }
      console.log('menu_add',menu)
  
      let response = await ajouterMenu(menu)
      if(response === 'ok'){
        this.setState({ isLoading: false })
        this.props.navigation.navigate("Menu")
      }else{
        this.setState({ isLoading: false })
        alert(response)

      }

    }


    

  }


  upload = async () => {

    let result = await ImagePicker.launchImageLibraryAsync({
      base64: true
    });

    if (!result.cancelled) {
      this.setState({image:result.base64})
      this.setState({ displayImg: result.uri })
    }
  }
  render() {
    return (
      <ImageBackground
        source={require("../../../assets/home.jpg")}
        style={{ flex: 1, width: "100%" }}>

      
        <View>
          <TopMenuP title="Ajout Menu" navigation={this.props.navigation} />
        </View>

        <KeyboardAvoidingView style={{ flex:1 }}>
        <ScrollView style={{flex:1}}>
        <View style={{ flex: 1, padding: 20, aligItems: "center", }}>
          <View style={{ flexDirection: "column", alignItems: "center" }}>
            {/* <Avatar.Image size={150} source={{ uri: `${PRESTA_URI}/${this.state.prestataire["nom_file"]}` }} /> */}
            <Text style={{ color: "yellow" }}>Ajouter un menu</Text>
          </View>
          {/* <ScrollView> */}
            <TextInput
              label="Type"
              returnKeyType="next"
              autoCapitalize="words"
              value={this.state.type.value}

              onChangeText={text => {
                let temp = {
                  value: text,
                  error : ''
                }
                this.setState({ type : temp})}
 
              }
              error = {!!this.state.type.error}
              errorText = {this.state.type.error}

            />
            <TextInput
              label="Nom du menu"
              returnKeyType="next"
              value={this.state.nom.value}
              autoCapitalize="words"
              onChangeText={text =>{
                let temp = {
                  value: text,
                  error : ''
                }
                 this.setState({ nom : temp})}}
              error = {!!this.state.nom.error}
                errorText = {this.state.nom.error}

            />

            <TextInput
              label="Descriptions et ingrédients"
              returnKeyType="next"
              value={this.state.description.value}
              autoCapitalize="words"
              onChangeText={text =>{
                let temp = {
                  value: text,
                  error : ''
                }
                this.setState({ description : temp})}}
              error = {!!this.state.description.error}
              errorText = {this.state.description.error}
            />

            <TextInput
              label="Taille"
              returnKeyType="next"
              value={this.state.taille.value}

              onChangeText={text => {
                this.setState({ taille :{value : text , error: ''}})
                let err = numberValidator(text)
                if(err){
                this.setState({ taille :{ value : text , error: err}})

                }
              }}              
              error = {!!this.state.taille.error}
                errorText = {this.state.taille.error}
              />
            <TextInput
              label="Prix"
              returnKeyType="done"
              autoCapitalize="none"
              value={this.state.prix.value}
              onChangeText={text => {
                this.setState({ prix :{value : text , error: ''}})
              let err = numberValidator(text)
              if(err){
                this.setState({ prix :{ value : text , error: err}})

              }
              
              }}
              error = {!!this.state.prix.error}
              errorText = {this.state.prix.error}
            />


            <Button style={{ backgroundColor: "red", marginBottom: 5 }} icon="camera" mode="contained" onPress={() => this.upload()}>
              <Text>Ajouter une photo</Text> 
            </Button>

            <View style={{ flex: 1 , flexDirection: 'column'}}>
                
            {this.state.displayImg !== null && <Text>Votre image a été selectionnée</Text>}

            {this.state.displayImg !== null && 
            <View style={{ flexDirection: 'row',flex: 1, flexbackgroundColor: 'green'}}>
              <View>
                <Image width={10} style={{width : width/3 , height:width/3 , position: 'relative'}} source={{uri : this.state.displayImg}} />
              </View>
              <TouchableOpacity onPress={()=> this.setState({displayImg : null})} elevation={5} style={{ backgroundColor: 'rgba(211, 211, 211,0.5)' , position: 'absolute',borderRadius: 50 , justifyContent: 'flex-end' , left:90  , top: 5}}>
                <Icon type="MaterialCommunityIcons" name="close" fontSize={20} color='black' />
              </TouchableOpacity>
              
              </View>}
          
          </View> 

          <View>
          <SignButton
            loading={this.state.isLoading}
            mode="contained"
            onPress={ ()=> this._addMenu()}
            style={styles.button}
          >
            Ajouter
            </SignButton>
          </View>
        </View>

        

        </ScrollView>
      
        </KeyboardAvoidingView>
        </ImageBackground>


     


    )
  }
}

const styles = StyleSheet.create({
  label: {
    color: theme.colors.secondary
  },
  button: {
    marginTop: 24
  },
  row: {
    flexDirection: "row",
    marginTop: 4
  },
  link: {
    fontWeight: "bold",
    color: theme.colors.primary
  }
});

export default memo(MenuAdd)
