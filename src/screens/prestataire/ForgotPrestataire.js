import React, { memo, useState ,useEffect} from 'react'
import { View, Text, Image, Keyboard, TouchableOpacity,Dimensions, StyleSheet } from 'react-native'
import axios from "axios"
import Background from "../../components/Background"
import TextInput from "../../components/TextInput"
import SignButton from "../../components/SignButton";
import { motDePasseOublie } from '../../API/prestataire/authPrestataire'
import { theme } from "../../core/theme";
var {width} = Dimensions.get('window');

const ForgotPrestataire = () => {


    const [email, setEmail] = useState({ value: "", error: "" })
    const [loading, setLoading] = useState(false)
    const [sent, setSent] = useState(false)
    const [keyboardShown, setkeyboardShown] = useState(false)

    useEffect(() => {
        Keyboard.addListener("keyboardDidShow", _keyboardDidShow);
        Keyboard.addListener("keyboardDidHide", _keyboardDidHide);
    
        // cleanup function
        return () => {
          Keyboard.removeListener("keyboardDidShow", _keyboardDidShow);
          Keyboard.removeListener("keyboardDidHide", _keyboardDidHide);
        };
      }, []); 
      
      const _keyboardDidShow = () => {
        // alert("Keyboard Shown");
        setkeyboardShown(true)
      };
  
      const _keyboardDidHide = () => {
        setkeyboardShown(false)
        // alert("Keyboard Hidden");
      };


    
    const _resetPassword = async () => {
        let response = await motDePasseOublie(email.value)

        console.log(response)

        response === "emailnotexist" ? setEmail({ ...email, error: "Cet adresse email n'existe pas dans notre base" }) : setSent(true)
    }


    return (
        <Background>
            <View style={{flex: 1 , alignItems: 'center', justifyContent: 'center',paddingHorizontal: 20}}>
            
            <View style={{ backgroundColor:'white' , padding:20,borderRadius:50, alignItems:'center',justifyContent: 'center'}}>
                <Image  
                style={ keyboardShown ? styles.imageTiny :  styles.image}
                source={require("../../../assets/logo.png")}
                resizeMode="contain"
                />
            </View>

            <View style={{ flexDirection:'column', alignItems:'center',justifyContent: 'center',marginTop:10 }}>
                <Text style={{ color: 'white',fontSize:18}}>REINITIALISER MOT DE PASSE</Text>  
            </View>
    
            <View style={{ flexDirection:'column', marginTop:5,padding:1,width:50 ,alignItems:'center',justifyContent: 'center',backgroundColor:'white'}}/>

            <View style={{height:20}}/>

            <TextInput
                label="Email"
                returnKeyType="next"
                autoCapitalize="none"
                value={email.value}

                onChangeText={text => setEmail({ value: text, error: "" })}
                error={!!email.error}
                errorText={email.error}
            />
            {sent === true && <View>
                <Text style={{ color: "white" }}>Vous allez recevoir un mot de passe réinitialisé dans votre email.</Text>
                <View style={styles.row}>
                    <Text style={{ color: "white" }}>Vous n'avez reçu aucun mail ? </Text>
                    <TouchableOpacity onPress={() => {
                        setEmail({value :"",error :""})
                        setSent(false)
                        }}>
                        <Text style={styles.link}>Réessayer</Text>
                    </TouchableOpacity>
                </View>
            </View>}

            {sent === false &&

                <SignButton loading={loading} mode="contained" onPress={_resetPassword}>
                    Envoyer le code
            </SignButton>
            }

            {sent === false &&
                <View style={styles.row}>
                    <Text style={{ color: "white" }}>Nouveau client ? </Text>
                    <TouchableOpacity onPress={() => navigation.navigate("RegisterScreen")}>
                        <Text style={styles.link}>S'inscrire</Text>
                    </TouchableOpacity>
                </View>
            }


</View>
        </Background>
    )
}

const styles = StyleSheet.create({
    forgotPassword: {
        width: "100%",
        alignItems: "flex-end",
        marginBottom: 24
    },
    row: {
        flexDirection: "row",
        marginTop: 4
    },
    label: {
        color: theme.colors.secondary
    },
    link: {
        fontWeight: "bold",
        color: theme.colors.primary
    },
    image:{
        width : width /4 - 30,
        height: width /4 -30,
    },
    imageTiny:{
        width : width /8-10,
        height: width /8-10,
    }
})


export default memo(ForgotPrestataire)
