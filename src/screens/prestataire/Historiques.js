import React, { Component, memo } from 'react'
import { Text, View, ActivityIndicator, StyleSheet, ImageBackground } from 'react-native'
import { allCommande } from "../../API/prestataire/list";
import { currentPrestataire, getData } from '../../core/session'
import { DataTable, Avatar } from 'react-native-paper';
import { PRESTA_URI } from '../../core/config'
import TopMenuP from "../../components/TopMenuP"
import {Item, Form, Input , Label , Icon} from 'native-base'
import { TouchableOpacity } from 'react-native-gesture-handler';
import DateTimePicker from '@react-native-community/datetimepicker';



class Historiques extends Component {

    constructor() {
        super()
        this.state = {
            facture: [],
            prestataire: [],
            isLoading: false,
            somme : 0,
            openSearch : false,
            dateSearch : new Date(),
            isMounted : false
        }
    }

    componentDidMount = async () => {
        this.setState({isLoading : true})
        await this.fetchConnectedPresta()
        await this.fetchAllPrestataireCmd()
        await this.calculTotal()
        this.setState({isLoading : false})
    }

    fetchConnectedPresta = async () => {
        let presta = await getData('prestataire')

        this.setState({
            prestataire: presta
        })
    }

    fetchAllPrestataireCmd = async () => {

        let Num_prestataire = this.state.prestataire["Num_prestataire"]
        let response = await allCommande(Num_prestataire)

        if(response !== 'vide' && response !== undefined) {
            this.setState({facture : response} , () => this.setState({isMounted : true}) )
        }
        response === "vide" ? this.setState({facture : []}) : this.setState({facture : response} , () => this.setState({isMounted : true}))
        
        
    }

    _formatDate(date) {
        if(date !== undefined){
            let year = date.getFullYear();
            let month = date.getMonth() + 1;
            let dt = date.getDate();

            if (dt < 10) {
                dt = '0' + dt;
            }
            if (month < 10) {
                month = '0' + month;
            }

            return year + '-' + month + '-' + dt
        }
    }

    renderHistorique = (fact , i) => (
        <DataTable.Row key={i}>
            <DataTable.Cell>{fact['Nom_menu_commnder']}</DataTable.Cell>
            <DataTable.Cell>{fact['Date_du_commande']}</DataTable.Cell>

            <DataTable.Cell numeric>{fact['Nom_client']}</DataTable.Cell>
            <DataTable.Cell numeric>{fact['Prix_du_commande']} €</DataTable.Cell>
        </DataTable.Row>
    )

    search = async () =>{
        let temp=[]
        this.state.facture.forEach( async (fac) => {
            if(fac['Date_du_commande'] === this._formatDate(this.state.dateSearch) ){
                temp.push(fac)

            }
            
        });
        this.setState({facture: temp}, async() => await this.calculTotal())
    }

    calculTotal = async () => {
        let s = 0;
        // this.state.facture.(fact => s += this.calculDipr(fact["Prix_du_commande"]) )
        this.state.facture.forEach(fact => {
            console.log('prix',fact["Prix_du_commande"])
            // s += this.calculDipr(fact["Prix_du_commande"]) 
             s +=  parseInt(fact["Prix_du_commande"])

        });
        console.log('somme',s)
        this.setState({somme : s})
    }

    calculDipr = val =>{
        return val - (val * 0.1)
    }
    render() {

        // if(!this.state.isMounted){
        //     return null
        // }

        return (
            <ImageBackground source={require("../../../assets/home.jpg")}
                style={{ flex: 1, width: "100%" }}>
                {this.state.isLoading && <View style={styles.loading_container}>
                    <ActivityIndicator size="large" />
                </View>}
                <View>
                    <TopMenuP title="Historiques" navigation={this.props.navigation} />
                </View>
                <View style={{ flexDirection: "column", alignItems: "center" , paddingTop: 10 }}>
                    <Avatar.Image size={150} source={{ uri: `${PRESTA_URI}/${this.state.prestataire["nom_file"]}` }} />
                    <Text style={{ color: "yellow" }}>{this.state.prestataire["Nom_etablissement"]}</Text>
                </View>
                {/* <View style={{ flexDirection: "column", alignItems: "center" , paddingTop: 10 }}>
                    <TextInput  />
                </View> */}

                <View style={{flex: 0.1 , flexDirection: 'row' , marginHorizontal: 20}}>
                <View style={{ flex:5, flexDirection: 'column'}}>
                    <TouchableOpacity  style={{ alignItems: 'stretch', flexDirection: 'row' ,alignItems:'center', justifyContent: 'space-around' , backgroundColor: 'rgba(255,255,255,0.8)' , borderRadius: 20}}  onPress={ ()=> this.setState({openSearch : true})}>
                            <Icon type="MaterialIcons" name="search" style={{ color : 'white'}}/>
                            {/* <View style={{width : 20}}/> */}
                            <Text style={{ color: 'white'}}>
                                {this._formatDate(this.state.dateSearch)}
                            </Text>

                </TouchableOpacity>


                </View >
                <View style={{ flex:1 , flexDirection: 'column' , alignItems: 'flex-end'}}>
                    <TouchableOpacity onPress={ async ()=> await this.fetchAllPrestataireCmd()}>
                        <Icon type="MaterialIcons" name="cancel" style={{ color : 'red'}}/>                   
                    </TouchableOpacity>
                </View>              
                


                </View>
                


                
                        
                 
                {this.state.openSearch === true &&
                            <DateTimePicker
                                testID="dateTimePicker"
                                timeZoneOffsetInMinutes={0}
                                value={this.state.dateSearch}
                                mode="date"
                            
                                display="default"
                                onChange={(event, selectedDate) => {
                                    this.setState({ openSearch: false })
                                    if(selectedDate !== undefined)  this.setState({dateSearch:selectedDate},async ()=>  await this.search())
                                }}
                            />
                        }
                <View style={styles.container}>
                    <DataTable style={{backgroundColor : "white" , borderRadius: 10 }}>
                        <DataTable.Header >
                            <DataTable.Title >Menu</DataTable.Title>
                            <DataTable.Title numeric >Client</DataTable.Title>
                            <DataTable.Title numeric >Revenu </DataTable.Title>
                        </DataTable.Header>
                        {  this.state.facture.length > 0 &&  this.state.facture.map((fact,i)=>{
                            const dipr = this.calculDipr(fact.Prix_du_commande)
                            return this.renderHistorique(fact,i)
                        })} 
                    </DataTable>

                        <View style={{ flex: 0.3 ,flexDirection: 'row' , marginTop: 10 }}>
                            <View style={{  flex: 1  , flexDirection: 'column' ,  color: 'white'}}>
                                <Text style={{ color: 'white' ,fontWeight : "bold", fontSize : 20}}>
                                Total :
                                </Text>                            
                            </View>

                            <View style={{ flex: 1 , flexDirection: 'column' }}>
                                {this.state.somme != 0 &&  <Text style={{ justifyContent: 'flex-end' , color: 'white',textAlign : "right" ,fontWeight : "bold", fontSize : 20}}>{this.state.somme} €</Text>}

                            </View>
                        </View>                   
                </View>
            </ImageBackground>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        padding: 20,
    },
    loading_container: {
        position: 'absolute',
        zIndex: 10,
        left: 0,
        right: 0,
        top: 100,
        bottom: 0,
        alignItems: 'center',
        justifyContent: 'center'
    }
})

export default memo(Historiques)
{/* <View>
                {this.state.facture.length !== 0 && this.state.facture.map((fact,i)=>{
                    return <Text key={i}>{fact.ref_commande}</Text>
                })}
            </View> */}