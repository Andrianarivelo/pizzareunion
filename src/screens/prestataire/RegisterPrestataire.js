import React, { memo, useState,useEffect, useRef} from 'react'
import { View, Text, StyleSheet, TouchableOpacity, Image, StatusBar,ImageBackground ,Animated,Dimensions,Keyboard, KeyboardAvoidingView, Easing} from 'react-native'
import TextInput from "../../components/TextInput"
import SignButton from "../../components/SignButton"
import Background from "../../components/Background"
import {theme} from "../../core/theme"
import {  passwordValidator, userExist, nameValidator ,formatTime , prenomValidator, formatDate , phoneValidator,emailValidator, noSpecCharValidator, isNotNull, numberValidator} from "../../core/utils";
import ViewPager from '@react-native-community/viewpager';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen'
import * as Permissions from 'expo-permissions'
import * as ImagePicker from 'expo-image-picker'
import { ScrollView } from 'react-native-gesture-handler'
import { signUpClient } from '../../API/client/authClient'
import { signUpPrestataire } from '../../API/prestataire/authPrestataire'
// import { Button } from 'react-native-paper';

import DateTimePicker from '@react-native-community/datetimepicker';
var {width} = Dimensions.get('window')

import {Icon , Item, Picker,Form, Container, Content,Header,Tabs,Tab,Button } from 'native-base'
import { SafeAreaView } from 'react-native-safe-area-context'
// import { Easing } from 'react-native-reanimated'


const AnimatedTabs = Animated.createAnimatedComponent(Tabs);

const RegisterScreen = ({ navigation }) => {
  const [nom, setNom] = useState({ value: "", error: "" });
  const [prenom, setPrenom] = useState({ value: "", error: "" })
  const [email, setEmail] = useState({ value: "", error: "" });
  const [password, setPassword] = useState({ value: "", error: "" });
  const [confirmPassword, setConfirmPassword] = useState({value : "", error :""})
  const [loading, setLoading] = useState(false);
  const [pseudo, setPseudo] = useState({ value: "", error: "" })
  const [phone, setPhone] = useState({ value: "", error: "" })
  const [etab, setEtab] = useState({ value:"", error: "" })
  const [ouverture, setOuverture] = useState({ value:'', error: "" })
  const [fermeture, setFermeture] = useState({ value:'', error: "" })
  const [houverture, setHOuverture] = useState({ value: new Date(), error: "" })
  const [hfermeture, setHFermeture] = useState({ value: new Date(), error: "" })
  const [numRue, setNumRue] = useState({ value: "", error: "" })
  const [rue, setRue] = useState({ value: "", error: "" })
  const [ville, setVille] = useState({ value: "", error: "" })
  const [codePostale, setCodePostale] = useState({ value: "", error: "" })

  const [error, setError] = useState("")
  const [image, setImage] = useState("")
  const [displayImg,setDisplayImg] = useState(null)

  const [currentPage, setCurrentPage] = useState(0)
  const [openDate, setOpenDate] =useState(false)
  const [closeDate, setCloseDate] = useState(false)
  const [time, setTime] = useState(new Date())
  const [activeTab, setActiveTab] = useState(0)

  const [page,setPage] = useState(null)


  const [validInfo, setvalidInfo] = useState(false)
  const [validCompte, setValidCompte] = useState(false)
  
  const week = ['Lundi','Mardi','Mercredi','Jeudi','Vendredi','Samedi','Dimanche']

  const [keyboardShown, setkeyboardShown] = useState(false)


  const dynamicHeight = new Animated.Value(width)

    useEffect(() => {   
      animate()  
      Keyboard.addListener("keyboardDidShow", _keyboardDidShow);
      Keyboard.addListener("keyboardDidHide", _keyboardDidHide);

      // cleanup function
      return () => {
        Keyboard.removeListener("keyboardDidShow", _keyboardDidShow);
        Keyboard.removeListener("keyboardDidHide", _keyboardDidHide);
        // setActiveTab(0)
      };
    }, [activeTab]);

    const _keyboardDidShow = () => {
      // alert("Keyboard Shown");
      setkeyboardShown(true)
    };

    const _keyboardDidHide = () => {
      setkeyboardShown(false)
      // alert("Keyboard Hidden");
    };


  const _handleSignUp = async () => {

    if (loading) return;

    /* const nameError = nameValidator(name.value);
    const emailError = emailValidator(email.value);
    const passwordError = passwordValidator(password.value);

    if (emailError || passwordError || nameError) {
      setName({ ...name, error: nameError });
      setEmail({ ...email, error: emailError });
      setPassword({ ...password, error: passwordError });
      return;
    } */


    const response = await signUpClient({
      nom: nom.value,
      phone: phone.value,
      pseudo: pseudo.value,
      email: email.value,
      password: password.value
    })

    if (response == "emailuser") {
      const emailError = userExist(email.value);
      setEmail({ ...email, error: emailError })
    }   

    return
  }

  const upload = async () => {
    let result = await ImagePicker.launchImageLibraryAsync({
      base64: true
    });

    if (!result.cancelled) {
      setError('')
      setImage(result.base64)
      setDisplayImg(result.uri)
    }
  }

  const animate = ()=>{
    switch (activeTab) {
      case 1:
        Animated.timing(dynamicHeight,{
          toValue: width - 20 ,
          easing :Easing.ease,
          duration: 200
        }).start();        
        break;

      case 2:
        Animated.timing(dynamicHeight,{
          toValue: width * 2 - 100, 
          easing :Easing.ease,
          duration: 100
        }).start();        
        break;
      default:   
      Animated.timing(dynamicHeight,{
        toValue: width ,
        easing :Easing.ease,
        duration: 200
      }).start();        
        break;
    }


    if(activeTab === 1){
     return 
    }
    
  }

  const confirmInfo = ()=>{
    if(email.error){
      setEmail({...email,error:email.error})
    }else{
      let nomError = nameValidator(nom.value)
      let prenomError = prenomValidator(prenom.value)
      let emailError  = emailValidator(email.value)
      let phoneError = phoneValidator(phone.value)

      if(nomError || prenomError || emailError || phoneError){
        setNom({ ...nom,error:nomError})
        setPrenom({ ...prenom,error:prenomError})
        setEmail({ ...email,error:emailError})
        setPhone({ ...phone,error:phoneError})
      }else{
        setvalidInfo(true)               
        setActiveTab(1)
        page.goToPage(1)
  
        
      }
    }
  }

  const confirmCompte = ()=>{
    if(!validInfo){
      setActiveTab(0)
      page.goToPage(0)
      return confirmInfo()
    }

    if(confirmPassword.value !== password.value){
      setConfirmPassword({...password,error : "Mot de passe non identique"})
    }
    let pseudoError = noSpecCharValidator(pseudo.value)
    let passwordError = passwordValidator(password.value)
    let imageError = (displayImg === null) ? 'Veuillez insérer une image' : ''

    if( pseudoError || passwordError || imageError){
      setPseudo({...pseudo,error: pseudoError})
      setPassword({...password,error: passwordError})
      setError(imageError)

    }else{
      setValidCompte(true)
      setActiveTab(2)
      page.goToPage(2)

    }
  }

  const inscription = async () => {

      if(!validInfo) {
        setActiveTab(0)
        page.goToPage(0)
        return confirmInfo()
      }

      if(!validCompte){
        setActiveTab(1)
        page.goToPage(1)
        return confirmCompte()
      }


      let etabError =  isNotNull(etab.value)
      let ouvertureError = isNotNull(ouverture.value)
      let fermetureError = isNotNull(fermeture.value)
      let villeError = isNotNull(ville.value)
      let codeError = numberValidator(codePostale.value)
      let rueError = noSpecCharValidator(rue.value)
      let numRueError = numberValidator(numRue.value)

      if(etabError || ouvertureError || fermetureError || villeError ||  codeError || rueError || numRueError){
        setEtab({...etab,error:etabError})
        setOuverture({...ouverture, error:ouvertureError})
        setFermeture({...fermeture,error:fermetureError})
        setVille({...ville,error:villeError})
        setCodePostale({...codePostale,error:codeError})
        setRue({...rue,error:rueError})
        setNumRue({...numRue,error : numRueError})
      }else{
        setLoading(true)
        console.log("entrai de")
        let data = {
          nom: nom.value,
          prenom: prenom.value,
          email: email.value,
          telephone: phone.value,
          pseudo: pseudo.value,
          password: password.value,
          nom_etablissement: etab.value,
          ouverture: ouverture.value,
          fermeture: fermeture.value,
          postale: codePostale.value,
          ville_prestataire: ville.value,
          heure_overture: formatTime(houverture.value),
          heure_fermeture: formatTime(hfermeture.value),
          Numero_du_rue: numRue.value,
          Rue: rue.value,
          image: image,
          nom_image: Math.random()
        }



        let response = await signUpPrestataire(data)
        console.log(response)

        if(response === "emailused"){       
            setEmail({...email, error : "Adresse email utilisé"})
            page.goToPage(0)
            setActiveTab(0)
            setLoading(false)
        }else if(response === 'ok'){
          setLoading(false)
          navigation.navigate("RegisterDone")
        }   

      }    
  }

  const renderInformation = () => (
    // <View style={styles.page}>
    <Animated.View style={{flexDirection: 'row' , height: dynamicHeight}}>
          <View  style={{ flexGrow:1, flexDirection:'column'}}>
        <TextInput
          label="Nom"
          autoFocus={true}
          returnKeyType="next"
          autoCapitalize="words"
          value={nom.value}
          onChangeText={text => setNom({ value: text, error: "" })}
          error={!!nom.error}
          errorText={nom.error}
        />
        <TextInput
          label="Prénom"
          returnKeyType="next"
          autoCapitalize="words"
          value={prenom.value}
          onChangeText={text => setPrenom({ value: text, error: "" })}
          error={!!prenom.error}
          errorText={prenom.error}
        />


        <TextInput
          label="Téléphone"
          returnKeyType="next"
          autoCapitalize="none"
          keyboardType='phone-pad'
          value={phone.value}
          onChangeText={text => setPhone({ value: text, error: "" })}
          error={!!phone.error}
          errorText={phone.error}
        />

        <TextInput
          label="Email"
          keyboardType='email-address'
          returnKeyType="next"
          autoCapitalize ="none"
          value={email.value}
          onChangeText={text => setEmail({ value: text, error: "" })}
          error={!!email.error}
          errorText={email.error}

        />

        {/* <View style={{height:20}}/> */}


        <View style={{alignItems:'flex-end'}}>

          <TouchableOpacity style={styles.buttonStep}  onPress={() => confirmInfo() }>
            <Icon  name='arrow-forward' style={styles.iconStep} />
            {/* <Text style={styles.labelStep}> Etape suivante </Text> */}
          </TouchableOpacity>
        </View>

        {/* <BottomView/> */}
      
    </View>
  </Animated.View>
  )


 const  renderCompte = () => (
  // <SafeAreaView style={{flexDirection: 'row' }}>
    <View  style={{flexGrow:1, flexDirection:'column'}}>
        <TextInput
          label="Pseudonyme"
          returnKeyType="next"
          autoCapitalize="none"
          value={pseudo.value}
          onChangeText={text => setPseudo({ value: text, error: "" })}
          error={!!pseudo.error}
          errorText={pseudo.error}
        />
        <TextInput
          label="Mot de passe"
          returnKeyType="next"
          autoCapitalize="none"
          value={password.value}
          onChangeText={text => setPassword({ value: text, error: "" })}
          error={!!password.error}
          errorText={password.error}
          secureTextEntry

        />
        <TextInput
          label="Confirmation mot de passe"
          returnKeyType="done"
          autoCapitalize="none"
          value={confirmPassword.value}
          onChangeText={text => setConfirmPassword({ value: text, error: "" })}
          error={!!confirmPassword.error}
          errorText={confirmPassword.error}
          secureTextEntry

        />      
       {/* {displayImg !== null && <Text>Votre image a été selectionnée</Text>} */}

      { displayImg!== null ?
        <View style={{ flexDirection: 'row',padding:10 ,alignSelf:'flex-start'}}>
          <View>
            <Image width={10} style={{width : width/5 , height:width/5, borderRadius:5 ,opacity: 0.5 }} source={{uri : displayImg}} />
          </View>
          <TouchableOpacity onPress={()=> setDisplayImg(null)} elevation={5} style={{
            borderWidth:1,
            borderColor:'rgba(0,0,0,0.2)',
            alignItems:'center',
            justifyContent:'center',
            width:30,
            height:30,
            backgroundColor:'rgba(205, 205, 205,0.5)',
            borderRadius:50,
            position: 'absolute',
            top:0,
            right:0
          }}>
            <Icon type="MaterialCommunityIcons" name="close" style={{ color: 'black',  fontSize:20}} />
          </TouchableOpacity>
        </View>
        :
        
        <View  style={{ flexDirection: 'row' ,alignSelf:'center'}}>
          <Text style={{color:theme.colors.error}}>{error} </Text>

        </View>
      
      
      }
        {/* <View style={{ height: 10}}/> */}

     
      <View style={{ flex:1,flexDirection:'row'}}>
        <View style={{flex:1,flexDirection:'column',alignItems:'flex-start'}}>
          <TouchableOpacity  style={styles.buttonStep}  onPress={() => page.goToPage(0)}>
            <Icon name='arrow-back' style={styles.iconStep} />
          </TouchableOpacity>
        </View>

        <View style={{flex:1,flexDirection:'column',alignItems:'center'}}>
          <TouchableOpacity  style={[styles.buttonStep,  error  ? {backgroundColor:theme.colors.error } :{backgroundColor:'#E1DEDB'}]}  onPress={() => upload()}>
            <Icon  type='MaterialIcons' name='photo' style={styles.iconStep}/>
          </TouchableOpacity>
        </View>

        <View style={{flex:1,flexDirection:'column',alignItems:'flex-end'}}>
          <TouchableOpacity style={styles.buttonStep}  onPress={() => confirmCompte()}>
              <Icon name='arrow-forward' style={styles.iconStep}/>
          </TouchableOpacity>
        </View>
      </View>

      {/* <BottomView/> */}
     
      </View>
      // </SafeAreaView>
      
  )
  const renderBoutique = () => (
    <View style={{flexGrow:1,flexDirection: 'row'}}>
      <View  style={{flexGrow:1,flexDirection:'column'}}>
        
        <TextInput
            label="Nom etablissement"
            returnKeyType="next"
            value={etab.value}
            onChangeText={text => setEtab({ value:text, error: "" })}
            error={!!etab.error}
            errorText={etab.error}
          />
        

        <View style={{ flexDirection:'row' ,flex: 1 ,marginVertical:10}}>
          <View style={{flex:1,flexDirection:'column',paddingRight:10 }}>
            <Item picker style={[{ borderRadius:10 ,minHeight:40,height:40 } , ouverture.error? {backgroundColor: 'rgba(247, 125, 120,0.8)' } : {backgroundColor:'white'}  ]} >
                <Picker
                  mode="dropdown"
                  iosIcon={<Icon name="calendar" />}
                  style={{ width: undefined }}
                  placeholder="Selectionner"
                  placeholderStyle={{ color: "grey" }}
                  placeholderIconColor="#007aff"
                  selectedValue={ouverture.value}
                  onValueChange={(value)=>setOuverture({value:value,error:''})}
                  style={ ouverture.value === "" ?   {color:"#CDCDCD" } :  {color:"black" }}
                >
                  <Picker.Item label="Ouverture" value="" color="#CDCDCD" />
                  
                  <Picker.Item label="Lundi" value="Lundi" />
                  <Picker.Item label="Mardi" value="Mardi" />
                  <Picker.Item label= "Mercredi" value="Mercredi" />
                  <Picker.Item label="Jeudi" value="Jeudi" />
                  <Picker.Item label="Vendredi" value="Vendredi" />
                  <Picker.Item label="Samedi" value="Samedi"/> 
                  <Picker.Item label="Dimanche" value="Dimanche"/>
                </Picker>
            </Item>

            { ouverture.error ?
            <Text style={{color: theme.colors.error }}> {ouverture.error}  </Text> : null}
                        
            <View style={{height:10}} />

            <View style={{flex:1,width:'100%' , minHeight:40 , maxHeight:40,borderRadius:10,flexDirection:'row' ,justifyContent:'center',backgroundColor:'white' ,alignItems:'center'}}>
              <TouchableOpacity  onPress={()=> setOpenDate(true)}>
                <Icon type='MaterialCommunityIcons' name='clock-start' style={{fontSize:30 ,color:'#CDCDCD'}}/>
              </TouchableOpacity>
              <Text style={{ color: 'black',fontWeight:'bold'}}> {formatTime(houverture.value)} </Text>   
            </View>    
          
          </View>


          <View style={{flex:1,flexDirection:'column',paddingLeft:10}}>
            <Item picker style={[{ borderRadius:10 ,minHeight:40,maxHeight:40 }, fermeture.error.length > 0 ? {backgroundColor: 'rgba(247, 125, 120,0.8)' } : {backgroundColor:'white'} ]} >
                <Picker
                  mode="dropdown"
                  iosIcon={<Icon name="calendar" />}
                  style={{ width: undefined }}
                  placeholder="Selectionner"
                  placeholderStyle={{ color: "grey" }}
                  placeholderIconColor="#007aff"
                  selectedValue={fermeture.value}
                  onValueChange={(value)=>setFermeture({value:value,error:''})}
                  style={ fermeture.value === "" ?   {color:"#CDCDCD" } :  {color:"black" }}
                >
                  <Picker.Item  label="Fermeture" value="" key={0} color="#CDCDCD"/>
                  
                  <Picker.Item label="Lundi" value="Lundi" key={0} />
                  <Picker.Item label="Mardi" value="Mardi" key={1} />
                  <Picker.Item label= "Mercredi" value="Mercredi"  key={2} />
                  <Picker.Item label="Jeudi" value="Jeudi" key={3}  />
                  <Picker.Item label="Vendredi" value="Vendredi"  key={4}/>
                  <Picker.Item label="Samedi" value="Samedi" key={5} /> 
                  <Picker.Item label="Dimanche" value="Dimanche" key={6} />
                </Picker>              
          </Item> 
          { fermeture.error ? <Text style={{color: theme.colors.error}}> {fermeture.error}  </Text> : null}
          
          <View style={{height:10}} />

          <View style={{flex:1,width:'100%' ,height:40, minHeight: 40, maxHeight:40,borderRadius:10,flexDirection:'row' ,justifyContent:'center',backgroundColor:'white' ,alignItems:'center'}}>
              <TouchableOpacity    onPress={()=> setCloseDate(true)}>
                <Icon type='MaterialCommunityIcons' name='clock-end' style={{fontSize:30 ,color:'#CDCDCD'}}/>             
              </TouchableOpacity>
              <Text style={{ color : 'black' ,fontWeight:'bold'}}> {formatTime(hfermeture.value)} </Text>  
                             
          </View>
      
        </View>
      </View>


        {closeDate === true && 
          <DateTimePicker
            testID="dateTimePicker"
            timeZoneOffsetInMinutes={0}
            value={time}
            mode="time"
            is24Hour={true}
            display="default"
            onChange={(event, selectedDate) => {            
              setCloseDate(false)
              if(selectedDate !== undefined) return  setHFermeture({value : selectedDate,error :""})
            }}
        />}

        {openDate ===true &&
          <DateTimePicker
            testID="dateTimePicker"
            timeZoneOffsetInMinutes={0}
            value={time}
            mode="time"
            is24Hour={true}
            display="default"
            onChange={(event, selectedDate) => {            
              setOpenDate(false)
              if(selectedDate !== undefined) return setHOuverture({ value :selectedDate ,error:""})
            }}
          /> }          

        <TextInput
          label="Ville"
          returnKeyType="next"
          autoCapitalize="words"
          value={ville.value}
          onChangeText={text => setVille({ value: text, error: "" })}
          error={!!ville.error}
          errorText={ville.error}

        />
        <TextInput
          label="code postale"
          keyboardType='numeric'
          returnKeyType="next"
          autoCapitalize="none"
          value={codePostale.value}
          onChangeText={text => setCodePostale({ value: text, error: "" })}
          error={!!codePostale.error}
          errorText={codePostale.error}

        />
        <TextInput
          label="Rue"
          returnKeyType="next"
          autoCapitalize="words"
          value={rue.value}
          onChangeText={text => setRue({ value: text, error: "" })}
          error={!!rue.error}
          errorText={rue.error}

        />
        <TextInput
          label="Numero de rue"
          returnKeyType="next"
          autoCapitalize="none"
          value={numRue.value}
          onChangeText={text => setNumRue({ value: text, error: "" })}
          error={!!numRue.error}
          errorText={numRue.error}

        />
    

      <TouchableOpacity style={styles.buttonStep}  onPress={() => page.goToPage(1)}>
        <Icon name='arrow-back' style={styles.iconStep} />
      </TouchableOpacity>
      {/* <View> */}
        <SignButton
          mode="contained"
          loading={loading}
          // style={styles.button}
          onPress={inscription}>
          M'inscrire
        </SignButton>
      {/* </View> */}
      {/* <BottomView/> */}
      </View>
    </View> 
  )

  const BottomView = ()=>(
    <View style={styles.row}>
          <Text style={{ color: "white" }}>Déjà notre prestataire ? </Text>
          <TouchableOpacity onPress={() => navigation.navigate("LoginPrestataire")}>
            <Text style={styles.link}>Se connecter</Text>
          </TouchableOpacity>
        </View>
  )

  return (

    // <ScrollView centerContent={true} bounces= {true} style={{ backgroundColor: 'blue', height:100}}>
    <Background style={styles.container}>
      <ScrollView contentContainerStyle={styles.content}>
      
        <View style={{ flexDirection:'column', padding:20 ,borderRadius:50 , alignItems:'center',justifyContent: 'center',backgroundColor:'white'}}>
          <Image  
          style={styles.image}
          source={require("../../../assets/logo.png")}
          resizeMode="contain"
          />
        </View>
        <View style={{ flexDirection:'column', alignItems:'center',justifyContent: 'center',marginTop:10 }}>
          <Text style={{ color: 'white',fontSize:18}}>INSCRIPTION</Text>  
        </View>

        <View style={{ flexDirection:'column', marginTop:5,padding:1,width:50 ,alignItems:'center',justifyContent: 'center',backgroundColor:'white'}}/>


        <View style={{height:20}}/>

          <Animated.View style={{ flex: 0.1 }} > 
          {/* <Container style={{ flex:1 ,alignItems:'center',justifyContent:'center'}}> */}
        <Tabs onChangeTab={(tab) => { setActiveTab(tab.i) }}   page={activeTab}   ref={component => setPage(component)}  tabBarPosition="top" tabBarUnderlineStyle={{ backgroundColor:theme.colors.primary}} style={{  marginHorizontal:20}} >
          <Tab    heading="Informations" activeTextStyle={{color:theme.colors.primary}} tabStyle={{ backgroundColor:'white' }} activeTabStyle={{backgroundColor: 'white' }} style={{backgroundColor:'transparent',flex:1 }}>
            {renderInformation()}
          </Tab> 
          <Tab   heading="Compte"   activeTextStyle={{color:theme.colors.primary}} tabStyle={{ backgroundColor:'white' }}  activeTabStyle={{backgroundColor: 'white' }}  style={{flex:1, backgroundColor:'transparent'}}  tabBarTextStyle={{color: 'green'}}>
            {renderCompte()}
          </Tab>
          <Tab   heading="Boutique"  activeTextStyle={{color:theme.colors.primary}} tabStyle={{ backgroundColor:'white' }} activeTabStyle={{backgroundColor: 'white' }}  style={{flexGrow:1,backgroundColor:'transparent' }}>
            {renderBoutique()}
          </Tab>
        </Tabs>
        </Animated.View>
        {/* {renderInformation()} */}
        
        {/* </Container> */}
        <BottomView/>

      </ScrollView>
    </Background>
    
    // </ScrollView>



  )
}

const styles = StyleSheet.create({
  container:{
    // paddingTop:StatusBar.currentHeight || 0,
    flex:1,
    alignItems:'center'
    // backgroundColor:'blue'
  },  
  content:{
      flexGrow: 1, 
      flexDirection:'column',
      justifyContent: 'center',
      alignItems:'center',
      // paddingHorizontal:20
  } , 
  label: {
    color: theme.colors.secondary
  },
  button: {
    marginTop: 24,
    backgroundColor : 'rgb(253, 165, 0)'
  },
  row: {
    flexDirection: "row",
    paddingTop: 20,
    // paddingBottom:20
  },
  link: {
    fontWeight: "bold",
    color: theme.colors.primary
  },
  viewPager: {
    flex: 1,
  },
  page: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  image:{
    width : width /4 -30,
    height: width /4 -30,
    },
    card: {
      flex: 1,
      borderRadius: 4,
      borderWidth: 2,
      borderColor: "#E8E8E8",
      justifyContent: "center",
      backgroundColor: "white"
    },
  buttonStep:{
    borderWidth:1,
    borderColor:'rgba(0,0,0,0.2)',
    alignItems:'center',
    justifyContent:'center',
    width:50,
    height:50,
    backgroundColor:theme.colors.primary,
    borderRadius:50,
  },
  iconStep:{
    color: 'white',
    fontSize:20

  }
});

export default memo(RegisterScreen)

