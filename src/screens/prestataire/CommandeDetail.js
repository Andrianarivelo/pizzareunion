import React, { Component, memo } from 'react'
import { Text, View, StyleSheet, ImageBackground, ActivityIndicator , Dimensions ,Image , TouchableOpacity, ScrollView} from 'react-native'
import { validerCommande, annulerCommande } from "../../API/prestataire/commande";
import { currentPrestataire } from '../../core/session'
import { Avatar, Card, Paragraph } from 'react-native-paper';
import { CommandeRef } from '../../API/prestataire/list';
import { MENU_URI } from '../../core/config';
import TopMenuP from "../../components/TopMenuP"
import AsyncStorage from '@react-native-community/async-storage';
import { Title,Button,Container, Header, Content,Thumbnail, Icon, Left, Body, Right} from 'native-base'
import { connectedClient } from '../../API/client/authClient';
import * as Font from 'expo-font'
import { AppLoading } from 'expo';
import { Ionicons } from '@expo/vector-icons';
var {height , width} = Dimensions.get('window');
// import {theme} from '../../core/theme'



class CommandeDetail extends Component {

    constructor(props) {
        super(props)
            this.state = {
                prestataire: "",
                commande: [],
                image: "",
                isLoading: false,
                ref_commande  : null,
                isReady: false
            }

    }



    componentDidMount = async () => {
        await Font.loadAsync({
            Roboto: require('native-base/Fonts/Roboto.ttf'),
            Roboto_medium: require('native-base/Fonts/Roboto_medium.ttf'),
            ...Ionicons.font,
          });
          this.setState({ isReady: true });

        this.setState({ isLoading: true })
        await this.fetchConnectedPresta()
        this.setState({ref_commande : this.props.navigation.state.params.ref_commande},async ()=>{
            await this.fetchCommandeInfo()
        })
        this.setState({ isLoading: false })
    }

    fetchCommandeInfo = async () => {
        let response = await CommandeRef(this.state.ref_commande)

        this.setState({ commande: response })
        let imageNamePath = this.state.commande["file"].split("/")
        let a = imageNamePath[0] + "/menus/" + imageNamePath[1]
        this.setState({ image: a })
        console.log('commande',this.state.commande)

    }

  

    fetchConnectedPresta = async () =>{
        try {
            await AsyncStorage.getItem('prestataire').then((res)=>{
                this.setState({prestataire : JSON.parse(res)})
            })
                
            } catch(e) {
            console.log(e)
            }
    }


    sendCommande = async () => {
        this.setState({ isLoading: true })
        let ref_commande = this.state.ref_commande
        let Num_prestataire = this.state.prestataire["Num_prestataire"]
        let response = await validerCommande(ref_commande, Num_prestataire)

        //console.log(response)
        this.setState({ isLoading: false })
        this.props.navigation.navigate("MainPrestataire")
    }



    cancelCommande = async () => {
        this.setState({ isLoading: true })
        let ref_commande = this.state.ref_commande

        let response = await annulerCommande(ref_commande)
        this.setState({ isLoading: false })
        this.props.navigation.navigate("CommandePrestataire")

    }
    render() {

        if (!this.state.isReady) {
            return <AppLoading />;
        }

        return (
            <ImageBackground
                source={require("../../../assets/home.jpg")}
                style={{ flex: 1, width: "100%"}}
            >
                {this.state.isLoading && <View style={styles.loading_container}>
                    <ActivityIndicator size="large" />
                </View>}

                {/* <View> */}
                    <TopMenuP title="Détails" navigation={this.props.navigation} />
                {/* </View> */}
                <ScrollView>
                 <View style={styles.container}> 
                    {/* <Card>
                        <Card.Title title={this.state.commande["Nom_client"]} subtitle={this.state.commande["Email_client"]} />
                        <Card.Content>
                            <Title>{this.state.commande["Nom_menu_commnder"]}</Title>
                            <Paragraph>{this.state.commande["Quantiter_menu_commnder"]}</Paragraph>
                            <Paragraph>{this.state.commande["Prix_du_commande"]} euro</Paragraph>
                        </Card.Content>
                        <Card.Cover source={{ uri: `${MENU_URI}`+ this.state.image }} />
                        <Card.Actions>
                            <Button danger style={{ backgroundColor: "red", margin: 2 }} onPress={() => this.cancelCommande()}>
                                <Text> Annuler </Text>
                            </Button>
                            <Button success style={{ backgroundColor: "green", margin: 2 }} onPress={() => this.sendCommande()}>
                               <Text>Valider</Text> 
                            </Button>
                        </Card.Actions>
                    </Card> */}

                   <View style={{ flex:1 ,flexDirection: 'column' }}>
                        <Text style={{ color: 'white'}}>Nom : {this.state.commande["Nom_client"]} {this.state.commande["Prenom_client"]}</Text>
                        <Text  style={{ color: 'white'}}>Email : {this.state.commande["Email_client"]}</Text>
                        <Text style={{ color: 'white'}}>Pour le : {this.state.commande["date_recep"]} { this.state.commande["date_recep"] ? `à ${this.state.commande["heure_recep"]}` : ''}</Text>
                        <Text style={{ color: 'white'}} >{this.state.commande["Mode_de_paiement"]} </Text>
      
                    </View>
                    <View style={{  flexDirection: 'row' ,height: 20  }}/>

                            <View style={{ borderWidth: 1, borderRadius: 10 , borderColor: 'rgb(253, 165, 0)',  flex:6 ,flexDirection:'row', paddingBottom:10}}>
                            
                                <Image resizeMode="contain" style={{width:width/3,height:width/3}} source={{ uri: `${MENU_URI}`+ this.state.image }} />
                                <View style={{flex:1, backgroundColor:'transparent', padding:10, justifyContent:"space-between"}}>
                                <View>
                                    <Text style={{fontWeight:"bold", fontSize:20 ,color: 'white' }}>{this.state.commande["Nom_menu_commnder"]}</Text>
                                    <Text style={{ color: 'white' }}>{this.state.commande["Description_menu_commnder"]}</Text>
                                </View>
                                <View style={{flexDirection:'row',justifyContent:'space-between'}}>
                                    <Text style={{fontWeight:'bold',color:"rgb(253, 165, 0)",fontSize:20}}>{ 'Quantité : '+this.state.commande["Quantiter_menu_commnder"]}</Text>
                                    <Text style={{paddingHorizontal:8, fontWeight:'bold', fontSize:18 ,color: 'white' }}>{this.state.commande["Prix_du_commande"]}€</Text>
                                    
                                </View>
                                </View>
                            </View>
                  


                    <View style={{  flexDirection: 'row' ,height: 20 }}/>

                    <View style={{ flexDirection: 'row' , justifyContent: 'space-evenly' }}>
                      <TouchableOpacity style={styles.buttonCancel} onPress={() => this.cancelCommande()}>
                            <Text> Annuler </Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.buttonSuccess} onPress={() => this.sendCommande()}>
                            <Text>Valider</Text> 
                        </TouchableOpacity>


                      </View>
                      


                </View>
                </ScrollView>
            </ImageBackground>

        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        // flexDirection: 'row',
        padding : 10 ,
        // alignItems: 'center'
    },
    loading_container: {
        position: 'absolute',
        zIndex: 10,
        left: 0,
        right: 0,
        top: 100,
        bottom: 0,
        alignItems: 'center',
        justifyContent: 'center'
    },
    buttonCancel:{
        padding: 20,
        elevation:8,
        shadowOpacity:0.3,
        shadowRadius:50,
        borderRadius: 10,
        marginHorizontal : 10,
        padding: 10,
        backgroundColor: '#f13a59'
    },
    buttonSuccess:{
        padding: 20,
        elevation:8,
        shadowOpacity:0.3,
        shadowRadius:50,
        borderRadius: 10,
        marginHorizontal : 10,
        padding: 10,
        backgroundColor: '#00B386'
    }
})
export default memo(CommandeDetail)