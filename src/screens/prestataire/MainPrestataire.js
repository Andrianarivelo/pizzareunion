import React, { Component, memo } from 'react'
import { Text, View, ImageBackground, StyleSheet,ActivityIndicator, Image, TouchableOpacity , Dimensions, FlatList,ScrollView } from 'react-native'
import { currentPrestataire, getData } from '../../core/session'
import { PRESTA_URI } from '../../core/config'
import { allCommande } from "../../API/prestataire/list";
import DateTimePicker from '@react-native-community/datetimepicker';
import { DataTable,Avatar } from 'react-native-paper';
var {width} = Dimensions.get('window')
import {
    widthPercentageToDP as wp,
    heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
var{width} = Dimensions.get('window')
import TopMenuP from "../../components/TopMenuP"
import {Icon} from 'native-base'
import LinearGradient from 'react-native-linear-gradient';
import {theme} from  '../../core/theme'
import { createMaterialBottomTabNavigator } from '@react-navigation/material-bottom-tabs';
import { NavigationContainer } from '@react-navigation/native'
import {createStackNavigator} from '@react-navigation/stack'
import Ionicons from 'react-native-vector-icons/MaterialCommunityIcons';
import Historiques from './Historiques';
import Menu from './Menu'
import Commande from './Commande'
import HomeScreen from '../HomeScreen';

const Tab = createMaterialBottomTabNavigator();


const options =[
    {
        label:'MENU',
        icon: 'menu',
        screen:'Menu'
    },
    {
        label:'HISTORIQUE',
        icon:'history',
        screen: 'Historiques'
    },
    {
        label:'COMMANDES',
        icon:'cart-arrow-right',
        screen: 'CommandePrestataire'
    }
]


class MainPrestataire extends Component {

    constructor() {
        super()
        this.state = {
            facture: [],
            prestataire: {},
            isLoading: false,
            somme : 0,
            openSearch : false,
            dateSearch : new Date(),
            isMounted : false
        }
    }

    componentDidMount = async () => {
        this.setState({isLoading : true})
        await this.fetchConnectedPresta()
        await this.fetchAllPrestataireCmd()
        await this.calculTotal()
        this.setState({isLoading : false})

    }



    fetchConnectedPresta = async () => {
        let presta = await getData('prestataire')
        this.setState({
            prestataire: presta
        })
    }

    fetchAllPrestataireCmd = async () => {

        let Num_prestataire = this.state.prestataire["Num_prestataire"]
        let response = await allCommande(Num_prestataire)

        if(response !== 'vide' && response !== undefined) {
            this.setState({facture : response} , () => this.setState({isMounted : true}) )
        }
        response === "vide" ? this.setState({facture : []}) : this.setState({facture : response} , () => this.setState({isMounted : true}))
        
        
    }

     _formatDate(date) {
        if(date !== undefined){
            let year = date.getFullYear();
            let month = date.getMonth() + 1;
            let dt = date.getDate();

            if (dt < 10) {
                dt = '0' + dt;
            }
            if (month < 10) {
                month = '0' + month;
            }

            return year + '-' + month + '-' + dt
        }
    }

    renderHistorique = (fact , i) => (
        <DataTable.Row key={i}>
            <DataTable.Cell>{fact['Nom_menu_commnder']}</DataTable.Cell>
            <DataTable.Cell>{fact['Date_du_commande']}</DataTable.Cell>

            <DataTable.Cell numeric>{fact['Nom_client']}</DataTable.Cell>
            <DataTable.Cell numeric>{fact['Prix_du_commande']} €</DataTable.Cell>
        </DataTable.Row>
    )

    search = async () =>{
        let temp=[]
        this.state.facture.forEach( async (fac) => {
            if(fac['Date_du_commande'] === this._formatDate(this.state.dateSearch) ){
                temp.push(fac)

            }
            
        });
        this.setState({facture: temp}, async() => await this.calculTotal())
    }

    calculTotal = async () => {
        let s = 0;
        // this.state.facture.(fact => s += this.calculDipr(fact["Prix_du_commande"]) )
        this.state.facture.forEach(fact => {
            console.log('prix',fact["Prix_du_commande"])
            // s += this.calculDipr(fact["Prix_du_commande"]) 
             s +=  parseInt(fact["Prix_du_commande"])

        });
        console.log('somme',s)
        this.setState({somme : s})
    }

    calculDipr = val =>{
        return val - (val * 0.1)
    }

    _renderCart(item){
        return(
            <TouchableOpacity style={{ margin: 10,alignItems:'center',justifyContent:'center',width: width/2-30 ,height: width/2 - 30 ,backgroundColor:'rgba(205, 205, 205,0.5)', borderRadius:10 }}
                onPress={()=> this.props.navigation.navigate(item.screen)}
            
            >
                <Icon name={item.icon} type='MaterialCommunityIcons' style={{fontSize:70 }} />
                <Text>{item.label} </Text>

            </TouchableOpacity>
        )

    }

    render() {
        return (
            // <ImageBackground
            //     source={require("../../../assets/home.jpg")}
            //     style={{ flex: 1 ,height:'100%'}}
            // >

            <View style={{ flex:1 , backgroundColor:theme.colors.primary}}>

              

                {/* <View> */}
                     <TopMenuP  navigation={this.props.navigation} title="Dashboard"   />
                {/* </View> */}

                <View style={{padding: 10 }}>

                    <View style={{  flexDirection:'row' }}>
                        <Avatar.Image size={width/6} source={{ uri: `${PRESTA_URI}/${this.state.prestataire["nom_file"]}` }} />
                        <View style={{width:10}}/>
        
                        <View style={{ flex: 1 , flexDirection:'column'}} >
                            <Text style={{  fontSize: 20 , color: "white" }}>{this.state.prestataire["Nom_etablissement"]}</Text>
                            <View style={{height:5}}/>
                            <Text style={{  fontSize: 12 , color: "white" }}>{this.state.prestataire["Email"]}</Text>

                        </View>
                    </View>

                <View  style={{height:20}} />

                </View>


                <View style={styles.container}>





                    {/* <View style={{ flexDirection:'row',alignItems:'center',justifyContent:'center', paddingTop: 20  }} > */}

                    {/* <FlatList
                    //horizontal={true}
                    // CellRendererComponent={ViewOverflow}
                    data={options}
                    numColumns={2}
                    renderItem={({ item }) => this._renderCart(item)}
                    keyExtractor = { (item,index) => index.toString() }
                /> */}
                {this.state.isLoading && <View style={styles.loading_container}>
                    <ActivityIndicator size="large" />
                </View>}
                
                <View style={{flex: 0.1 , flexDirection: 'row' ,marginHorizontal:10 }}>
                {/* <View style={{ flex:5, flexDirection: 'column'}}> */}
                    <TouchableOpacity  style={{  flex:1, alignItems: 'stretch', flexDirection: 'row' ,alignItems:'center', justifyContent: 'space-between' , backgroundColor: 'rgba(205, 205, 205,0.5)' , borderRadius: 20}}  onPress={ ()=> this.setState({openSearch : true})}>
                        <Icon type="MaterialIcons" name="search" style={{ }}/>
                        {/* <View style={{width : 20}}/> */}
                        <Text style={{ }}>
                            {this._formatDate(this.state.dateSearch)}
                        </Text>
                        <TouchableOpacity onPress={ async ()=> await this.fetchAllPrestataireCmd()}>
                            <Icon type="MaterialIcons" name="cancel" style={{ color : 'red'}}/>                   
                        </TouchableOpacity>

                    </TouchableOpacity>


                {/* </View > */}
                {/* <View style={{ flex:1 , flexDirection: 'column' , alignItems: 'flex-end'}}> */}
                    
                {/* </View>               */}
                


                </View>                        
                 
                {this.state.openSearch === true &&
                            <DateTimePicker
                                testID="dateTimePicker"
                                timeZoneOffsetInMinutes={0}
                                value={this.state.dateSearch}
                                mode="date"
                            
                                display="default"
                                onChange={(event, selectedDate) => {
                                    this.setState({ openSearch: false })
                                    if(selectedDate !== undefined)  this.setState({dateSearch:selectedDate},async ()=>  await this.search())
                                }}
                            />
                        }
                <ScrollView style={styles.container}>
                    <DataTable style={{backgroundColor : "rgba(205, 205, 205,0.5)" , borderRadius: 10 }}>
                        <DataTable.Header >
                            <DataTable.Title >Menu</DataTable.Title>
                            <DataTable.Title >Date</DataTable.Title>
                            <DataTable.Title numeric >Client</DataTable.Title>
                            <DataTable.Title numeric >Revenu </DataTable.Title>
                        </DataTable.Header>
                        {  this.state.facture.length > 0 &&  this.state.facture.map((fact,i)=>{
                            const dipr = this.calculDipr(fact.Prix_du_commande)
                            return this.renderHistorique(fact,i)
                        })} 
                    </DataTable>

                        <View style={{ flex: 0.3 ,flexDirection: 'row' , marginTop: 10 }}>
                            <View style={{  flex: 1  , flexDirection: 'column' ,  color: 'white'}}>
                                <Text style={{ fontWeight : "bold", fontSize : 20}}>
                                Total :
                                </Text>                            
                            </View>

                            <View style={{ flex: 1 , flexDirection: 'column' }}>
                                {/* {this.state.somme != 0 && */}
                                  <Text style={{ justifyContent: 'flex-end' ,textAlign : "right" ,fontWeight : "bold", fontSize : 20}}>{this.state.somme} €</Text>

                            </View>
                        </View>                   
                </ScrollView>
               

                </View>

           

        </View>
                      
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingTop:20,
        padding: 10,
        backgroundColor:'white',
        borderTopRightRadius:40,
        borderTopLeftRadius:40
    },
    cardContainer: {
        justifyContent: "center",
        alignItems: "center",
        padding: 20,
        backgroundColor: "white",
        width: wp('50%'),

        marginRight: 10,
        borderRadius: 5
    },
     loading_container: {
        position: 'absolute',
        zIndex: 10,
        left: 0,
        right: 0,
        top: 100,
        bottom: 0,
        alignItems: 'center',
        justifyContent: 'center'
    }
})




export default function Main() {
    return (
    //   <NavigationContainer>
        <Tab.Navigator
            
            activeColor={theme.colors.primary}
            inactiveColor='grey'
            barStyle={{backgroundColor:'white' , borderRadius: 10}}
            shifting={true}
            screenOptions={({ route }) => ({
                tabBarIcon: ({ focused, color, size }) => {
                    let iconName;
        
                    if (route.name === 'Dashboard') {
                    iconName = focused
                        ? 'tablet-dashboard'
                        : 'view-dashboard-outline';
                    } else if (route.name === 'Menu') {
                    iconName = focused ? 'format-list-bulleted' : 'format-list-bulleted';
                    } else if (route.name === 'Commande') {
                    iconName = focused ? 'cart-arrow-right' : 'cart';
                    }
        
                    // You can return any component that you like here!
                    return <Ionicons  name={iconName} size={20} color={color} />;
                },
                })}
                tabBarOptions={{
                activeTintColor: 'red',
                inactiveTintColor: 'black',
                }}
        
        
        
        
        >
          <Tab.Screen name="Dashboard" component={MainPrestataire} />
          <Tab.Screen name="Menu" component={Menu} />
          <Tab.Screen name="Commande" component={Commande} />

          


        </Tab.Navigator>
    //   </NavigationContainer>
    );
  }
