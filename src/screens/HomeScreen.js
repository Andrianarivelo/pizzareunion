import React, { memo, useState, useEffect } from 'react'
import { Image , TouchableHighlight, View, Text, ImageBackground, StyleSheet, StatusBar, Pressable, SafeAreaView, FlatList , TouchableOpacity } from 'react-native'
import {Toast, Container} from 'native-base'
import {
    widthPercentageToDP as wp,
    heightPercentageToDP as hp,
} from 'react-native-responsive-screen'
import { Avatar, Button } from 'react-native-paper';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome'
import {faClock , faPizza, faPizzaSlice, faUserAlt, faUserClock, faTrademark, faHandshake } from '@fortawesome/free-solid-svg-icons'
import { isClientLogged, logout, getData } from '../core/session';

import * as Font from 'expo-font'
import { AppLoading } from 'expo';
import { Ionicons } from '@expo/vector-icons';
import TopMenuP from '../components/TopMenuP';


const DATA = [
    {
      id: "bd7acbea-c1b1-46c2-aed5-3ad53abb28ba",
      title: "Commander un pizza",
      icon : faPizzaSlice,
      onPress : (navigation,isLogged) =>{
          if(isLogged){
            navigation.navigate("MainTabs",{screen:"Prestataire"},{express :false, isNewCli : false})
            
          }else{
            navigation.navigate("LoginTabs",{screen:"LoginScreen"},{express :false, isNewCli : false})
          }
       
      }
    },
    {
      id: "3ac68afc-c605-48d3-a4f8-fbd91aa97f63",
      title: "Commande express",
      icon : faClock,
      onPress : (navigation) =>{
        navigation.navigate("LoginTabs",{screen:"Prestataire",params:{express :true, isNewCli : false}})
      }
    },
    {
      id: "58694a0f-3da1-471f-bd96-145571e29d72",
      title: "Devenir partenaire",
      icon : faHandshake,
      onPress : (navigation,isLogged,isPrestaLogged) =>{
        if(isPrestaLogged){
            // navigation.navigate("MainPrestataire",{express :false, isNewCli : false})
            navigation.navigate('MainTabs', { screen: 'MainPrestataire' } ,{express :false, isNewCli : false});

        
          }else{
            navigation.navigate("AuthTabs" ,{screen:"LoginPrestataire"},{express :false, isNewCli : false})
          }
      }
    },
  ];

const Item = ({ item,isLogged,isPrestaLogged, style,navigation }) => (
        <TouchableOpacity 
            onPress={()=>{
                item.onPress(navigation,isLogged,isPrestaLogged)

            }} 
            style={[styles.item, style]}>        
            <View style ={{flexDirection: 'row' , flex:1 , alignSelf: 'stretch' }}>
                <View style={styles.element}>
                    <FontAwesomeIcon icon={item.icon} color={'black'} size={40}/>
                </View>
                <View style={styles.separator} />
                <View style={styles.element}>
                    <Text style={styles.title}>{item.title}</Text> 
                </View>
            </View>
            
        </TouchableOpacity>
    );


 



const HomeScreen = ({ navigation }) => {
    const [selectedId, setSelectedId] = useState(null);
    const [isLogged, setisLogged] = useState(false)
    const [isPrestaLogged, setisPrestaLogged] = useState(false)

    
    const navigationWillFocusListener  =  navigation.addListener('willFocus',async ()=>{
        await _getClient()
        await _getPresta()
    })
    const renderItem = ({ item }) => {
        // const backgroundColor = item.id === selectedId ? "#6e3b6e" : "white";   
        return (
          <Item
            item={item}
            onPress={() => setSelectedId(item.id)}
            navigation={navigation}
            isLogged={isLogged}
            isPrestaLogged={isPrestaLogged}

            // style={{ backgroundColor }}
          />
        );
      };



      useEffect( () => {

        (async()=> Font.loadAsync({
            Roboto: require('native-base/Fonts/Roboto.ttf'),
            Roboto_medium: require('native-base/Fonts/Roboto_medium.ttf'),
            ...Ionicons.font,
        }))()
          _getClient()
          _getPresta()

          return () => {
            //   setisLogged(ssss
            // navigationWillFocusListener.remove()
              
          }
      }, [isLogged,isPrestaLogged])

     const _getClient = async ()=>{
        let c = await isClientLogged()
        setisLogged(c)
     } 

    const _getPresta = async()=>{
        let p = await getData('prestataire')
        if(p != null && p.Num_prestataire !== undefined){
            // console.log('presta',p)
            setisPrestaLogged(true)
        }
    } 

     const _logout = async ()=>{
        await logout('client')
        await logout('prestataire')
        setisPrestaLogged(false)
        setisLogged(false)
     }
    
    return (

        <ImageBackground
            source={require("../../assets/home.jpg")}
            style={{ flex: 1, width: "100%" }}>

                {/* <TopMenuP/> */}
            <View style={styles.container} style={{ flex: 1 }}>
                <View style={{flex : 1.5  , flexDirection: 'row' }}>
                    <View style={{
                        flex:1,
                        flexDirection : 'column',
                        alignItems: 'center',
                        // juctifyContent: 'center',
                        // backgroundColor: 'blue'

                    }}>
                        <TouchableOpacity style={styles.avatarLeft}>
                            <Image
                                style={styles.tinyLogo}
                                source={require('../../assets/logo.png')}
                            />
                        </TouchableOpacity>
                        
                    </View>
                    <View style={{
                        flex:1,
                        flexDirection : 'column',
                        alignItems: 'center',
                        // backgroundColor : 'green'
                        // juctifyContent: 'center'
                    }}>
                        <TouchableOpacity style={styles.avatarRight}>
                            {/* <Image
                                style={styles.tinyLogo}
                                source={require('../../assets/logo.png')}
                            /> */}
                            <FontAwesomeIcon icon={faUserAlt} color={'white'} size={20}/>
                        </TouchableOpacity>
                        
                    </View>
                </View>
                <View style={{ flex:0.5 , flexDirection :'row' , alignItems: 'center' , justifyContent: 'center'}}>
                    <Text style={{color : 'white' }}>
                        CHOISSISEZ VOTRE TYPE DE COMMANDE
                    </Text>


                </View>

                {/* <View style={{flexDirection : "row",margin : 20}}>
                    <Text style={{color : "white",margin : 3,fontSize : 22}}>PIZZA</Text>
                    <Text style={{color : "#EDF514",margin  : 3,fontSize : 22}}>REUNION</Text>
                </View> */}

                {/* <View style={[styles.cardContainer, {
                    backgroundColor: "#046504", opacity: 0.75,
                    width: wp('90%'),
                    marginBottom : hp("1%")
                }]}>
                    <Avatar.Image size={70} source={require("../../assets/assietePizza.png")} />
                    <Text style={styles.label}>COMMANDER VOTRE PIZZA</Text>
                    <Text style={[styles.label,{fontSize : 10,margin : 2}]}>Sélectionnez votre pizzeria parmi nos nombreux partenaires</Text>
                    <Button onPress={()=>navigation.navigate("LoginScreen",{express :false, isNewCli : false})} labelStyle={{fontSize : 11}} mode="contained">Commander maintenant</Button>
                </View>

                <View style={{ flexDirection: "row" }}>
                    <View style={[styles.cardContainer, {
                        backgroundColor: "#A20F0F", opacity: 0.75,
                        width: wp('45%'),
                    }]}>
                        <Avatar.Image size={70} source={require("../../assets/assietePizza.png")} />
                        <Text style={styles.label}>COMMANDE EXPRESS</Text>
                       
                        <Button onPress={()=> navigation.navigate("LoginScreen",{express : true, isNewCli : false})} labelStyle={{fontSize : 11}} mode="contained">Allons-Y</Button>
                    </View>
                    <View style={[styles.cardContainer, {
                        backgroundColor: "#10AC9D", opacity: 0.75,
                        width: wp('45%'),
                    }]}>
                        <Avatar.Image size={70} source={require("../../assets/assietePizza.png")} />
                        <Text style={styles.label}>PARTENARIAT</Text>
                     
                        <Button onPress={()=>navigation.navigate("LoginPrestataire")} labelStyle={{fontSize : 11}} mode="contained">Rejoignez</Button>
                    </View>
                </View> */}
                <View style={{ flex: 4 }}>
                    <View style={styles.FlatList}  >
                            <FlatList
                                data={DATA}
                                renderItem={renderItem}
                                keyExtractor={(item) => item.id}
                                extraData={selectedId}
                            />
                    </View>    
                    <View style={{flex: 1 , alignItems:'center' ,flexDirection: 'column' }}>
                        { isLogged || isPrestaLogged  ?
                            <TouchableOpacity style={{ }} onPress={ () => _logout()}>
                                <Text dataDetectorType ={'link'} selectionColor='red' style={{ color : 'white'}}>
                                    DECONNEXION
                                </Text>
                            </TouchableOpacity>
                        :

                            <TouchableOpacity style={{ }} onPress={()=>navigation.navigate("LoginScreen",{express :false, isNewCli : false})}>
                            <Text dataDetectorType ={'link'} selectionColor='red' style={{ color : 'white'}}>
                                SE CONNECTER / CRÉER UN COMPTE
                            </Text>
                            </TouchableOpacity>
                        }
                    </View>

                </View>
                            
            </View>
        </ImageBackground>
    
    )
}

const styles = StyleSheet.create({
    container: {
        // flex: 1,
        // justifyContent: "center",
        // alignItems: "center"
        flex: 1,
        marginTop: StatusBar.currentHeight || 0,
    },
    cardContainer: {
        justifyContent: "center",
        alignItems: "center",
        padding: 20,
        marginRight: 10,
        borderRadius: 5
    },
    label : {
        color : "#E6F7F6",
        textAlign : "center"
    },
    item: {
        flex:1,
        backgroundColor: 'rgba(255,255,255,0.8)',
        // padding : 15,
        borderRadius: 10,
        marginVertical: 8,
        marginHorizontal: 16,
      },
    title: {
    fontSize: 20,
    },
    separator:{
        alignSelf : 'stretch',
        borderRightColor : 'black',
        borderRightWidth: 2,
        margin: 5
    },
    element:{
        flexDirection : 'row',
        alignItems: 'center',
        margin: 10,
    },
    FlatList:{
        flex: 1.5,
        flexDirection : 'row',
        // alignIems: 'center',
        justifyContent: 'center',
        // backgroundColor : 'red'
    },
    tinyLogo: {
        width: 20,
        height: 20,
      },
    avatarLeft:{
        // width: 30,
        // marginLeft: 20,
        position : "absolute",
        top : '50%',
        left : '10%',
        // transform : [{
        //     // translateX : -50
        //     // translateY : -50
        // }],
        padding : 10,
        backgroundColor : 'rgba(255,255, 255,0.8)',
        borderRadius : 10
    },
    avatarRight:{
        // width: 30,
        // paddingRight : 40,
        position : "absolute",
        top : '50%',
        right: '10%',
        padding : 10,
        backgroundColor : 'rgba(210, 10, 10,0.8)',
        borderRadius : 10
    }    
    
})




export default memo(HomeScreen)