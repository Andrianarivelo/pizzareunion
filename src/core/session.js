import AsyncStorage from '@react-native-community/async-storage';
import { connectedClient } from "../API/client/authClient"
import { connectedPrestataire } from "../API/prestataire/authPrestataire"
import { createIconSetFromFontello } from "react-native-vector-icons"

export const currentClient = async () =>{
  try {
    let response ;
    await AsyncStorage.getItem('client').then(async res =>{
      response = JSON.parse(res)
    })
// console.log('test',response)
    return response
     
  } catch(e) {
    console.log(e)
  }
}

export const isClientLogged = async () =>{
  try {
    let response ;
    await AsyncStorage.getItem('client').then(async res =>{
      if(res !== null) return response = true
      response = false
    })
     return response
  } catch(e) {
    console.log(e)
  }
}

export const isPrestaLogged = async () =>{
  try {
    let response ;
    await AsyncStorage.getItem('prestataire').then(async res =>{
      if(res !== null) return response = true
      response = false
    })
     return response
  } catch(e) {
    console.log(e)
  }
}

export const removeCart = async () => {    
  try { 
        await AsyncStorage.removeItem('cart')
      } catch(e) {
          console.log('errorRemove',e)
      }
}

export const logout = async (key) => {    
  try { 
        await AsyncStorage.removeItem(key)
        // alert('cart')           
      } catch(e) {
          console.log('errorLougout',e)
      }
}

export const getData = async (key) => {
  try {
    let response = []
    await AsyncStorage.getItem(key).then((jsonValue)=>{
      if(jsonValue !== null){
        response = JSON.parse(jsonValue)
      }   
    })
    return response
  } catch(e) {
    // error reading value
  }
}


export const currentPrestataire = async () => {
    let email = await AsyncStorage.getItem("prestataire")
  
    let prestataire = await connectedPrestataire(email)

    return prestataire
}

// export const storeDate = async (key,value) =>{
//     try {
//         await AsyncStorage.setItem(key, JSON.stringify(value));
//         alert('succes')
//       } catch (error) {
    
//         // Error saving data
//       }
// }

export const storeDate = async (key,value) => {
  try {
    const jsonValue = JSON.stringify(value)
    await AsyncStorage.setItem(key, jsonValue)
  } catch (e) {
    // saving error
  }
} 

export const storeData = async (key,value) => {
  try {
    await AsyncStorage.setItem(key, value)
  } catch (e) {
    // saving error
  }
} 

export const logged = ()=>{
  AsyncStorage.getItem('client').then((c)=>{
    if(c !== null ) return true
    return false
  })
}