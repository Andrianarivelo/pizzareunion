import { AsyncStorage } from "react-native";

const noSpace = new RegExp( /^$|\s+/) 

  export const emailValidator = email => {
    const re = /\S+@\S+\.\S+/;
  
    if ( email === '' || email.length <= 0) return "Email obligatoire";
    if (!re.test(email)) return "Renseigner une adresse email valide";
  
    return "";
  };
  
  export const passwordValidator = password => {
    if (!password || password.length <= 0) return "Mot de passe obligatoire";
    if(noSpace.test(password)) return " Espace vide non autorisé"
    if(password.length < 6) return 'Trop court ( au moins 6 caractères )'
    return "";
  };


  
  export const nameValidator = name => {
    const reg = new RegExp(/^[a-zA-Z]+$/)
    if (name === '' || name.length <= 0) return "Nom obligatoire";
    if (!reg.test(name)) return 'Nom invalide'
    return "";
  };
  export const prenomValidator = prenom => {
    const reg = new RegExp(/^[a-zA-Z \s]*$/)
    if (prenom === '' || prenom.length <= 0) return "Prénom obligatoire";
    if (!reg.test(prenom)) return 'Nom invalide'
    return "";
  };

  export const phoneValidator = phone => {
    const reg = new RegExp(/^\d{10}$/)
    if(phone.length <= 0) return 'Téléphone obligatoire'
    if(noSpace.test(phone)) return 'Espace vide non-autorisé'
    if(reg.test(phone)) return ''
    return 'Numéro non valide'
  }

  export const noSpecCharValidator = value =>{
    const reg = new RegExp(/^[0-9a-zA-Z]+$/)
    if(value.length <= 0) return 'Champ obligatoire'
    if(!reg.test(value)) return 'Caractère spéciaux non-autorisé'
  }

  
  
  export const credValidator = cred =>{
    //if (!email || email.length <= 0) return "Email cannot be empty.";
        if(cred) return "Wrong credential .";

  }



 export const userExist = cred =>{
    //if (!email || email.length <= 0) return "Email cannot be empty.";
        if(cred) return "User with same mail already exist ";

  }

export const  isNotNull = (value) =>{
  if (value === '' || value === null || value.length <= 0){
    return 'Champ obligatoire'
  }
  return ''
}


export const formatTime = (date)=> {
  if(date !== undefined){
  let hour = date.getHours()
  let minutes = date.getMinutes()

  hour = hour < 10 ? "0" + hour : hour
  minutes = minutes < 10 ? "0" + minutes : minutes

  return hour + ":" + minutes
  }
} 

export const formatDate =(date)=> {
  if(date !== undefined){
      let year = date.getFullYear();
      let month = date.getMonth() + 1;
      let dt = date.getDate();

      if (dt < 10) {
          dt = '0' + dt;
      }
      if (month < 10) {
          month = '0' + month;
      }

      return year + '-' + month + '-' + dt
  }
}

export const numberValidator = number =>{
  const reg = new RegExp (/^\d+$/)
  if(number === ''   || number.length <= 0) return 'Champ obligatoire'
  if(!reg.test(number)) return 'Veuillez entré une valeur Numérique'
  return ''

}