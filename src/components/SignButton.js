import React, { Component, memo } from 'react'
import { Text, View,StyleSheet } from 'react-native'
import { Button as SignBtn } from "react-native-paper";
import { theme } from "../core/theme"
const SignButton = ({mode,style,children,...props}) => (
    <SignBtn
    // style={[
    //     styles.button,
    //     mode === "outlined" && {backgroundColor :'#FDA500',
    //     style }
    // ]}
    // labelStyle={[
    //     styles.text,
    //     mode === "contained" && {color : 'rgb(253, 165, 0)'}
    // ]}
    style={[
      styles.button,
      props.disabled ? {backgroundColor: '#C88B15'} : {backgroundColor: theme.colors.primary}
    ]}
    labelStyle={[
      styles.text,
      props.disabled ? {color:'#A79E95'} : {color:'white'}
    ]}

    mode={mode}
    {...props}
    >
        {children}
    </SignBtn>
)

const styles = StyleSheet.create({
    button: {
      width: "100%",
      // padding:5,
      marginVertical: 10,
      backgroundColor : 'rgb(253, 165, 0)'
    },
    text: {
      fontWeight: "bold",
      fontSize: 15,
      lineHeight: 26
    }
  })

  export default memo(SignButton)
