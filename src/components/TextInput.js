import React, {memo} from "react"
import { View, StyleSheet, Text } from "react-native";
import { theme } from "../core/theme";
// import {Input,Label ,Item } from 'native-base'
import { TextInput as Input } from 'react-native'


const TextInput = ({errorText, ...props}) =>(
    <View style={styles.container}>
              <Input 
                style={[
                  props.style,
                  styles.input,
                  errorText ? {backgroundColor : 'rgba(247, 125, 120,0.8)'} : {backgroundColor : 'white'}
                ]}
                placeholder={props.label}
                {...props}
                // clearTextOnFocus={props.secureTextEntry}
                selectionColor={props.selectionColor ? props.selectionColor : "#CDCDCD" }
              
              />
        {errorText ? <Text style={styles.error}>{errorText}</Text> : null}
    </View>
)

const styles = StyleSheet.create({
    container: {
      width: "100%",
      marginVertical: 10
    },
    input: {
      backgroundColor: 'white',
      paddingHorizontal:10,
      height:40,
      minHeight:40,
      borderRadius: 10
      
    },
    error: {
      fontSize: 14,
      color: theme.colors.error,
      paddingHorizontal: 4,
      // paddingTop: 10,
    }
  })
  
  export default memo(TextInput)
