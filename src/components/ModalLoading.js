import React ,{useState,useEffect} from 'react';
import {Modal,Text,TouchableOpacity , StyleSheet,View ,StatusBar} from 'react-native'
import {ActivityIndicator} from 'react-native-paper'


const ModalLoading =  ({isVisible})=>{

return(

    <Modal
    animationType="fade"
    transparent={true}
    visible={isVisible}

    onRequestClose={() => {
        // onRequestClose()
    }}
>
    <View style={styles.centeredView}>
    <View style={styles.modalView}>
        <Text style={styles.modalText}>Veuillez patienter</Text>

        {/* <View style={{  flexDirection: 'row'}}>
        <TouchableOpacity
        style={{ ...styles.openButton, backgroundColor: "#FDA500" }}
        onPress={() => {
           setVisible()
        }}
        >
        <Text style={styles.textStyle}>Annuler</Text> */}
        {/* </TouchableOpacity>
        <View style={{width: 20}}/>
        <TouchableOpacity
        style={{ ...styles.openButton, backgroundColor: "#FDA500" }}
        onPress={() => {
           setVisible()
           onRequestClose()
        }}
        >
        <Text style={styles.textStyle}>Se connecter</Text>
        </TouchableOpacity> */}
        <ActivityIndicator  size='small' animating={isVisible} color='orange' />

        {/* </View> */}

       
    </View>
    </View>
</Modal>
)}

const styles = StyleSheet.create({
    centeredView: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        // marginTop: 22
      },
       modalView: {
        // margin: 20,
        backgroundColor: "rgba(255,255,255,0.8)",
        borderRadius: 20,
        padding: 35,
        alignItems: "center",
        shadowColor: "#000",
        shadowOffset: {
          width: 0,
          height: 2
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 1
      },
      
    //   openButton: {
    //     backgroundColor: "#F194FF",
    //     borderRadius: 20,
    //     padding: 10,
    //     elevation: 2
    //   },
      textStyle: {
        color: "white",
        fontWeight: "bold",
        textAlign: "center"
      },
      modalText: {
        marginBottom: 15,
        textAlign: "center",
        color: 'white'
      },


})

export default (ModalLoading)