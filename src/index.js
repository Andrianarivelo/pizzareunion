import React, { useState, useEffect } from 'react'
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createAppContainer , createSwitchNavigator} from "react-navigation"
import { createBottomTabNavigator } from 'react-navigation-tabs';
import AsyncStorage from '@react-native-community/async-storage'

import {
    HomeScreen,   
    Dashboard,
    LoginScreen,
    RegisterScreen,
    ForgotClient,
    AuthLoadingScreen,
    Prestataire,
    Pizza,

    MenuDetails,
    
    Panier,
    Notifications,
    Commande,

    LoginPrestataire,
    RegisterPrestataire,
    RegisterDone,
    ForgotPrestataire,
    MainPrestataire,
    CommandePrestataire,
    CommandeDetail,
    Historiques,
    Menu,
    MenuAdd,
    MenuEdit,

     Pay
} from "./screens"
import { isClientLogged ,isPrestaLogged, getData} from './core/session';

const navOptionHandler ={
    headerShown: false
}

const AuthContext = React.createContext();


const Stack = createStackNavigator()

const  Router = ({navigation}) =>{

    const [state, dispatch] = React.useReducer(
        (prevState, action) => {
          switch (action.type) {
            case 'RESTORE_TOKEN':
              return {
                ...prevState,
                userToken: action.token.userToken,
                prestaToken :action.token.prestaToken,
                isLoading: false,
              };
            case 'SIGN':
              return {
                ...prevState,
                isSignout: false,
                userToken: action.token.userToken,
                prestaToken :action.token.prestaToken,
              };
            case 'SIGN_OUT':
              return {
                ...prevState,
                isSignout: true,
                userToken: null,
                prestaToken : null,
              };
            
          }
        },
        {
          isLoading: true,
          isClientSignout: false,
          isPrestaSignout: false,
          userToken: null,
          prestaToken: null,
        }
      );
    
      React.useEffect(() => {
        // Fetch the token from storage then navigate to our appropriate place
        const bootstrapAsync = async () => {
          let userToken;
          let prestaToken;
    
          try {
            userToken =  await AsyncStorage.getItem('client') ;
            prestaToken = await AsyncStorage.getItem('prestataire')
          } catch (e) {
            // Restoring token failed
          }
    
          // After restoring token, we may need to validate it in production apps
    
          // This will switch to the App screen or Auth screen and this loading
          // screen will be unmounted and thrown away.
          
          dispatch({ type: 'RESTORE_TOKEN', token: { userToken : userToken , prestaToken : prestaToken  } });

        };
    
        bootstrapAsync();
      }, []);
    
      const authContext = React.useMemo(
        () => ({
          signIn: async data => {
            // In a production app, we need to send some data (usually username, password) to server and get a token
            // We will also need to handle errors if sign in failed
            // After getting token, we need to persist the token using `AsyncStorage`
            // In the example, we'll use a dummy token
    
            dispatch({ type: 'SIGN_IN', token: 'dummy-auth-token' });

          },
          signOut: () => dispatch({ type: 'SIGN_OUT_CLIENT' }),
          signUp: async data => {
            // In a production app, we need to send user data to server and get a token
            // We will also need to handle errors if sign up failed
            // After getting token, we need to persist the token using `AsyncStorage`
            // In the example, we'll use a dummy token
    
            dispatch({ type: 'SIGN_IN', token: 'dummy-auth-token' });
          },
        }),
        []
      );

        // alert(state.prestaToken)
        // alert(state.userToken)

    return (
        // <AuthContext.Provider>
        <NavigationContainer>
           <Stack.Navigator headerMode={'none'}>
                <Stack.Screen name="AuthTabs" component={AuthTbas} />
                <Stack.Screen name="LoginTabs" component={LoginTabs} />
                <Stack.Screen name="MainTabs" component={MainTabs} />

            </Stack.Navigator>  
        </NavigationContainer>    

        // </AuthContext.Provider>
   
    )
}


function LoginTabs(){
    return(
      <Stack.Navigator headerMode={'none'}>  
        {/* <Stack.Screen name="HomeScreen" component={HomeScreen} /> */}
        <Stack.Screen name="LoginScreen" component={LoginScreen} />
        <Stack.Screen name="ForgotClient" component={ForgotClient} />
        <Stack.Screen name="RegisterScreen" component={RegisterScreen} />
        <Stack.Screen name="Cart" component={Panier} />
        <Stack.Screen name="MenuDetails" component={MenuDetails} />
        <Stack.Screen name="Prestataire" component={Pizza} />
        <Stack.Screen name="AuthLoadingScreen" component={AuthLoadingScreen} />
        </Stack.Navigator>
    )
}

function AuthTbas(){
    return(
        <Stack.Navigator headerMode={"none"}> 
            <Stack.Screen name="HomeScreen" component={HomeScreen} />
            <Stack.Screen name="LoginPrestataire" component={LoginPrestataire} /> 
            <Stack.Screen name="ForgetPrestataire" component={ForgotPrestataire} /> 
            <Stack.Screen name="RegisterPrestataire" component={RegisterPrestataire} /> 

            <Stack.Screen name="LoginScreen" component={LoginScreen} />
            <Stack.Screen name="ForgotClient" component={ForgotClient} />
            <Stack.Screen name="RegisterScreen" component={RegisterScreen} />
            <Stack.Screen name="Cart" component={Panier} />
            <Stack.Screen name="MenuDetails" component={MenuDetails} />
            <Stack.Screen name="Prestataire" component={Pizza} />
            <Stack.Screen name="RegisterDone" component={RegisterDone} />

        </Stack.Navigator>
    )
}

function MainTabs(){
    return(
        <Stack.Navigator headerMode={"none"} >
            
            <Stack.Screen name="HomeScreen" component={HomeScreen}  />
            <Stack.Screen name="CommandePrestataire" component={CommandePrestataire} />
            <Stack.Screen name="Prestataire" component={Pizza} />
            <Stack.Screen name="MenuDetails" component={MenuDetails} />
            <Stack.Screen name="Cart" component={Panier} />
            <Stack.Screen name="Notifications" component={Notifications} />
            <Stack.Screen name="Commande" component={Commande} />
            <Stack.Screen name="MainPrestataire" component={MainPrestataire} />
            <Stack.Screen name="CommandeDetail" component={CommandeDetail} />
            <Stack.Screen name="Menu" component={Menu} />
            <Stack.Screen name="MenuAdd" component={MenuAdd} />
            <Stack.Screen name="MenuEdit" component={MenuEdit} />
            <Stack.Screen name="RegisterDone" component={RegisterDone} />
            <Stack.Screen name="Dashboard" component={Dashboard} />
            <Stack.Screen name="Pay" component={Pay} />

        </Stack.Navigator>

    )
}

// const AuthTabs = createStackNavigator({
//     HomeScreen:{
//         screen: HomeScreen,
//         navigationOptions:navOptionHandler
//     },
//     Pizza:{
//         screen : Pizza,
//         navigationOptions:navOptionHandler
//     },
//     MenuDetails:{
//         screen : MenuDetails,
//         navigationOptions:navOptionHandler
//     },
//     Panier:{
//         screen : Panier,
//         navigationOptions:navOptionHandler
//     },
//     AuthLoadingScreen:{
//         screen : AuthLoadingScreen,
//         navigationOptions:navOptionHandler
//     },
//     RegisterPrestataire:{
//         screen : RegisterPrestataire,
//         navigationOptions:navOptionHandler
//     },
//     LoginPrestataire:{
//         screen : LoginPrestataire,
//         navigationOptions:navOptionHandler
//     },
//     ForgotPrestataire:{
//         screen : ForgotPrestataire,
//         navigationOptions:navOptionHandler
//     },
// },{
//     initialRouteName: 'HomeScreen' , header: null
// } )


// const Router = createStackNavigator({
//     HomeScreen,
//     LoginScreen,
//     AuthLoadingScreen,
//     RegisterScreen,
//     ForgotClient,
//     Dashboard,
//     Prestataire,
//     Pizza,
//     MenuDetails,
//     Panier,
//     Notifications,
//     Commande,
//     LoginPrestataire,
//     RegisterPrestataire,
//     RegisterDone,
//     ForgotPrestataire,
//     MainPrestataire,
//     CommandePrestataire,
//     CommandeDetail,
//     Historiques,
//     Menu,
//     MenuAdd,
//     MenuEdit,
//     Pay
// },{
//     initialRouteName : "HomeScreen",
//     headerMode : "none"
// })

// const Router = createSwitchNavigator({ 
//     AuthTabs:{
//         screen : AuthTabs
//     },
//     MainTabs : {
//         screen : MainTabs
//     },
//     LoginTabs: {
//         screen:Login
//     }
// } ,{
//     backBehavior:'InitialRouteName'
// }  
// )


export default (Router)